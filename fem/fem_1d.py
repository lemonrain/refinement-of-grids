"""Finite element method for one dimensional domains"""
import math

import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    if d == 2:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
            ],
        )
        dirichlet = np.array([[0, 1]])
        neumann = np.array(
            [
                [1, 2],
                [2, 3],
                [3, 0],
            ],
        )
    elif d == 1:
        coord = np.array([[0], [1]])
        triangles = np.array([[0, 1]])
        dirichlet = np.array([0])
        neumann = np.array([1])
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [1, 0, 0],
                [1, 1, 0],
                [0, 1, 0],
                [0, 0, 1],
                [1, 0, 1],
                [1, 1, 1],
                [0, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2, 6],
                [0, 5, 1, 6],
                [0, 4, 5, 6],
                [0, 7, 4, 6],
                [0, 3, 7, 6],
                [0, 2, 3, 6],
            ],
        )
        dirichlet = np.array(
            [
                [0, 1, 2],
                [0, 3, 2],
            ],
        )
        neumann = np.array(
            [
                [1, 2, 6],
                [1, 6, 5],
                [0, 1, 5],
                [4, 5, 6],
                [0, 7, 4],
                [4, 6, 7],
                [0, 3, 7],
                [3, 6, 7],
                [2, 3, 6],
            ],
        )
    return coord, triangles, dirichlet, neumann


def get_area(coord:np.array) -> float:
    """Calculation of the length of a segment"""
    T = np.array([coord[1, :] - coord[0, :]])
    return T[0, 0] - T[0, 1]


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0], [1]])
    return np.linalg.solve(tmp1, tmp2)


def FEM(coord:np.array, triangles:np.array, dirichlet:np.array,
        f:callable)-> np.array:
    """Calculation of the finite element solution

    Args:
        coord: coordinates of the triangulation
        dirichlet: mapping of the coordinates to the dirichlet boundary
        triangles: mapping of the coordinates to the segments
        f: right hand side

    Returns:
        The solution calculated with the finite element method

    """
    dirichlet = np.array([0])
    nnodes = np.size(coord, 0)
    A = stiffness_matrix(coord, triangles)
    b = RHS_vector(coord, triangles, f)

    # calculate degrees of freedom
    dbnodes = np.unique(dirichlet)
    dof = np.setdiff1d(range(nnodes), dbnodes)
    A = A.tolil()

    # restrict right hand side and stiffness matrix to degrees of freedom
    A_inner = A[np.ix_(dof, dof)]
    b_inner = b[dof]

    x = np.zeros(nnodes)
    A_inner = A_inner.tocsr()
    x[dof] = ssp.linalg.spsolve(A_inner, b_inner)
    return x


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    d = np.size(coord, 1)
    nnodes = np.size(coord, 0)
    ctr = 0
    ctr_max = (d + 1) ** 2 * nelems
    Alocal = np.zeros((ctr_max, 1))
    I1 = np.zeros((ctr_max, 1))
    I2 = np.zeros((ctr_max, 1))

    for j,nodes_loc in enumerate(triangles):
        arr = np.transpose(coord[nodes_loc, :])
        A_t = np.concatenate((np.ones(d + 1).reshape((1, 2)), arr), axis=0)
        area = np.linalg.det(A_t) / math.factorial(d)
        grads = np.linalg.solve(A_t, np.concatenate((np.zeros((1, d)), np.eye(d))))
        for m in range(d + 1):
            for n in range(d + 1):
                Alocal[ctr] = area * np.matmul(grads[m, :], grads[n, :].T)
                I1[ctr] = triangles[j, m]
                I2[ctr] = triangles[j, n]
                ctr = ctr + 1
    return ssp.csr_matrix(
        (Alocal[:, 0], (I1[:, 0], I2[:, 0])), shape=(nnodes, nnodes),
    )


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    d = np.size(coord, 1)
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        arr = np.transpose(coord_loc)
        A_t = np.concatenate((np.ones(d + 1).reshape((1, 2)), arr), axis=0)
        area = np.linalg.det(A_t) / math.factorial(d)
        mid = 1 / 2 * (coord_loc[0, :] + coord_loc[1, :])
        b[nodes_loc] = b[nodes_loc] + area / 2 * f(mid)

    return b


#######################################################################
#######################################################################
#######################################################################

coord, section, dirichlet, neumann = get_geometry(1)


def fun_ex(x):
    """Exact solution"""
    return -(x**2) + 2 * x

u_exact = np.vectorize(fun_ex)


def f(x):
    """Right hand side"""
    return 2


max_err = np.zeros(6)
for j in range(6):
    coord, section, dirichlet, neumann, _, _ = red_refine(
        coord, section, dirichlet, neumann,
    )
    x = FEM(coord, section, dirichlet, f)
    u_at_nodes = u_exact(coord[:, 0])

## plot the solution graph (FEM and exact)
coord_sorted = np.sort(coord[:, 0])
x = np.sort(x)
fig, ax = plt.subplots(1, 2, figsize=(12, 6))
ax = ax.ravel()
ax[0].plot(coord_sorted, x)
ax[0].legend(["finite element solution"])

ax[1].plot(coord_sorted, u_exact(coord_sorted))
ax[1].legend(["exact solution"])
plt.show()
