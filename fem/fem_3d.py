"""Finite element method for three dimensional domains"""
import math

import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from red_refine import red_refine


def get_faces(tets:np.array)->np.array:
    """Get faces of the triangulation"""
    n = 4 * tets.shape[0]
    tris = np.zeros([n, 3], dtype=int)
    i = 0
    for tet in tets:
        tris[i] = [tet[0], tet[2], tet[1]]
        tris[i + 1] = [tet[0], tet[3], tet[1]]
        tris[i + 2] = [tet[0], tet[2], tet[3]]
        tris[i + 3] = [tet[1], tet[2], tet[3]]
        i = i + 4
    tris = np.sort(tris)
    return np.unique(tris, axis=0)


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    if d == 1:
        coord = np.array([[0], [1]])
        triangles = np.array([[0, 1]])
        dirichlet = np.array([0])
        neumann = np.array([1])

    # 2d Einheitsquadrat
    elif d == 2:
        coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])

    # 3d Einheitswürfel
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [1, 0, 0],
                [0, 0, 1],
                [1, 0, 1],
                [0, 1, 0],
                [1, 1, 0],
                [0, 1, 1],
                [1, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 3, 7],
                [0, 1, 5, 7],
                [0, 4, 5, 7],
                [0, 4, 6, 7],
                [0, 2, 6, 7],
                [0, 2, 3, 7],
            ],
        )
        neumann = np.zeros([0, 3])
        dirichlet = np.array(
            [
                [0, 1, 5],
                [0, 4, 5],
                [1, 3, 7],
                [1, 7, 5],
                [2, 3, 7],
                [2, 6, 7],
                [0, 2, 6],
                [0, 4, 6],
                [0, 1, 3],
                [0, 2, 3],
                [4, 6, 7],
                [4, 5, 7],
            ],
        )
    # L-Gebiet
    elif d == 4:
        coord = np.asarray(
            [[0, 0], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]],
        )
        triangles = np.asarray(
            [[0, 1, 2], [0, 2, 3], [5, 0, 3], [5, 3, 4], [6, 0, 5], [6, 7, 0]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 0]],
        )
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann



def FEM(coord:np.array, tets:np.array, dirichlet:np.array,
        f:callable)-> tuple:
    """Calculation of the finite element solution

    Args:
        coord: coordinates of the triangulation
        dirichlet: mapping of the coordinates to the dirichlet boundary
        tets: mapping of the coordinates to the tetraeders
        f: right hand side

    Returns:
        The solution calculated with the finite element method

    """
    nnodes = np.size(coord, 0)
    A = stiffness_matrix(coord, tets)
    b = RHS_vector(coord, tets, f)

    # calculate the degrees of freedom(dof)
    dbnodes = np.unique(dirichlet)
    dof = np.setdiff1d(range(nnodes), dbnodes)
    # restriction to the dof
    A_inner = A[np.ix_(dof, dof)]
    b_inner = b[dof]
    x = np.zeros(nnodes)
    A_inner = A_inner.tocsr()
    # solving of the problem
    x[dof] = ssp.linalg.spsolve(A_inner, b_inner)
    return x, A


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def fe_matrices(coord:np.array, tets:np.array) -> tuple:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(tets, 0)
    d_coord = np.size(coord, 1)
    nnodes = np.size(coord, 0)

    ctr = 0
    ctr_max = (d_coord + 1) ** 2 * nelems

    Slocal = np.zeros((ctr_max, 1))
    Mlocal = np.zeros((ctr_max, 1))
    I1 = np.zeros((ctr_max, 1))
    I2 = np.zeros((ctr_max, 1))
    m_loc = (np.ones((d_coord + 1, d_coord + 1)) + np.eye(d_coord + 1)) / (
        (d_coord + 1) * (d_coord + 2)
    )
    vol = np.zeros((nelems, 1))

    for j in range(nelems):
        arr = np.transpose(coord[tets[j, :], :])
        A_t = np.concatenate((np.ones(d_coord + 1).reshape((1, 4)), arr), axis=0)
        vol[j] = np.abs(np.linalg.det(A_t) / math.factorial(d_coord))
        grads = np.linalg.solve(
            A_t, np.concatenate((np.zeros((1, d_coord)), np.eye(d_coord))),
        )
        for m in range(d_coord + 1):
            for n in range(d_coord + 1):
                Slocal[ctr] = vol[j] * np.matmul(grads[m, :], grads[n, :].T)
                Mlocal[ctr] = vol[j] * m_loc[m, n]
                I1[ctr] = tets[j, m]
                I2[ctr] = tets[j, n]
                ctr = ctr + 1
    S = ssp.csc_matrix((Slocal[:, 0], (I1[:, 0], I2[:, 0])), shape=(nnodes, nnodes))
    M = ssp.csc_matrix((Mlocal[:, 0], (I1[:, 0], I2[:, 0])), shape=(nnodes, nnodes))
    S.eliminate_zeros()
    return S, M


def get_vol(coord:np.array) -> float:
    """Calculation of the volume of a tetraeder"""
    T = np.array(
        [
            coord[1, :] - coord[0, :],
            coord[2, :] - coord[0, :],
            coord[3, :] - coord[0, :],
        ],
    )
    return np.abs(np.sqrt(2) / 12 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0]) ** 3)


def stiffness_matrix(coord:np.array, tets:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(tets, 0)
    d_coord = np.size(coord, 1)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 4, 4))
    I1 = np.zeros((nelems, 4, 4))
    I2 = np.zeros((nelems, 4, 4))

    for j,nodes_loc in enumerate(tets):
        coord_loc = coord[nodes_loc, :]
        A_t = np.concatenate(
            (np.ones(d_coord + 1).reshape((1, 4)), np.transpose(coord_loc)), axis=0,
        )
        vol = np.abs(np.linalg.det(A_t) / math.factorial(d_coord))

        grads = get_gradients(coord_loc)
        Alocal[j, :, :] = vol * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        I1[j, :, :] = np.concatenate(
            (nodes_loc, nodes_loc, nodes_loc, nodes_loc), axis=0,
        )
        I2[j, :, :] = np.concatenate(
            (nodes_loc.T, nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1,
        )
    Alocal = np.reshape(Alocal, (16 * nelems, 1)).T
    I1 = np.reshape(I1, (16 * nelems, 1)).T
    I2 = np.reshape(I2, (16 * nelems, 1)).T
    A = ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])), shape=(nnodes, nnodes))
    A.eliminate_zeros()
    return A


def RHS_vector(coord:np.array, tets:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    d = np.size(coord, 1)
    b = np.zeros(nnodes)

    for nodes_loc in tets:
        coord_loc = coord[nodes_loc, :]
        arr = np.transpose(coord_loc)
        A_t = np.concatenate((np.ones(d + 1).reshape((1, 4)), arr), axis=0)
        vol = np.abs(np.linalg.det(A_t) / math.factorial(d))
        mid = (
            1/4
            * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :] + coord_loc[3, :])
        )

        b[nodes_loc] = b[nodes_loc] + vol / 4 * f(mid[0], mid[1], mid[2])
    return b


def show_3d(x:np.array, y:np.array, z:np.array, b:np.array, u_exact:callable)->None:
    """Plot routine"""
    c = u_exact(x, y, z)
    plt.figure(300)
    ax_ex = plt.axes(projection="3d")

    # Create color map
    color_map = plt.get_cmap("spring")

    color_norm = colors.Normalize(vmin=np.min(b), vmax=np.max(c))
    # Create scatter plot and colorbar
    scatter_plot = ax_ex.scatter3D(x, y, z, c=c, cmap=color_map, norm=color_norm)

    plt.colorbar(scatter_plot)

    # adding title and labels
    ax_ex.set_title("exact solution")
    ax_ex.set_xlabel("X-axis")
    ax_ex.set_ylabel("Y-axis")
    ax_ex.set_zlabel("Z-axis")

    plt.figure(301)
    ax_fem = plt.axes(projection="3d")

    # Create scatter plot and colorbar
    scatter_plot = ax_fem.scatter3D(x, y, z, c=b, cmap=color_map, norm=color_norm)

    plt.colorbar(scatter_plot)

    # adding title and labels
    ax_fem.set_title("fem")
    ax_fem.set_xlabel("X-axis")
    ax_fem.set_ylabel("Y-axis")
    ax_fem.set_zlabel("Z-axis")

    plt.show()
    return 0


#######################################################################
#######################################################################
#######################################################################

if __name__ == "__main__":
    coord, tets, dirichlet, neumann = get_geometry(3)
    n = 5

    def fun_ex(x, y, z):
        """Exact solution"""
        return (x - x ** 2) * (y - y ** 2) * (z - z ** 2)
    u_exact = np.vectorize(fun_ex)
    def f(x, y, z):
        """Right hand side"""
        return (2 * (x - x ** 2) * (z - z ** 2) + 2 * (x - x ** 2) *
                (y - y ** 2) + 2 * (y - y ** 2) * (z - z ** 2))

    max_err = np.zeros(n)
    diam = np.zeros(n)
    enorm_err = np.zeros(n)
    for j in range(n):
        coord, tets, dirichlet, neumann, _, _ = red_refine(
            coord,
            tets,
            dirichlet,
            neumann,
        )
        b, A = FEM(coord, tets, dirichlet, f)
        u_at_nodes = u_exact(coord[:, 0], coord[:, 1], coord[:, 2])
        max_err[j] = np.max(np.abs(u_at_nodes - b))
        enorm_err[j] = np.sqrt(
            np.abs((u_at_nodes - b).transpose().dot(A.dot(u_at_nodes - b))),
        )

        # Grid is at the beginning 1 unit big and is halfed by red refine
        # -> start with 1
        diam[j] = np.sqrt(2) / (2**j)
    fig, ax_err = plt.subplots()
    ax_err.loglog(diam, max_err, "-x", base=10)
    ax_err.set_xlabel("$h$")
    ax_err.set_ylabel("Maximums-Norm Fehler")
    plt.setp(
        ax_err,
        xticks=[10 ** (-2), 10 ** (-1), 10 ** (-0), 10**1],
        yticks=[10 ** (-4), 10 ** (-3), 10 ** (-2), 10 ** (-1)],
    )
    plt.show()
    print(max_err)
    print(enorm_err)

