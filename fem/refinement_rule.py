"""Subroutine of red_refinement"""
def refinement_rule(d:int)->list:
    """Gives back a list of the new indicees of a refined triangle

    Args:
        d: Dimension of the local triangle

    Returns:
        List with the new indicees of the local triangle
    """
    switcher = {
        # nothing happens
        0: [0],
        # new indice is 2
        #0----1 -> 0--2--1
        1: [0,2,2,1],
        # new are the elements with indicees 3,4,5
        #     2              2
        #    / \            / \
        #   /   \          4---5
        #  /     \        / \ / \
        # 0-------1      0---3---1
        2: [0,3,4,4,5,2,3,1,5,5,4,3],
        3: [0,4,5,6,4,1,7,8,5,7,2,9,6,8,9,3,4,5,6,8,8,5,7,4,5,6,8,9,9,7,8,5],
    }
    return switcher.get(d)
