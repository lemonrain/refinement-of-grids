"""Possible solution to sheet 6 exercise 2"""
import numpy as np
from hex06_1 import quad_mesh


def stiffness(mesh:tuple) -> np.array:
    """Calculate stiffness matrix on tensor-product mesh"""
    ref_stiff = np.array([
        [2/3, -1/6,-1/6,-1/3],
        [-1/6,2/3,-1/3,-1/6],
        [-1/6,-1/3,2/3,-1/6],
        [-1/3,-1/6,-1/6,2/3],
        ])
    (V,T,D,N) = mesh
    d = V.shape[1]
    h = V[1,0]-V[0,0]
    loc_s = h**(d-2) * ref_stiff

    N = V.shape[0]
    # global stiffness matrix
    S = np.zeros((N,N))
    for q in T:
        # create local-to-global matrix
        L = np.zeros((N,4))
        L[q,[0,1,2,3]] = 1
        S = S+L@loc_s@L.transpose()
    return S

def mass(mesh:tuple) -> np.array:
    """Calculate mass matrix on tensor-product mesh"""
    ref_mass = np.array([
        [1/9, 1/18,1/18,1/36],
        [1/18,1/9,1/36,1/18],
        [1/18,1/36,1/9,1/18],
        [1/36,1/18,1/18,1/9],
        ])
    (V,T,D,N) = mesh
    d = V.shape[1]
    h = V[1,0]-V[0,0]
    loc_m = h**(d) * ref_mass

    N = V.shape[0]
    M = np.zeros((N,N))
    for q in T:
        L = np.zeros((N,4))
        L[q,[0,1,2,3]] = 1
        M = M+L@loc_m@L.transpose()
    return M


if __name__ == "__main__":
    print("stiffness matrix:", "\n",stiffness(quad_mesh(2)))
    print("mass matrix:", "\n",mass(quad_mesh(2)))
