"""Possible solution to sheet 6 exercise 2"""
import matplotlib.pyplot as plt
import numpy as np
from hex06_1 import quad_mesh
from hex06_2 import mass, stiffness


def solve(mesh:tuple, f:callable) -> tuple:
    """Calculation of the fem solution on the tensor-product mesh"""
    V, T, D, N = mesh
    # assemble stiffness matrix
    S = stiffness(mesh)
    # assemble mass matrix
    M = mass(mesh)

    N = V.shape[0]

    inner_nodes = np.setdiff1d(range(N), D)

    u = np.zeros(N)

    b = M @ f(V[:,0],V[:,1])
    b = b[inner_nodes]
    A = S[np.ix_(inner_nodes,inner_nodes)]

    # solving
    u[inner_nodes] = np.linalg.solve(A,b)
    return u, S

def f(x, y):
    """Right-hand side"""
    return 2 * np.pi ** 2 * np.sin(np.pi * x) * np.sin(np.pi * y)

def u_exact(x, y):
    """Exact solution"""
    return np.sin(np.pi * x) * np.sin(np.pi * y)

if __name__ == "__main__":
    fine = 4
    enorm_err = np.zeros(fine)
    h = np.zeros(fine)
    j=0
    for n in range(2,6):
        mesh = quad_mesh(n)
        b,S = solve(mesh, f)
        u_ex = u_exact(mesh[0][:,0],mesh[0][:,1])
        enorm_err[j] = np.sqrt(np.abs((u_ex-b).transpose().dot(S.dot(u_ex-b))))

        h[j] = mesh[0][1,0]-mesh[0][0,0]
        j=j+1
    print("||u_ex-b||_E = ", enorm_err)

    fig, ax_err = plt.subplots()
    ax_err.loglog(h, enorm_err, "-x")
    plt.setp(ax_err,
        xticks=[10**(-1),
                10**(-0),
        ],
        yticks=[10**(-1),
                10**(-0),
        ],
    )
    ax_err.set_xlabel("$h$")
    ax_err.set_ylabel("e-norm error")
    ax_err.legend(["e-norm with $u_ex$ and $b$"])
    plt.show()

