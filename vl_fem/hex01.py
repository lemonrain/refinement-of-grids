"""Possible solution to sheet 7"""
import numpy as np
from numpy.linalg import det


def loc_mass(T:np.array) -> np.array:
    """Calculation of the local mass matrix"""
     # checking whether the array T has the correct shape
    assert T.shape[0] == T.shape[1] + 1, ("T is not of the correct shape "
                                          + str(T.shape) + ", shape (d, d+1) "
                                          + "for simplices needed")
    d = T.shape[1]  # dimension
     # checking, whether the triangle defined by T is of correct dimension
    assert d == 1 or d == 2, ("T has not the correct dimension "
                              + str(d) + ", only dimensions 1 and 2 implemented")
    if d == 1:
        # matrix B - Differential of linear transformation F
        B = T[1, 0] - T[0, 0]
        det_B = np.abs(B)  # determinant of B (for d=1 B is scalar)
        M_loc = np.array([[1/3,1/6],[1/6,1/3]])
    elif d == 2:
         # matrix B - Differential of linear transformation F
        B = np.zeros((d, d))
        B[:, 0] = T[1, :].T - T[0, :].T  # x2 - x1
        B[:, 1] = T[2, :].T - T[0, :].T  # x3 - x2
        det_B = np.abs(det(B))  # determinant of B
        M_loc = 1/24* np.array([[2,1,1],[1,2,1],[1,1,2]])
    return M_loc * det_B

if __name__ == "__main__":
    T1 = np.array([
        [1/2],
        [3/4],
    ])
    T2 = np.array([
        [1/2, 1/2],
        [3/4, 1/2],
        [1/2,3/4],
    ])
    print(loc_mass(T1))
    print(loc_mass(T2))
