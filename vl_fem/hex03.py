"""Possible solution to sheet 3"""
import numpy as np
from numpy.linalg import det, inv


def loc_stiffness(T:np.array) -> np.array:
    """Calculating local stiffness matrix"""
    # checking whether the array T has the correct shape
    assert T.shape[0] == T.shape[1] + 1, ("T is not of the correct shape "
                                          + str(T.shape) + ", shape (d, d+1) "
                                          + "for simplices needed")

    d = T.shape[1]  # dimension
    # checking, whether the triangle defined by T is of correct dimension
    assert d == 1 or d == 2, ("T has not the correct dimension " + str(d)
                              + ", only dimensions 1 and 2 implemented")

    if d == 1:
        I = np.array([[0],[1]])
        E = np.stack((np.array([1,1]),T[:,0]))
        det_E = np.absolute(det(E))
        grads = inv(E)@I
        # stiffness matrix S_T
        S = det_E * (grads @ grads.T)

    elif d == 2:
        # gradients of phi
        I = np.array([[0,0],[1,0],[0,1]])
        E = np.stack((np.array([1,1,1]),T[:,0],T[:,1]))
        grads = inv(E)@I
        det_E = np.absolute(det(E))

        # stiffness matrix S_T
        S = 1/2* det_E * (grads @ grads.T)
    return S
if __name__ == "__main__":
    t1 = np.array([
        [1/4],
        [1/2],
    ])
    t2 = np.array([
        [1/4,1/2],
        [1/2,1/2],
        [1/4,1/4],
    ])
    print(loc_stiffness(t1))
    print(loc_stiffness(t2))
