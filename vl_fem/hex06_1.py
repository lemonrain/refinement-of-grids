"""Possible solution to sheet 6 exercise 1"""
import numpy as np


def quad_mesh(n:int) -> tuple:
    """Generate tensor-product mesh"""
    h = 1/n
    N = np.zeros((0,1))
    D = np.zeros((0,1))
    V = np.zeros((0,2))
    T = np.zeros((0,4), dtype = int)
    for i in np.arange(0,1+h,h):
        for j in np.arange(0,1+h,h):
            V = np.append(V,[[j,i]], axis =0)
    for m in range(n**2+n-1):
        T = np.append(T,[[m,m+1,n+m+1,n+m+2]], axis = 0)

    # delete 'wrong' ones that are on the right boundary
    delete = np.arange(n,T.shape[0]-1,n+1)
    T = np.delete(T, delete,0)
    for i,v in enumerate(V):
        if v[0]==0 or v[0]==1 or v[1]==0 or v[1]==1:
            D = np.append(D, [[i]], axis = 0)
    return (V,T,D,N)


if __name__ == "__main__":
    mesh = quad_mesh(2)
    print(mesh)
