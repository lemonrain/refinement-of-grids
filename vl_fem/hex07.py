"""Possible solution for exercise 7"""
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
import scipy.sparse as ssp
from hex02 import assemble_mass, assemble_stiffness
from hex05_2 import initial_mesh, red_refine
from scipy.sparse.linalg import inv, spsolve


def solve_wave(mesh:tuple, f:callable, g:callable, r:callable, tau:float,
               n:int) -> tuple:
    """Calculation of the solution for the wave equation

    Args:
        mesh: tuple with coord,triangles, dirichlet and neumann boundary data
        f: right-hand side
        g: initial condition
        r: initial condition
        tau: time step size
        n: number of time steps

    Returns:
        The solution for the wave equation

    """
    tri = mtri.Triangulation(mesh[0][:,0],mesh[0][:,1], mesh[1])
    S = ssp.csc_matrix(assemble_stiffness(tri))
    M = ssp.csc_matrix(assemble_mass(tri))
    V, T, D, N = mesh
    nnodes = V.shape[0]
    inner_nodes = np.setdiff1d(range(nnodes), np.unique(D))
    S_inner = S[np.ix_(inner_nodes,inner_nodes)]
    M_inner = M[np.ix_(inner_nodes,inner_nodes)]

    u = np.zeros((n,nnodes))
    u[0] = g(V[:,0],V[:,1])
    u[1][inner_nodes] = spsolve(M_inner, M_inner@u[0][inner_nodes]+
                                tau*M_inner@r(V[:,0],V[:,1])[inner_nodes]-
                                tau**2/2*S_inner@u[0][inner_nodes]+
                                M_inner@f(0,V[:,0],V[:,1])[inner_nodes])
    S_inner = S[np.ix_(inner_nodes,inner_nodes)]
    M_inner = M[np.ix_(inner_nodes,inner_nodes)]

    for k in range(2,n):
        u[k][inner_nodes] = (tau**2*inv(M_inner)@
                             (M_inner@f(tau*k,V[:,0],V[:,1])[inner_nodes] -
                              S_inner@u[k-1][inner_nodes]) +
                              2*u[k-1][inner_nodes] - u[k-2][inner_nodes])
    return u,S,M

def visualization(t:int, coord: np.array, ut:np.array, triangles: np.array,
                  x:np.array) -> None:
    """Visualization of fem and exact solution"""
    fig = plt.figure(300)
    plt.title("Approximated solution")
    for j in range (t):
        ax = fig.add_subplot(int(t/4),4,j+1, projection="3d")
        ax.plot_trisurf(coord[:,0], coord[:,1], x[j], triangles=triangles)

    fig = plt.figure()
    plt.title("Exact solution")
    for k in range(t):
        ax = fig.add_subplot(int(t/4),4,k+1, projection="3d")
        ax.plot_trisurf(coord[:,0], coord[:,1],ut[k], triangles=triangles)
    plt.show()


def ex2a(T:int) -> None:
    """Error calculation between the approximated and exact solution"""
    err = np.zeros(4)
    hs = np.zeros(4)
    mesh = initial_mesh(2)
    mesh = red_refine(mesh)

    i = 0
    for k in [2,3,4,5]:
        # -1 because we are in python and it starts counting at 0
        h = 2**(-k-1)
        tau = h/2
        n = int(T/tau)

        time = np.zeros(n)
        time[0] = tau
        for step in range(n-1):
            time[step+1]=time[step]+tau


        mesh = red_refine(mesh)
        u_ex = g(mesh[0][:,0],mesh[0][:,1])
        nnodes=np.size(mesh[0],0)
        ut = np.zeros([n, nnodes])
        u_wave,S,_ = solve_wave(mesh, f, g, r, tau, n)
        error = np.zeros(n)
        for j in range(n):
            ut[j] = u_ex*np.cos(np.sqrt(2)*np.pi*time[j])
            error[j] = np.sqrt(np.abs((ut[j]-u_wave[j]).dot(S.dot((ut[j]-u_wave[j]).transpose()))))

        visualization(n,mesh[0],ut,mesh[1],u_wave)
        err[i] = np.max(error)

        # Gittergröße
        hs[i] = tau
        i = i+1
    print("||u_ex-u_wave||_E = ", err)
    print(hs)

    fig, ax_err = plt.subplots()
    ax_err.loglog(hs, err, "-x")
    ax_err.set_xlabel("$h$")
    ax_err.set_ylabel("E-Norm Fehler")
    ax_err.legend(["E-Norm mit $u_ex$ und $u_wave$"])
    plt.show()

def ex2b(T:int) -> None:
    """Calculation of the norm for the approximated solution"""
    k= 5
    h = 2**(-k-1)
    norm = np.zeros(3)
    mesh = initial_mesh(2)
    for _ in range(k):
        mesh = red_refine(mesh)

    for i,j in enumerate([1/2, 1, 2]):
        tau = h*j
        n = int(T/tau)

        u_wave,S,_ = solve_wave(mesh, f, g, r, tau, n)
        norm[i] = np.max(np.sqrt(np.abs((u_wave).dot(S.dot((u_wave).transpose())))))
    print(norm)

def ex2c(T:int) -> None:
    """Calculation of the discrete energy norm of the approximated solution"""
    k = 5
    h = 2**(-k-1)
    mesh = initial_mesh(2)
    for _ in range(k):
        mesh = red_refine(mesh)
    tau = h/2
    n = int(T/tau)

    u_wave,S, M = solve_wave(mesh, f, g, r, tau, n)

    time = np.zeros(n)
    error = np.zeros(n)
    time[0] = tau
    for j in range(n-1):
        time[j+1]=time[j]+tau
        u_half = (u_wave[j+1]-u_wave[j])/tau
        error[j] = (1/2*np.sqrt(np.abs((u_half).dot(M.dot((u_half).transpose())))) +
            1/2*np.sqrt(np.abs((u_wave[j]+1).dot(S.dot((u_wave[j]).transpose())))))

    print(error)
    print(time)
    fig, ax_err = plt.subplots()
    ax_err.loglog(time, error, "-x")
    ax_err.set_xlabel("time")
    ax_err.set_ylabel("Discrete energy")
    plt.show()


def f(x, y, z):
    """Right-hand side"""
    return 0 * y
def g(x, y):
    """Initial condition"""
    return np.sin(np.pi * x) * np.sin(np.pi * y)
def r(x, y):
    """Initial condition"""
    return 0 * x


if __name__ == "__main__":
    T = 1
    ex2a(T)

