"""Possible solution to sheet 5 exercise 1"""
import matplotlib.pyplot as plt
import numpy as np
from hex04 import solve
from hex05_1 import initial_mesh, red_refine


# H1 Fehler
def compute_H1_error(coord:np.array, triangles:np.array, gruex:callable,
                     u:np.array) -> np.array:
    """H1 error"""
    nelems=np.size(triangles,0)
    H1errSq = 0
    for j in range(nelems):
        nodes_loc=triangles[j,:]
        coord_loc=coord[nodes_loc,:]
        T=np.array([coord_loc[1,:]-coord_loc[0,:] ,
               coord_loc[2,:]-coord_loc[0,:] ])
        area = 0.5 * ( T[0,0]*T[1,1] - T[0,1]*T[1,0] )
        tmp1= np.concatenate((np.array([[1,1,1]]), coord_loc.T),axis=0)
        tmp2= np.array([[0,0],[1,0],[0,1]])
        grads = np.linalg.solve(tmp1,tmp2)
        mid=1/3*(coord_loc[0,:]+coord_loc[1,:]+coord_loc[2,:])
        gru_ex_mid = gruex(mid[0],mid[1])
        gruh = (u[nodes_loc[0]]*grads[0,:]
                + u[nodes_loc[1]]*grads[1,:]
                + u[nodes_loc[2]]*grads[2,:])
        e=gru_ex_mid-gruh
        H1errSq=H1errSq+area*(e[0]**2+e[1]**2)

    return H1errSq

if __name__ == "__main__":
    mesh = initial_mesh(2)

    def f(x, y):
        """Right-hand side"""
        return 2 * np.pi ** 2 * np.sin(np.pi * x) * np.sin(np.pi * y)

    def u_exact(x, y):
        """Exact solution"""
        return np.sin(np.pi * x) * np.sin(np.pi * y)

    def gruex(x, y):
        """Gradient of the exact solution"""
        return np.array([np.pi * np.cos(np.pi * x) * np.sin(np.pi * y),
                         np.pi * np.cos(np.pi * y) * np.sin(np.pi * x)])
    red = 5
    enorm_err = np.zeros(red)
    H1_err = np.zeros(red)
    h = np.zeros(red)

    for j in range(red):
        mesh = red_refine(mesh)
        b, S = solve(mesh, f)
        u_ex = u_exact(mesh[0][:,0],mesh[0][:,1])

        enorm_err[j] = np.sqrt(np.abs((u_ex-b).transpose().dot(S.dot(u_ex-b))))
        H1_err[j]=np.sqrt(compute_H1_error(mesh[0],mesh[1],gruex,b))

        # size of grid
        h[j] = np.sqrt(2)/(2**j)

    print("||u_ex-b||_E = ", enorm_err)

    fig, ax_err = plt.subplots()
    ax_err.loglog(h, enorm_err, "-x")
    plt.setp(ax_err,
        xticks=[10**(-2),
                10**(-1),
                10**(-0),
                ],
        yticks=[10**(-2),
                10**(-1),
                10**(0),
                ],
    )
    ax_err.set_xlabel("$h$")
    ax_err.set_ylabel("e-norm error")
    ax_err.legend(["e-norm with $u_ex$ and $b$"])
    plt.show()

