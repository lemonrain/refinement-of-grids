"""Possible solution to sheet 2"""
import matplotlib.tri as mtri
import numpy as np

# import of the already written functions
from hex01 import loc_mass
from session_01 import loc_stiffness


def assemble_stiffness(mesh:mtri.Triangulation) -> np.array:
    """Creating global stiffness matrix"""
    # concadenating of coordinates
    coord = np.stack((mesh.x,mesh.y), axis = -1)
    N = coord.shape[0]
    # global stiffness matrix
    S = np.zeros((N,N))
    for t in mesh.triangles:
        # local-to-global matrix
        L = np.zeros((N,3))
        # call to function
        loc_s = loc_stiffness(coord[t,:])
        L[t,[0,1,2]] = 1
        S = S+L@loc_s@L.transpose()
    return S


# nearly the same procedure as above
def assemble_mass(mesh:mtri.Triangulation) -> np.array:
    """Creating of the gobal mass matrix"""
    coord = np.stack((mesh.x,mesh.y), axis = -1)
    N = coord.shape[0]
    M = np.zeros((N,N))
    for t in mesh.triangles:
        L = np.zeros((N,3))
        loc_m = loc_mass(coord[t,:])
        L[t,[0,1,2]] = 1
        M = M+L@loc_m@L.transpose()
    return M

if __name__ == "__main__":
    # coordinates and triangles
    coord = np.array([
        [0,0],
        [1,0],
        [1/2,1/2],
        [0,1],
        [1,1],
    ])
    triangles = np.array([
        [2,0,1],
        [2,1,4],
        [2,4,3],
        [2,3,0],
    ])

    # creating the grid
    tri = mtri.Triangulation(coord[:,0], coord[:, 1], triangles)
    print("stiffness matrix:", "\n", assemble_stiffness(tri))
    print("mass matrix:", "\n", assemble_mass(tri))
