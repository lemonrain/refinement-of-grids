import numpy as np
from numpy.linalg import det, solve

def loc_stiffness(T):
    # check if everything is correct
    assert T.shape[0] == T.shape[1]+1, 'Triangle T has not the correct shape'
    d = T.shape[1]
    # check dimension
    if d == 1:
        B = T[1,0] - T[0,0]
        det_B = np.abs(B)
        S = np.zeros((2,2))
        for j in range(2):
            for k in range(2):
                if j==k:
                    phi = 1
                else:
                    phi = -1
                S[j,k] = phi / B /B*det_B
    elif d == 2:
        B = np.zeros((2,2))
        B[0,0] = T[1,0] - T[0,0]
        B[1,0] = T[1,1] - T[0,1]
        B[0,1] = T[2,0] - T[0,0]
        B[1,1] = T[2,1] - T[0,1]
        det_B = det(B)
        Dphi_hat = np.array([[-1,1,0],[-1,0,1]])
        BDphi = solve(B,Dphi_hat)
        S = np.zeros((3,3))
        for j in range(3):
            for k in range(3):
                S[j,k] = 1/2 * np.inner(BDphi[:,j],BDphi[:,k]) * det_B
    return S
if __name__ == '__main__':
    T1 = np.array([
        [1/2],
        [3/4]
    ])
    T2 = np.array([
        [1/2, 1/2],
        [3/4, 1/2],
        [1/2,3/4]
    ])
    print(loc_stiffness(T1))
    print(loc_stiffness(T2))