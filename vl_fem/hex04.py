"""Possible solution to sheet 4"""
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
from hex02 import assemble_mass, assemble_stiffness
from scipy.sparse.linalg import spsolve


def solve(mesh:tuple, f:callable) -> np.array:
    """Solving of the system with the finite element method"""
    V, T, D, N = mesh
    # assemble stiffness matrix
    S = assemble_stiffness(mtri.Triangulation(V[:,0], V[:,1], T))
    # assemble mass matrix
    M = assemble_mass(mtri.Triangulation(V[:,0], V[:,1], T))

    nnodes = V.shape[0]

    # all 'places' of the elements of V which are not in D
    inner_nodes = np.setdiff1d(range(nnodes), D)

    # create empty solution array
    u = np.zeros(nnodes)

    # calculate right hand side
    b = M @ f(V[:,0],V[:,1])
    # limit to degrees of freedom
    b_inner = b[inner_nodes]
    S_inner = S[np.ix_(inner_nodes,inner_nodes)]

    # solve the system of equations
    u[inner_nodes] = spsolve(S_inner,b_inner)
    return u, S

def show(coord:np.array, triangles:np.array, u:np.array, u_exact:callable) -> None:
    """Visualization"""
    plt.figure(300)
    ax_fe = plt.axes(projection ="3d")

    # create color map

    my_cmap = plt.get_cmap("summer")

    # create plot
    ax_fe.plot_trisurf(
        coord[:,0], coord[:,1], u,
        triangles = triangles,
        cmap = my_cmap,
        edgecolor="Gray",
    )

    # naming
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection ="3d")
    ax_ex.plot_trisurf(
        coord[:,0],coord[:,1],u_exact(coord[:,0], coord[:,1]),
        triangles = triangles,
        cmap = plt.get_cmap("summer"),
        edgecolor ="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()

def generate_mesh() -> tuple:
    """Generate fine grid"""
    x = np.linspace(0, 1, 15)
    y = np.linspace(0, 1, 15)
    xv, yv = np.meshgrid(x, y)
    C = np.array([xv.flatten(), yv.flatten()]).transpose()

    # make triangulation with delauny
    fine_tri = mtri.Triangulation(C[:,0],C[:,1])
    T = fine_tri.triangles

    N = np.zeros([0,2])
    D = np.zeros([0,2])

    # get edges
    for e in fine_tri.edges:
        if ((C[e[0]][0]==0 or C[e[0]][0]==1 or C[e[0]][1]==0 or C[e[0]][1]==1) and
            (C[e[1]][0]==0 or C[e[1]][0]==1 or C[e[1]][1]==0 or C[e[1]][1]==1)):
            D = np.append(D, [e], axis = 0)
    return (C,T,D,N)


if __name__ == "__main__":
    # adjust filepath to your folder structure
    filepath = "vl_fem/"
    V = np.load(filepath + "V.npy")
    T = np.load(filepath + "T.npy")
    D = np.load(filepath + "D.npy")
    N = np.load(filepath + "N.npy")
    mesh1 = (V, T, D, N)

    # define f
    def f(x, y):
        """Right hand side"""
        return 2 * np.pi ** 2 * np.sin(np.pi * x) * np.sin(np.pi * y)

    def u_exact(x, y):
        """Exact solution"""
        return np.sin(np.pi * x) * np.sin(np.pi * y)

    # plot the functions
    b1,_ = solve(mesh1, f)
    show(V, T, b1, u_exact)
    mesh2 = generate_mesh()
    b2,_ = solve(mesh2, f)
    show(mesh2[0], mesh2[1], b2, u_exact)


