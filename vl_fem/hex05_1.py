"""Possible solution to sheet 5 exercise 1"""
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np


def initial_mesh(d:int) -> tuple:
    """Get initial mesh data"""
    if d == 1:
        V =  np.array([
                [0],
                [1/2],
                [1],
                ])
        T =  np.array([
                [0,1],
                [1,2],
                ])
        D =  np.array([
                [0],
                [2],
                ])
        N = np.zeros((0, 1))
    elif d == 2:
        V =  np.array([
                [0,0],
                [1,0],
                [1,1],
                [0,1],
                [1/2,1/2],
                ])
        T =  np.array([
                [4,0,1],
                [4,1,2],
                [4,2,3],
                [4,3,0],
                ])
        D =  np.array([
                [0,1],
                [1,2],
                [2,3],
                [3,0],
        ])
        N = np.zeros((0, 2))
    return V, T, D, N


def red_refine(mesh:tuple) -> tuple:
    """Red refinement"""
    V, T, D, N = mesh  # unpacking the mesh
    nV, d = V.shape  # number of vertices, dimension
    nT, _ = T.shape  # number of elements

    # The following arrays contain information about the edges, and which
    # edges belong to which element/boundary.
    # E is the array containing all edges.  The shape is (number of edges, 2)
    # end the entries are the indices of the global vertices that are the
    # endpoints of each edge.
    # el2edges is the array containing the information on which edge belongs
    # to which element.  The shape is (Number of elements, edges per element)
    # and the entries are the indices of the edges
    # Analogously, D2edges and N2edges are the arrays containing information
    # on which edge belongs to the boundary.  The shape is
    # (Number of boundary edges, 1) for d=2 and the arrays are empty for d=1.
    # The entries are the indices of the edge.
    E, el2edges, D2edges, N2edges = get_edges(T, D, N)
    nE = E.shape[0]  # number of edges

    # calculate the midpoints
    # E contains all edges and all new nodes are on the midpoints of the old ones
    newV = 1/2*(V[E[:,0]]+V[E[:,1]])
    V = np.concatenate((V,newV))

    # global indices for only the new vertices.
    new_indices = nV + np.arange(nE)

    # use el2edges to add the correct new_indices to each element
    newT = new_indices[el2edges]
    ref_indces = refinement(d)

    # old triangle with new nodes
    T = np.concatenate((T,newT),axis=1)
    # ordering to get those together who build a triangle
    T = np.transpose(T[:, ref_indces])
    # seperate elements to get the right form
    T = np.transpose(T.reshape(d+1,-1, order = "F").copy())

    ref_indces = refinement(d-1)
    if D.shape[0] > 0:
        newD = new_indices[D2edges]
        D = np.concatenate((D,newD),axis=1)
        D = np.transpose(D[:, ref_indces])
        D = np.transpose(D.reshape(d,-1, order = "F").copy())

    if N.shape[0] > 0:
        newN = new_indices[N2edges]
        N = np.concatenate((N,newN),axis=1)
        N = np.transpose(N[:, ref_indces])
        N = np.transpose(N.reshape(d,-1, order = "F").copy())

    return V, T, D, N


def refinement(d:int) -> list:
    """Refinement convention"""
    # Given are the local indices of one refined element T:
    # d=0: [0]
    # d=1: [0, 1, 2]
    # d=2: [0, 1, 2, 3, 4, 5]
    # nothing happens
    if d == 0:
        return [0]
    if d == 1:
        # new one is the element with indicee 2 which lies between the values with 
        # indicees 0 and 1
        #0----1 -> 0--2--1
        return [0,2,2,1]
    if d == 2:
        # new are the elements with indicees 3,4,5
        #     2              2
        #    / \            / \
        #   /   \          4---5
        #  /     \        / \ / \
        # 0-------1      0---3---1
        return [0,3,4,4,5,2,3,1,5,5,4,3]
    return None


# The following function is given without needing to be adapted.
# It returns edge information of the mesh, i.e., it returns the edges and
# information whether an edge belongs to the boundary or a specific element
# of the mesh.
def get_edges(T:np.array, D:np.array, N:np.array) -> tuple:
    """Calculate edge information"""
    nT, d = T.shape[0], T.shape[1] - 1
    nD, nN = D.shape[0], N.shape[0]
    # NE_loc[0] is the number of edges a boundary element has
    # for d=1 boundary element is a point, for d=2 it is a line segment
    # NE_loc[1] is the number of edges an inner element has
    # for d=1 inner element is a line segment, for d=2 a triangle
    if d == 1:
        nE_loc = [0, 1]
    elif d == 2:
        nE_loc = [1, 3]
    B = np.concatenate((D, N))
    # array of all edges of shape (NE_loc*NT, 2)
    # for d=2 concatentated with boundary edges
    if d == 1:
        E = T
    elif d == 2:
        E = np.concatenate((np.reshape(T[:, [0, 1, 0, 2, 1, 2]], (-1, 2)), B))
    # obtaining all unique edges in E
    E, E_idx = np.unique(np.sort(E, axis=1), axis=0, return_inverse=True)
    # indices of edges for the respective array, T, D, N
    el2edges = np.reshape(E_idx[0:nT*nE_loc[1]], (-1, nE_loc[1]))
    if d == 1:
        # we need this part because reshape doesn't work on empty arrays
        D2edges = np.zeros((nD, 0), dtype="int")
        N2edges = np.zeros((nN, 0), dtype="int")
    else:
        D2edges = np.reshape(E_idx[nT*nE_loc[1] + np.arange(nE_loc[0]*nD)],
                             (-1, nE_loc[0]))
        N2edges = np.reshape(E_idx[nT*nE_loc[1] + nE_loc[0]*nD
                                   + np.arange(nE_loc[0]*nN)],
                             (-1, nE_loc[0]))
    return E.astype(int), el2edges, D2edges, N2edges

if __name__ == "__main__":

    mesh = initial_mesh(2)
    coord, triangles,_,_ = mesh
    tri1 = mtri.Triangulation(coord[:,0], coord[:, 1], triangles)
    plt.triplot(tri1)
    plt.show()
    coord, triangles,dirichlet,neumann = red_refine(mesh)
    tri2 = mtri.Triangulation(coord[:,0], coord[:, 1], triangles)
    plt.triplot(tri2)
    plt.show()
