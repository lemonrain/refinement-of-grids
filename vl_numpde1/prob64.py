"""Possible solution for Problem 64"""
import math

import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
import numpy.matlib
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    if d == 1:
        coord = np.array([[0], [1]])
        triangles = np.array([[0, 1]])
        dirichlet = np.array([0])
        neumann = np.array([1])

    # 2d Einheitsquadrat / 2d unit square
    elif d == 2:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
            ],
        )
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])

    # 3d Einheitswürfel / 3d unit cube
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [0, 0, 1],
                [1, 0, 0],
                [1, 0, 1],
                [0, 1, 0],
                [0, 1, 1],
                [1, 1, 0],
                [1, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 3, 7],
                [0, 2, 3, 7],
                [0, 1, 5, 7],
                [0, 4, 5, 6],
                [0, 2, 6, 7],
                [0, 4, 6, 7],
            ],
        )
        neumann = np.zeros([0, 3])
        dirichlet = np.array(
            [
                [0, 2, 6],
                [1, 5, 6],
                [0, 1, 5],
                [5, 4, 6],
                [0, 4, 6],
                [4, 6, 7],
                [2, 3, 7],
                [3, 7, 6],
                [3, 2, 6],
                [1, 0, 2],
                [0, 2, 3],
            ],
        )

    # L-Gebiet/ L domain
    elif d == 4:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
                [-1, 1],
                [-1, 0],
                [-1, -1],
                [0, -1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
                [5, 0, 3],
                [5, 3, 4],
                [6, 0, 5],
                [6, 7, 0],
            ],
        )
        dirichlet = np.array(
            [
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [5, 6],
                [6, 7],
                [7, 0],
            ],
        )
        neumann = np.zeros([0, 2])
    # 2d Einheitsquadrat nach Buch/ 2D unit square like in book
    elif d == 5:
        coord = np.array([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
        ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
        ])
        dirichlet = np.array([[0,1]])
        neumann = np.array([
            [1,2],
            [2,3],
            [3,0],
        ])
    # L-Gebiet mit Neumann Rand/ L domain with neumann boundary
    elif d == 6:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
                [-1, 1],
                [-1, 0],
                [-1, -1],
                [0, -1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
                [5, 0, 3],
                [5, 3, 4],
                [6, 0, 5],
                [6, 7, 0],
            ],
        )
        dirichlet = np.array(
            [
                [0, 1],
                [7, 0],
            ],
        )
        neumann = np.array(
            [
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [5, 6],
                [6, 7],
            ],
        )


    return coord, triangles, dirichlet, neumann

def edges(triangles:np.array, dirichlet:np.array, neumann:np.array)-> tuple:
    """Routine that calculates all around the edges"""
    # Number of elements (triangles)
    nE = triangles.shape[0]

    # Dimensionality (2D or 3D)
    d = triangles.shape[1] - 1

    # Number of Dirichlet and Neumann boundary edges
    #nDb = dirichlet.shape[0]
    #nNb = neumann.shape[0]

    # Determine the edges of the triangles based on dimensionality
    if d == 2:  # 2D case
        t_edges = np.concatenate(
            (triangles[:, [1, 2]], triangles[:, [2, 0]], triangles[:, [0, 1]]),
        )
    else:  # 3D case
        t_edges = np.concatenate(
            (
                triangles[:, [1, 3, 2]],
                triangles[:, [0, 2, 3]],
                triangles[:, [0, 3, 1]],
                triangles[:, [0, 1, 2]],
            ),
        )

    # Find unique edges and their indices
    [n4s, i2, j] = np.unique(
        np.sort(t_edges), return_index=True, return_inverse=True, axis=0,
    )
    # Reshape j to get side-to-element connectivity
    s4e = np.reshape(j, (d+1,nE)).T

    sorted_unique_indices = np.argsort(i2)
    side_numbers = np.empty_like(j)
    side_numbers[sorted_unique_indices] = np.arange(len(i2))

    # Initialize sign array for side orientations
    sign_s4e = np.ones(((d + 1) * nE, 1))
    sign_s4e[i2] = -1  # Mark the first occurrence of each unique edge with -1
    sign_s4e = np.reshape(sign_s4e, (nE, d + 1), order="F")

    # Return the computed arrays
    return s4e, sign_s4e, n4s

def compute_lengths(coord:np.array, n4s:np.array) -> np.array:
    """Compute the lengths of the sides"""
    data = coord[n4s]
    return np.sqrt((data[:,1,0]-data[:,0,0])**2 +
                  (data[:,1,1]-data[:,0,1])**2)

def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def compute_midpoints(coord:np.array, n4s:np.array) -> np.array:
    """Compute the midpoints of the sides"""
    return 0.5 * ( coord[n4s[:,0],:] + coord[n4s[:,1],:] )



def matrices(coord:np.array, triangles:np.array, n4s:np.array, s4e:np.array,
             sign_s4e:np.array) -> ssp.csr_matrix:
    """Calculation of the matrix"""
    nelems = triangles.shape[0]
    nsides = n4s.shape[0]

    Alocal = np.zeros((nelems, 3, 3))
    Blocal = np.zeros((nelems, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))
    I3 = np.zeros((nelems, 3))
    I4 = np.zeros((nelems, 3))

    mid_sides = compute_midpoints(coord, n4s)
    length_sides = compute_lengths(coord, n4s)

    for nelem,(nodes_loc,sides,signs) in enumerate(zip(triangles,s4e,sign_s4e)):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        # midpoint of side (x,y)
        mid = mid_sides[sides,:]

        coord_opposite = coord_loc
        lg = signs * length_sides[sides]
        for j in range(3):
            for k in range(3):
                Alocal[nelem,j,k] = lg[j] * lg[k] / (12 * area) * (
                    np.dot(mid[0, :] - coord_opposite[j, :], mid[0, :] - coord_opposite[k, :])
                    + np.dot(mid[1, :] - coord_opposite[j, :], mid[1, :] - coord_opposite[k, :])
                    + np.dot(mid[2, :] - coord_opposite[j, :], mid[2, :] - coord_opposite[k, :])
                )
            Blocal[nelem,j] = lg[j]
        # indice matrices
        I1[nelem, :, :] = np.stack((sides, sides, sides), axis=0)
        I2[nelem, :, :] = np.stack((sides, sides, sides), axis=1)
        I3[nelem,:] = sides
        I4[nelem,:] = np.array([nelem, nelem, nelem])

    A = ssp.csr_matrix((Alocal.ravel(), (I1.ravel(),I2.ravel())),
                       shape=(nsides, nsides))
    B = ssp.csr_matrix((Blocal.ravel(), (I3.ravel(), I4.ravel())),
                       shape=(nsides,nelems))
    return ssp.vstack((ssp.hstack((A,B)),
                       ssp.hstack((B.T,ssp.csr_matrix((nelems,nelems)))),
                       )).tocsr(), B


def RHS_vector(coord:np.array, triangles:np.array, n4s:np.array,
               f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nelems = triangles.shape[0]
    nsides = n4s.shape[0]

    b = np.zeros(nelems + nsides)

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        mid = np.reshape(mid,(1,-1))
        b[nsides+j] = -area * f(mid)[0]
    return b



def raviart_thomas(coord:np.array, triangles:np.array, dirichlet:np.array,
                   neumann:np.array, f:callable) -> tuple:
    """Raviart-Thomas method calculate the solution"""
    s4e, sign_s4e, n4s = edges(triangles,dirichlet,neumann)
    nsides = n4s.shape[0]

    S,B = matrices(coord,triangles,n4s, s4e, sign_s4e)
    b = RHS_vector(coord,triangles,n4s,f)
    x = ssp.linalg.spsolve(S, b)

    # Extract solutions for sigma and u
    sigma = x[0:nsides]
    u = x[nsides:]

    return sigma, u, s4e, sign_s4e, n4s, B

def calculate_sigma(coord:np.array, n4s:np.array, triangles:np.array,
                    sign_s4e:np.array, s4e:np.array, coef:np.array) -> np.array:
    """Calculates the vector field out of the coefficients"""
    nE = triangles.shape[0]
    u_T = np.zeros((nE,2))
    mid_sides = compute_midpoints(coord, n4s)
    length_sides = compute_lengths(coord, n4s)

    for nelem,(nodes_loc,sides,signs) in enumerate(zip(triangles,s4e,sign_s4e)):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        # midpoint of side (x,y)
        mid = mid_sides[sides,:]
        coord_opposite = coord_loc
        lg = signs * length_sides[sides]
        for j in range(3):
                u_T[nelem,:] = u_T[nelem,:] + lg[j]  / (6 * area) * (
                    coef[sides[j]] * (mid[0, :] - coord_opposite[j, :]
                    + mid[1, :] - coord_opposite[j, :]
                    + mid[2, :] - coord_opposite[j, :]))
    return u_T

def compute_mid_tri(coord:np.array, triangles:np.array) -> np.array:
    """Calculates all midpoints of the triangles of a triangulation"""
    nE = triangles.shape[0]
    mp_T = np.zeros((nE,2))
    for nelem,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        mp_T[nelem, :] = np.sum(coord_loc, axis=0) / 3
    return mp_T



def show_rt(coord:np.array, triangles:np.array, u:np.array, p:np.array,
            s4e:np.array, sign_s4e:np.array, n4s:np.array) -> None:
    """Plot routine"""
    nE = triangles.shape[0]
    u_T = calculate_sigma(coord, n4s, triangles,sign_s4e,s4e,u)
    mp_T = compute_mid_tri(coord, triangles)
    E = np.reshape(np.arange(3 * nE), (3, nE), order="F").T
    n4e_t = triangles.ravel()
    X = coord[n4e_t, 0]
    Y = coord[n4e_t, 1]
    P = np.repeat(p, 3)

    plt.figure(300)
    ax = plt.axes(projection="3d")

    # Creating color map

    my_cmap = plt.get_cmap("summer")

    # Creating plot
    ax.plot_trisurf(X, Y, P, triangles=E, cmap=my_cmap, edgecolor="Gray")

    # Adding labels
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")

    plt.figure(301)
    ax = plt.axes(projection="3d")
    ax.plot_trisurf(X, Y, np.repeat(u_exact(mp_T),3), triangles=E,
                    cmap=my_cmap, edgecolor="Gray")

    # Adding labels
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")


    fig, ax = plt.subplots()
    # Creating plot
    ax.triplot(coord[:,0], coord[:,1], triangles)

    ax.quiver(mp_T[:, 0], mp_T[:, 1], u_T[:, 0], u_T[:, 1])
    plt.show()
    return 0

def l2_norm_pressure(p:callable,p_h:np.array,triangles:np.array,
                coord:np.array)-> np.array:
    """Calculation of the error in the L2 norm"""
    err = 0
    mp_T = compute_mid_tri(coord, triangles)
    P_h = np.repeat(p_h, 3)
    P= np.repeat(p(mp_T),3)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        err = err + area / 3 * (np.mean(P[nodes_loc]) -
                                np.mean(P_h[nodes_loc])
                                )**2
    return np.sqrt(np.sum(err))


# ||div(sigma-sigma_h)||_l2 = ||u_ex -div sigma_h||_l2
def second_part(coord:np.array, triangles:np.array, sigmah:np.array,
                u_ex:callable, B:ssp.csr_matrix)-> np.array:
    """Calculation of the error in the energy norm"""
    enorm_err = 0
    tmp = B.T@sigmah
    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)

        mid = 1/3*np.sum(coord_loc,axis=0)
        mid = np.reshape(mid,(1,-1))
        div_sigmah =  1/area * tmp
        enorm_err = (
            enorm_err
            + area *(u_ex(mid) + div_sigmah[j])** 2 # div sigmah
            )
    return enorm_err

# ||sigma-sigma_h||_l2
def first_part(coord:np.array, triangles:np.array, s4e:np.array, sign_s4e:np.array,
               coef:np.array, ux:callable, uy:callable)-> np.array:
    """Calculation of the error in the energy norm"""
    err = 0
    length_sides = compute_lengths(coord, n4s)

    for nodes_loc,sides,signs in zip(triangles,s4e,sign_s4e):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        # midpoint of side (x,y)
        coef_sides = coef[sides]

        # merkwürdig aber es passt
        coord_opposite = coord_loc
        lg = signs * length_sides[sides]

        mid_T = 1/3*np.sum(coord_loc,axis=0)
        mid_T = np.reshape(mid_T,(1,-1))
        sigmah = 0
        for j in range(3):
            sigmah = sigmah + lg[j]*coef_sides[j] / (2 * area) * (
                        mid_T - coord_opposite[j, :])

        err = (
            err + (
                (ux(np.reshape(mid_T,(1,-1))) - sigmah[:,0])**2 +
                (uy(np.reshape(mid_T,(1,-1))) - sigmah[:,1])**2
                    ) *area
                    )
    return err

def convergence_history(hlist:np.array,errlist:np.array) -> None:
    """Convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history")
    ax.legend()
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    # Dirichlet Rand
    def u_D(x):
        return np.zeros(x.shape[0])

    def u_exact(x):
        return (x[:,0]-x[:,0]**2)*(x[:,1]-x[:,1]**2)

    def sigma(x):
        return np.array([(-1+2*x[:,0])*(-1+x[:,1])*x[:,1],
                (-1+x[:,0])*x[:,0]*(-1+2*x[:,1])])

    def u_h(x):
        return 2*(x[:,0]-x[:,0]**2+x[:,1]-x[:,1]**2)

    def ux(x):
        return x[:,1]*(x[:,1]-1)*(2*x[:,0]-1)
    def uy(x):
        return x[:,0]*(x[:,0]-1)*(2*x[:,1]-1)

    def u_test(x):
        return np.ones(x.shape)

    coord, triangles, dirichlet, neumann = get_geometry(2)
    red = 5
    err_l2 = np.zeros(red)
    err_hdiv = np.zeros(red)
    h = np.zeros(red)


    for j in range(red):
        coord, triangles, dirichlet, neumann, _, _ = red_refine(
            coord, triangles, dirichlet, neumann,
        )
        sigmah, uh, s4e, sign_s4e, n4s, B = raviart_thomas(coord,triangles,dirichlet,
                                                           neumann,u_h)

        h[j] = 0.5 ** (1+j)
        err_hdiv[j] = np.sqrt(first_part(coord,triangles,s4e, sign_s4e,sigmah,ux,uy)
                             + second_part(coord,triangles,sigmah,u_h,B))
        err_l2[j] = l2_norm_pressure(u_exact,uh,triangles,coord)


    show_rt(coord,triangles,sigmah,uh,s4e,sign_s4e,n4s)

    convergence_history(h, err_l2 + err_hdiv)

    print(err_l2)
    print(err_hdiv)

