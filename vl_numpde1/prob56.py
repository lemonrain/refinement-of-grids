"""Calculation of a saddle point problem with Mini-FEM see Problem 56"""
import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
import scipy.sparse as ssp
from red_refine import red_refine


# Quadrat auf (-1,1)^2
# mit inhomogenen Dirichlet Randbedingungen
def get_geometry() -> tuple:
    """Square on (-1,1)^2 with inhomogeneous dirichlet boundary"""
    coord = np.array([[-1,-1],
                    [1,-1],
                    [1,1],
                    [-1,1],
                    ])
    triangles = np.array([[0,1,2],
                        [0,2,3],
                        ])
    dirichlet = np.array([[0,1],
                        [1,2],
                        [2,3],
                        [3,0],
                        ])
    neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann

# Mini FEM
def FEM(coord:np.array, triangles:np.array, dirichlet:np.array, f:callable,
        uD:callable) -> tuple:
    """Mini FEM"""
    nelems=np.size(triangles,0)
    nnodes=np.size(coord,0)

    A=stiffness_matrix(coord,triangles)
    b=RHS_vector(coord,triangles,f)

    x=np.zeros(3*nnodes+2*nelems+1)

    # dirichlet nodes
    dbnodes=np.unique(dirichlet)
    for j in dbnodes:
        coord_loc=(coord[j,:])
        tmp=uD(coord_loc[0],coord_loc[1])
        x[j]=tmp[0]
        x[nnodes+j]=tmp[1]

    b=b-A.dot(x)

    inodes=np.setdiff1d(range(nnodes),dbnodes)
    dof=np.concatenate((inodes,nnodes+inodes,
                        2*nnodes+np.array(range(2*nelems+nnodes+1)),
                        ),axis=0)
    A_inner=A[np.ix_(dof,dof)]
    b_inner=b[dof]
    x[dof]=ssp.linalg.spsolve(A_inner,b_inner)

    u1=x[0:nnodes]
    u2=x[nnodes:2*nnodes]
    u=np.stack((u1,u2),axis=1)
    return x,u



def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix for a saddle point problem"""
    nelems=np.size(triangles,0)
    nnodes=np.size(coord,0)
    Alocal=np.zeros((nelems,3,3))
    Ab_loc=np.zeros((nelems,1)) #bubble part
    Blocal=np.zeros((nelems,3,8))
    I1=np.zeros((nelems,3,3))
    I2=np.zeros((nelems,3,3))

    J1=np.zeros((nelems,3,8))
    J2=np.zeros((nelems,3,8))
    #compute local matrices
    for j,nodes_loc in enumerate(triangles):
        coord_loc=coord[nodes_loc,:]
        T=np.array([coord_loc[1,:]-coord_loc[0,:] ,
               coord_loc[2,:]-coord_loc[0,:] ])
        area = 0.5 * ( T[0,0]*T[1,1] - T[0,1]*T[1,0] )
        tmp1= np.concatenate((np.array([[1,1,1]]), coord_loc.T),axis=0)
        tmp2= np.array([[0,0],[1,0],[0,1]])
        grads = np.linalg.solve(tmp1,tmp2)
        #P1 part
        Alocal[j,:,:]=area* np.matmul(grads,grads.T)
        I1[j,:,:] = np.stack((nodes_loc,nodes_loc,nodes_loc))
        I2[j,:,:] = np.stack((nodes_loc,nodes_loc,nodes_loc),axis=1)

        #bubble part
        Ab_loc[j]=area/180*np.sum((np.matmul(grads,grads.T)).diagonal())
        #local matrix B
        tmp3 = np.array([np.concatenate((grads[:,0],grads[:,1]),axis=0).T])
        Blocal[j,:,:]=area*(1/3)*np.concatenate(
            (np.concatenate((tmp3,tmp3,tmp3),axis=0),(1/60)*grads),axis=1)

        J1[j,:,:] = np.matlib.repmat(nodes_loc,8,1).T
        J2[j,:,:] = np.concatenate(
                    (np.array([nodes_loc]),np.array([nnodes+nodes_loc]),
                     np.array([[2*nnodes+j]]),
                     np.array([[2*nnodes+nelems+j]]),
                     ),axis=1)


    #assemble
    Alocal=np.reshape(Alocal,(9*nelems,1)).T
    I1=np.reshape(I1,(9*nelems,1)).T
    I2=np.reshape(I2,(9*nelems,1)).T
    A=ssp.csr_matrix((Alocal[0,:],(I1[0,:],I2[0,:])),
                     shape = (nnodes,nnodes))

    #assemble
    Blocal=np.reshape(Blocal,(24*nelems,1)).T
    J1=np.reshape(J1,(24*nelems,1)).T
    J2=np.reshape(J2,(24*nelems,1)).T
    B=ssp.csr_matrix((Blocal[0,:],(J1[0,:],J2[0,:])),
                     shape = (nnodes,2*(nnodes+nelems)))

    #assemble
    Ab_local = np.reshape(np.concatenate((Ab_loc,Ab_loc),axis=1).T,(2*nelems,1)).T
    K=np.reshape(range(2*nelems),(2*nelems,1)).T
    Ab=ssp.csr_matrix((Ab_local[0,:],(K[0,:],K[0,:])),\
        shape = (2*nelems,2*nelems))

    #compute nodal areas
    nodalareas=np.zeros((nnodes,1))
    for j in range(nelems-1):
        nodes_loc=triangles[j,:]
        coord_loc=coord[nodes_loc,:]
        T=np.array([coord_loc[1,:]-coord_loc[0,:] ,
               coord_loc[2,:]-coord_loc[0,:] ])
        area = 0.5 * ( T[0,0]*T[1,1] - T[0,1]*T[1,0] )
        nodalareas[nodes_loc] = nodalareas[nodes_loc]+area

    N=nnodes
    E=nelems
    A = ssp.vstack( (ssp.hstack((A,ssp.csr_matrix((N,N+2*E)))),
                     ssp.hstack((ssp.csr_matrix((N,N)),A,ssp.csr_matrix((N,2*E)))),
                     ssp.hstack((ssp.csr_matrix((2*E,2*N)),Ab))  ))


    A = ssp.vstack((ssp.hstack((A,B.T,ssp.csr_matrix((2*N+2*E,1)))),
                    ssp.hstack((B,ssp.csr_matrix((N,N)),1/3*nodalareas)),
                    ssp.hstack((ssp.csr_matrix((1,2*N+2*E)),1/3*nodalareas.T,0))))
    return A.tocsr()

def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nelems=np.size(triangles,0)
    nnodes=np.size(coord,0)
    b=np.zeros(3*nnodes+2*nelems+1)


    for j,nodes_loc in enumerate(triangles):
        coord_loc=coord[nodes_loc,:]
        tmp=np.array([coord_loc[1,:]-coord_loc[0,:] ,
               coord_loc[2,:]-coord_loc[0,:] ])
        area = 0.5 * ( tmp[0,0]*tmp[1,1] - tmp[0,1]*tmp[1,0] )
        mid=1/3*(coord_loc[0,:]+coord_loc[1,:]+coord_loc[2,:])

        # mid point rule to calculate integral
        f_mid = f(mid[0],mid[1])
        b[nodes_loc]=b[nodes_loc]+area/3*f_mid[0]
        b[nnodes+nodes_loc]=b[nnodes+nodes_loc]+area/3*f_mid[1]
        b[2*nnodes+j]=9*area*f_mid[0]
        b[2*nnodes+nelems+j]=9*area*f_mid[1]

    return b

def compute_H1_error(coord:np.array, triangles:np.array, gruex:callable,
                     u:callable) -> np.array:
    """Computation of the error in the H1 norm"""
    nelems=np.size(triangles,0)
    H1errSq = 0
    for j in range(nelems):
        nodes_loc=triangles[j,:]
        coord_loc=coord[nodes_loc,:]
        T=np.array([coord_loc[1,:]-coord_loc[0,:],
               coord_loc[2,:]-coord_loc[0,:] ])
        area = 0.5 * ( T[0,0]*T[1,1] - T[0,1]*T[1,0] )

        tmp1= np.concatenate((np.array([[1,1,1]]), coord_loc.T),axis=0)
        tmp2= np.array([[0,0],[1,0],[0,1]])
        grads = np.linalg.solve(tmp1,tmp2)

        mid=1/3*(coord_loc[0,:]+coord_loc[1,:]+coord_loc[2,:])
        gru_ex_mid = gruex(mid[0],mid[1])
        gruh = grads.T @ u[nodes_loc]
        e=gru_ex_mid-gruh
        H1errSq=H1errSq+area*(e[0]**2+e[1]**2)

    return H1errSq


def visulization(coord:np.array, triangles:np.array, u:np.array,
                 u1_at_nodes:np.array) -> None:
    """Visualization of the fem solution and the directions"""
    plt.figure()
    ax = plt.axes(projection ="3d")
    ax.plot_trisurf(coord[:,0], coord[:,1], u[:,0],
                    triangles = triangles,
                    cmap =plt.get_cmap("summer"),
                    edgecolor="Gray")
    ax.set_title("finite element solution")
    plt.show(block=False)

    plt.figure()
    ax = plt.axes(projection ="3d")
    ax.plot_trisurf(coord[:,0], coord[:,1], u1_at_nodes,
                    triangles = triangles,
                    cmap =plt.get_cmap("summer"),
                    edgecolor="Gray")
    ax.set_title("exact solution")
    plt.show(block=False)


    fig, ax = plt.subplots()
    ax.quiver(coord[:,0],coord[:,1], u[:,0], u[:,1])
    plt.show()


def convergence_history(hlist:np.array,errlist:np.array) -> None:
    """Convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history")
    ax.legend()
    plt.tight_layout()
    plt.show()


#######################################################################
#######################################################################
#######################################################################



def u1_exact(x, y):
    """Exact solution"""
    return 20 * x * y ** 4 - 4 * x ** 5
def u2_exact(x, y):
    """Exact solution"""
    return 20 * x ** 4 * y - 4 * y ** 5

def f(x, y):
    """Right-hand side"""
    return np.array([0, 0])

# Dirichlet Rand
def uD(x, y):
    """Dirichlet boundary"""
    return np.array([20 * x * y ** 4 - 4 * x ** 5,
                     20 * x ** 4 * y - 4 * y ** 5])

# Gradient für H1 Fehler
def gruex1(x, y):
    """Gradient of function 1"""
    return np.array([20 * y ** 4 - 20 * x ** 4,
                     80 * x * y ** 3]) # gradient für H1 Fehler
def gruex2(x, y):
    """Gradient of function 2"""
    return np.array([80 * x ** 3 * y,
                     20 * x ** 4 - 20 * y ** 4])

if __name__ == "__main__":
    nref=6
    max_err=np.zeros(nref)
    H1_err=np.zeros(nref)
    coord, triangles, dirichlet, neumann = get_geometry()
    diam = np.zeros(nref)
    for j in range(nref):
        coord, triangles, dirichlet,_,_,_ = red_refine(coord, triangles,
                                                       dirichlet, neumann)
        x,u = FEM(coord, triangles, dirichlet, f, uD)
        u1_at_nodes = u1_exact(coord[:,0],coord[:,1])
        u2_at_nodes = u2_exact(coord[:,0],coord[:,1])
        max_err[j] = np.max(np.abs(u1_at_nodes-u[:,0]))
        H1_err[j] = np.sqrt(compute_H1_error(coord,triangles,gruex1,u[:,0]) +
                            compute_H1_error(coord,triangles,gruex2,u[:,1]))
        diam[j] = 0.5 ** (1+j)
    print("Maximumsfehlernorm: ", max_err)
    print("H1-Fehlernorm: ", H1_err)

    # plot the convergence history
    convergence_history(diam, H1_err)
    ## plot the solution graph (FEM and exact)
    visulization(coord, triangles, u, u1_at_nodes)
