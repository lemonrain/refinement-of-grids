"""Possible solution for Problem 39"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from get_geometry import get_geometry
from red_refine import red_refine


def FEM(coord:np.array, triangles:np.array, dirichlet:np.array, f:callable,
        eps:float, beta:np.array) -> tuple:
    """Finite element method for the convection-diffusion equation"""
    nnodes=np.size(coord,0)
    A=stiffness_matrix(coord,triangles, eps, beta)
    b=RHS_vector(coord,triangles,f, eps)

    # calculate degrees of freedom
    dbnodes = np.unique(dirichlet)
    dof = np.setdiff1d(range(nnodes), dbnodes)
    ndof = np.size(dof)
    R = ssp.csr_matrix(
        (np.ones(ndof), (dof, np.arange(0, ndof))), shape=(nnodes, ndof),
    )

    A_inner = (R.transpose() @ A) @ R
    b_inner = R.transpose() @ b
    x = np.zeros(nnodes)
    x[dof] = ssp.linalg.spsolve(A_inner,b_inner)
    return x

def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)

def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def stiffness_matrix(coord:np.array, triangles:np.array, eps:float,
                     beta:np.array) -> ssp.csr_matrix:
    """Calculation of the stiffness matrix for the convection-diffusion equation"""
    nelems=np.size(triangles,0)
    nnodes=np.size(coord,0)
    Alocal=np.zeros((nelems,3,3))
    B_tmp=np.zeros((3,3))
    I1=np.zeros((nelems,3,3))
    I2=np.zeros((nelems,3,3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc=coord[nodes_loc,:]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        B_tmp[:,:]=area/3*(beta@grads.T)
        Alocal[j,:,:]= area*(eps*np.matmul(grads,grads.T)) + B_tmp.T
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)


    Alocal=np.reshape(Alocal,(9*nelems,1)).T
    I1=np.reshape(I1,(9*nelems,1)).T
    I2=np.reshape(I2,(9*nelems,1)).T
    return ssp.csr_matrix((Alocal[0,:],(I1[0,:],I2[0,:])),shape = (nnodes,nnodes))



def RHS_vector(coord:np.array, triangles:np.array, f:callable,
               eps:float) -> np.array:
    """Calculation of the right-hand side for the convection-diffusion equation"""
    nnodes=np.size(coord,0)
    b=np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc=coord[nodes_loc,:]
        area =get_area(coord_loc)
        mid=1/3*(coord_loc[0,:]+coord_loc[1,:]+coord_loc[2,:])
        b[nodes_loc]=b[nodes_loc]+area/3*f(mid[0],mid[1],eps)

    return b

# Darstellung des Ergebnisses
def show(coord:np.array, triangles:np.array, u:np.array, u_exact:callable,
         eps:float) -> None:
    """Visulaization of the FEM and the exact solution"""
    plt.figure(300)
    ax_fe = plt.axes(projection ="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_fe.plot_trisurf(
        coord[:,0], coord[:,1], u,
        triangles = triangles,
        cmap = my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection ="3d")
    ax_ex.plot_trisurf(
        coord[:,0],coord[:,1],u_exact(coord[:,0], coord[:,1], eps),
        triangles = triangles,
        cmap = plt.get_cmap("summer"),
        edgecolor ="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()

def l2_norm(u:callable, u_h:np.array, triangles:np.array, coord:np.array,
            eps:float)-> np.array:
    """Calculation of the error in the L2 norm"""
    err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        err = err + area / 3 * (u(mid[0],mid[1], eps) -
                                np.mean(u_h[nodes_loc])
                                )**2
    return np.sqrt(np.sum(err))


def convergence_history(hlist:np.array,errlist:np.array) -> None:
    """Convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history")
    ax.legend()
    plt.tight_layout()
    plt.show()


def fun(x, y, eps):
    """Exact solution"""
    r1 = (-1 + np.sqrt(1 + 4*eps**2*(np.pi)**2))/(-2*eps)
    r2 = (-1 - np.sqrt(1 + 4*eps**2*(np.pi)**2))/(-2*eps)
    return ((np.exp(r1 * (x - 1)) - np.exp(r2 * (x - 1))) /
             (np.exp(-r1) - np.exp(-r2)) + x - 1) * np.sin(np.pi * y)

def f_new(x, y, eps):
    """Right-hand side"""
    r1 = (-1 + np.sqrt(1 + 4*eps**2*(np.pi)**2))/(-2*eps)
    r2 = (-1 - np.sqrt(1 + 4*eps**2*(np.pi)**2))/(-2*eps)
    return np.sin(np.pi * y) * (eps * (np.pi ** 2 * (
            np.exp(r1 * (x - 1)) - np.exp(r2 * (x - 1)) /
            (np.exp(-r1) - np.exp(-r2)) + x - 1)
          - (r1 ** 2 * np.exp(r1 * (x - 1)) - r2 ** 2 * np.exp(r2 * (x - 1))) /
            (np.exp(-r1) - np.exp(-r2))) +
            (r1 * np.exp(r1 * (x - 1)) - r2 * np.exp(r2 * (x - 1))) /
            (np.exp(-r1) - np.exp(-r2)) + 1)


#######################################################################
#######################################################################
#######################################################################

if __name__ == "__main__":
    epsilons = np.array([0.1, 0.001])
    beta = np.array([1,0])
    red = 6

    diam = np.zeros(red)
    error = np.zeros(red)
    for eps in epsilons:
        coord, triangles, dirichlet, neumann = get_geometry(2)
        u_exact=np.vectorize(fun)
        f_fun = np.vectorize(f_new)
        for j in range(red):
            coord, triangles, dirichlet,neumann,_,_ = red_refine(coord, triangles,
                                                           dirichlet, neumann)
            x=FEM(coord, triangles, dirichlet,f_fun, eps, beta)
            diam[j] = 0.5 ** (1+j)
            error[j] = l2_norm(u_exact,x,triangles,coord,eps)


        # plot the solution graph (FEM and exact)
        show(coord, triangles, x, u_exact, eps)
        convergence_history(diam,error)
