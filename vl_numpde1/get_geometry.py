"""Routine to get geometries"""
import numpy as np


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    # 1d Linie
    if d == 1:
        coord = np.array([
            [0],
            [1]])
        triangles = np.array([[0,1]])
        dirichlet = np.array([0])
        neumann = np.array([1])

    # 2d Einheitsquadrat
    elif d == 2:
        coord = np.array([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
        ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
        ])
        dirichlet = np.array([[0,1],
        [1,2],
        [2,3],
        [3,0]])
        neumann = np.zeros([0, 2])

    # 3d Einheitswürfel
    elif d == 3:
        coord = np.array([
            [0,0,0],
            [0,0,1],
            [1,0,0],
            [1,0,1],
            [0,1,0],
            [0,1,1],
            [1,1,0],
            [1,1,1]
            ])
        triangles = np.array([
            [0,1,3,7],
            [0,2,3,7],
            [0,1,5,7],
            [0,4,5,6],
            [0,2,6,7],
            [0,4,6,7],
        ])
        neumann = np.zeros([0,3])
        dirichlet = np.array([
            [0,2,6],
            [1,5,6],
            [0,1,5],
            [5,4,6],
            [0,4,6],
            [4,6,7],
            [2,3,7],
            [3,7,6],
            [3,2,6],
            [1,0,2],
            [0,2,3],
            ])

    # L-Gebiet
    elif d == 4:
        coord = np.asarray([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
            [-1,1],
            [-1,0],
            [-1,-1],
            [0,-1],
            ])
        triangles = np.asarray([
            [0,1,2],
            [0,2,3],
            [5,0,3],
            [5,3,4],
            [6,0,5],
            [6,7,0],
            ])
        dirichlet= np.array([
            [0,1],
            [1,2],
            [2,3],
            [3,4],
            [4,5],
            [5,6],
            [6,7],
            [7,0],
            ])
        neumann= np.zeros([0, 2])

    # 2d Einheitsquadrat nach Buch
    elif d == 5:
        coord = np.array([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
        ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
        ])
        dirichlet = np.array([[0,1]])
        neumann = np.array([
            [1,2],
            [2,3],
            [3,0],
        ])
    return coord, triangles, dirichlet, neumann
