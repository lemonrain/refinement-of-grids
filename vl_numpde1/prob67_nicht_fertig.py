"""Possible solution for problem 67"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    if d == 1:
        coord = np.array([
            [0],
            [1]])
        triangles = np.array([[0,1]])
        dirichlet = np.array([0])
        neumann = np.array([1])

    # 2d Einheitsquadrat
    elif d == 2:
        coord = np.array([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
        ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
        ])
        dirichlet = np.array([[0,1],
        [1,2],
        [2,3],
        [3,0]])
        neumann = np.zeros([0, 2])

    # 3d Einheitswürfel
    elif d == 3:
        coord = np.array([
            [0,0,0],
            [0,0,1],
            [1,0,0],
            [1,0,1],
            [0,1,0],
            [0,1,1],
            [1,1,0],
            [1,1,1],
            ])
        triangles = np.array([
            [0,1,3,7],
            [0,2,3,7],
            [0,1,5,7],
            [0,4,5,6],
            [0,2,6,7],
            [0,4,6,7],
        ])
        neumann = np.zeros([0,3])
        dirichlet = np.array([
            [0,2,6],
            [1,5,6],
            [0,1,5],
            [5,4,6],
            [0,4,6],
            [4,6,7],
            [2,3,7],
            [3,7,6],
            [3,2,6],
            [1,0,2],
            [0,2,3],
            ])

    # L-Gebiet
    elif d == 4:
        coord = np.asarray([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
            [-1,1],
            [-1,0],
            [-1,-1],
            [0,-1],
            ])
        triangles = np.asarray([
            [0,1,2],
            [0,2,3],
            [5,0,3],
            [5,3,4],
            [6,0,5],
            [6,7,0],
            ])
        dirichlet= np.array([
            [0,1],
            [1,2],
            [2,3],
            [3,4],
            [4,5],
            [5,6],
            [6,7],
            [7,0],
            ])
        neumann= np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


def FEM(coord:np.array, triangles:np.array, dirichlet:np.array,
        f:callable)-> np.array:
    """Calculation of the finite element solution

    Args:
        coord: coordinates of the triangulation
        dirichlet: mapping of the coordinates to the dirichlet boundary
        triangles: mapping of the coordinates to the triangles
        f: right hand side

    Returns:
        The solution calculated with the finite element method

    """
    nnodes=np.size(coord,0)
    A=stiffness_matrix(coord,triangles)
    b=RHS_vector(coord,triangles,f)
    dbnodes=np.unique(dirichlet)
    dof=np.setdiff1d(range(nnodes),dbnodes)
    A_inner=A[np.ix_(dof,dof)]
    b_inner=b[dof]
    x=np.zeros(nnodes)
    x[dof]=ssp.linalg.spsolve(A_inner,b_inner)
    return x

def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        # local stiffness matrix
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])),
                          shape=(nnodes, nnodes))


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        b[nodes_loc] = b[nodes_loc] + area / 3 * f(mid[0], mid[1])
    return b

def mass_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Calculation of the mass matrix"""
    nelems = np.size(triangles,0)
    nnodes = np.size(coord,0)
    Mlocal = np.zeros((nelems,3,3))
    I1 = np.zeros((nelems,3,3))
    I2=np.zeros((nelems,3,3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc,:]
        area = get_area(coord_loc)
        Mlocal[j,:,:] = area*(1/12) * np.array([[2,1,1],[1,2,1],[1,1,2]])
        nodes_loc = np.array([nodes_loc])
        I1[j,:,:] = np.concatenate((nodes_loc,nodes_loc,nodes_loc),axis=0)
        I2[j,:,:] = np.concatenate((nodes_loc.T,nodes_loc.T,nodes_loc.T),axis=1)

    Mlocal=np.reshape(Mlocal,(9*nelems,1)).T
    I1=np.reshape(I1,(9*nelems,1)).T
    I2=np.reshape(I2,(9*nelems,1)).T
    return ssp.csr_matrix((Mlocal[0,:],(I1[0,:],I2[0,:])),shape = (nnodes,nnodes))

def explicit_euler(coord:np.array, triangles:np.array, g0:np.array,
                   nnodes:np.array, dirichlet:np.array, J, f, time_step) -> np.array:
    """Explicit euler method

    Args:
        coord: coordinates of the triangles
        triangles: assignment of the coordinates
        g: initial solution
        t: time steps
        nnodes: amount of nodes
        dirichlet: dirichlet boundary data

    Returns:
        The solution calculated with the euler method

    """
    A=stiffness_matrix(coord, triangles)
    M=mass_matrix(coord,triangles)
    b = RHS_vector(coord,triangles,f)
    data = M.sum(axis=1).T
    M_lumped = ssp.spdiags(data,0,M.shape[0],M.shape[0]).tocsr()
    M_lumped_inv = ssp.spdiags(1.0/data,0,M.shape[0],M.shape[0]).tocsr()

    dbnodes=np.unique(dirichlet)
    dof=np.setdiff1d(range(nnodes),dbnodes)

    A_inner = A[np.ix_(dof,dof)]
    M_inner = M_lumped[np.ix_(dof,dof)]
    M_inv = M_lumped_inv[np.ix_(dof,dof)]
    b_inner = b[dof]

    x=np.zeros((J,nnodes))
    x[0] = g0

    for j in range(J-1):
        x[j+1][dof] = M_inv@(time_step*b_inner + (M_inner-time_step*A_inner.T)@x[j][dof])
    return x

def implicit_euler(coord:np.array, triangles:np.array, g0:np.array,
                   nnodes:np.array, dirichlet:np.array, J, f,time_step) -> np.array:
    """Implicit euler method

    Args:
        coord: coordinates of the triangles
        triangles: assignment of the coordinates
        g: initial solution
        t: time steps
        nnodes: amount of nodes
        dirichlet: dirichlet boundary data

    Returns:
        The solution calculated with the euler method

    """
    A=stiffness_matrix(coord, triangles)
    M=mass_matrix(coord,triangles)
    b = RHS_vector(coord,triangles,f)

    dbnodes=np.unique(dirichlet)
    dof=np.setdiff1d(range(nnodes),dbnodes)

    A_inner = A[np.ix_(dof,dof)]
    M_inner = M[np.ix_(dof,dof)]
    b_inner = b[dof]

    x=np.zeros((J,nnodes))
    x[0] = g0

    for j in range(J-1):
        x[j+1][dof] = ssp.linalg.spsolve(M_inner.T+time_step*A_inner.T,
        time_step*b_inner+M_inner.T@x[j][dof])
    return x

def visualization(t:int, coord: np.array, ut:np.array, triangles: np.array) -> None:
    """Visualization of fem and exact solution"""
    fig = plt.figure(300)
    plt.title("Approximated solution")
    for j in range (t):
        ax = fig.add_subplot(int(t/4),4,j+1, projection="3d")
        ax.plot_trisurf(coord[:,0], coord[:,1], x[j,:], triangles=triangles)

    fig = plt.figure()
    plt.title("Exact solution")
    for k in range(t):
        ax = fig.add_subplot(int(t/4),4,k+1, projection="3d")
        ax.plot_trisurf(coord[:,0], coord[:,1],ut[k], triangles=triangles)
    plt.show()


#######################################################################
#######################################################################
#######################################################################

def g(x, y):
    """Initial solution"""
    return np.sin(np.pi * x) * np.sin(np.pi * y)
def f(x,y):
    """Right-hand side"""
    return 0 * y

if __name__ == "__main__":
    T = 1
    red = 4

    coord, triangles, dirichlet, neumann = get_geometry(2)
    coord, triangles, dirichlet,_,_,_ = red_refine(coord, triangles, dirichlet,
                                                    neumann)
    for j in range(1,red):
        h = 2**(-j)
        time_step = h/2
        J = int(T/time_step)

        time = np.zeros(J)
        for i in range(J-1):
            time[i+1]=time[i]+time_step

        coord, triangles, dirichlet,_,_,_ = red_refine(coord, triangles, dirichlet,
                                                    neumann)
        g_vec = np.vectorize(g)
        initial = FEM(coord, triangles, dirichlet, g_vec)

        nnodes=np.size(coord,0)
        ut = np.zeros([J, nnodes])

        u = g_vec(coord[:,0],coord[:,1])

        # exact solution
        for i in range(J):
            ut[i] = u*np.exp(-2*np.pi**2*time[i])

        x=explicit_euler(coord, triangles, initial, nnodes, dirichlet, J, f,time_step)

        ## plot the solution graph (euler and exact)
        visualization(J,coord,ut,triangles)
