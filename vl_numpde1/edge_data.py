"""Subroutine for the red_refinement function"""
import numpy as np


def edge_data(triangles:np.array, dirichlet:np.array, neumann:np.array)->tuple:
    """Calculates all edges between nodes

    Args:
        triangles: manner of the triangles of the triangulation
        dirichlet: dirichlet boundary values
        neumann: neumann boundary data


    Returns:
        edges as well as matrices to regain old form after refinement

    """
    [nE, nV] = triangles.shape
    d = nV - 1
    idx = np.array([0, 1, 3, 6])
    boundary = np.concatenate((dirichlet, neumann), axis=0)
    new_edges = idx[d] * nE
    new_dirichlet = idx[d - 1] * dirichlet.shape[0]
    new_neumann = idx[d - 1] * neumann.shape[0]

    # get all connections between nodes
    edges = get_edges(d, triangles, boundary)

    # sort and get unique values
    # edge_numbers contains indicees to reconstrruct old array
    edges, edge_numbers = np.unique(
        np.sort(edges), return_inverse=True, axis=0,
    )

    el2edges = get_el2edges(new_edges, edge_numbers, idx, d)
    # indicees for dirichlet boundary
    Db2edges = get_Db2edges(new_edges, edge_numbers, new_dirichlet, idx, d)

    # indicees for neumann boundary
    Nb2edges = get_Nb2edges(
        new_edges, edge_numbers, new_dirichlet, new_neumann, idx, d,
    )

    return edges, el2edges, Db2edges, Nb2edges


# d = 1 -> case_1
# d = 2 -> case_2
# d = 3 -> case_3
def get_edges(d:int, triangles:np.array, boundary:np.array)->np.array:
    """Get edges depending on dimension"""
    switcher = {
        1: case_1,
        2: case_2,
        3: case_3,
    }
    func = switcher.get(d)
    return func(triangles, boundary)


def case_1(A:np.array, B:np.array)->np.array:
    return A


def case_2(A:np.array, B:np.array)->np.array:
    return np.concatenate((get_matrix(A, [0, 1, 0, 2, 1, 2]), B))


def case_3(A:np.array, B:np.array)->np.array:
    edges = get_matrix(A, [0, 1, 0, 2, 0, 3, 1, 2, 1, 3, 2, 3])
    return np.concatenate((edges, get_matrix(B, [0, 1, 0, 2, 1, 2])), axis=0)


def get_matrix(A:np.array, list_of_colums:list)->np.array:
    """Specifc way of reshaping

    The list contains how the columns of the array will be stacked
    """
    new_A = np.transpose(A[:, list_of_colums])
    reshaped_A = new_A.reshape(2, -1, order="F").copy()
    return np.transpose(reshaped_A)


def get_el2edges(new_edges:int, edge_numbers:np.array, idx:np.array,
                 d:int)->np.array:
    """Reshape edges"""
    edge_numbers = np.transpose(edge_numbers[0:new_edges])
    el2edges = edge_numbers.reshape(idx[d], -1, order="F").copy()
    return np.transpose(el2edges)


def get_Db2edges(new_edges:int, edge_numbers:np.array, new_dirichlet:int,
                 idx:np.array, d:int)->np.array:
    """Extrat the indicees of edge_numbers concering the dirichlet boundary"""
    ind1 = np.arange(new_dirichlet) + new_edges
    edge_numbers1 = edge_numbers[ind1]
    edge_numbers1 = np.transpose(edge_numbers1)
    if edge_numbers1.size > 0:
        Db2edges = edge_numbers1.reshape(idx[d - 1], -1, order="F").copy()
    else:
        Db2edges = edge_numbers1
    return np.transpose(Db2edges)


def get_Nb2edges(new_edges:int, edge_numbers:np.array, new_dirichlet:int,
                 new_neumann:int, idx:np.array, d:int)->np.array:
    """Extrat the indicees of edge_numbers concering the neumann boundary"""
    ind2 = np.arange(new_neumann) + new_edges + new_dirichlet
    edge_numbers2 = edge_numbers[ind2]
    edge_numbers2 = np.transpose(edge_numbers2)
    if edge_numbers2.size > 0:
        Nb2edges = edge_numbers2.reshape(idx[d - 1], -1, order="F").copy()
    else:
        Nb2edges = edge_numbers2
    return np.transpose(Nb2edges)
