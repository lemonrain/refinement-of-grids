"""Possible solution for Problem 18"""
import matplotlib.pyplot as plt
import numpy as np
from get_geometry import get_geometry
from red_refine import red_refine


def u_exact(x, y):
    """Exact solution"""
    return np.sin(12 * np.pi * x) * y ** 2


if __name__ == "__main__":
    # aus der Funktion get_geometry wird das Einheitsquadrat aufgerufen
    coord, triangles, dirichlet, neumann = get_geometry(2)

    # Durchführen von sechs Rotverfeinerungen
    for _ in range(6):
        coord, triangles, dirichlet,_,_,_ = red_refine(coord, triangles,
                                                       dirichlet, neumann)

    # plotten der Funktion auf der Triangulierung des Einheitsquadrates
    ex = plt.figure(301)
    ax_ex = plt.axes(projection ="3d")
    trisurf_ex = ax_ex.plot_trisurf(
        coord[:,0],coord[:,1],u_exact(coord[:,0], coord[:,1]),
        triangles = triangles,
        cmap = plt.get_cmap("summer"),
        edgecolor ="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()

