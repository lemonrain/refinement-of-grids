"""Calculation of a saddle point problem with Mini-FEM see Problem 63"""
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
from get_geometry import get_geometry
from red_refine import red_refine


def print_triangulation_with_numbered_edges(coord:np.array, triangles:np.array,
                                            dirichlet:np.array,
                                            neumann:np.array) -> None:
    """Visualization of the triangulation"""
    boundary = np.concatenate((dirichlet, neumann), axis=0)

    # erhalte alle Verbindungen zwischen den Knoten
    edges = get_edges(triangles, boundary)

    # sortiere edges, gebe jeweils nur die erste Verbindung für die Knoten zurück
    # edge_numbers sind die Indizes, um das alte Array rekonstruieren zu können
    edges = np.unique(np.sort(edges),  axis = 0)
    edge_coord = np.zeros(edges.shape)
    for i in range(edges.shape[0]):
        x = int(edges[i, 0])
        y = int(edges[i, 1])
        # wird berechnet um den Mittelpunkt der Kante zu bekommen
        edge_coord[i, :] = 1/2 *(coord[x, :] + coord[y, :])
        plt.annotate("%.i"%(i),
            xy=edge_coord[i,:], xycoords="data",
            xytext=(-6, -6), textcoords="offset points",\
             fontsize=10)
    tri = mtri.Triangulation(coord[:,0], coord[:, 1], triangles)
    plt.triplot(tri)
    plt.show()

def get_edges(triangles:np.array, boundary:np.array) -> np.array:
    """Routinte to get the edges"""
    triangles_tmp = np.transpose(triangles[:, [0,1,1,2,2,0]])
    triangles_tmp = triangles_tmp.reshape(2,-1, order = "F").copy().T
    return np.concatenate((triangles_tmp,boundary))


if __name__ == "__main__":
    coord, triangles, dirichlet, neumann = get_geometry(2)
    red = 2
    for _ in range(red):
        coord,triangles,dirichlet,neumann,_,_ = red_refine(coord,triangles,dirichlet,neumann)

    print_triangulation_with_numbered_edges(coord, triangles, dirichlet, neumann)
