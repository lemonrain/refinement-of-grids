"""Finite element method for two dimensional domains"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
import scipy.sparse.linalg
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    if d == 1:
        coord = np.array([[0], [1]])
        triangles = np.array([[0, 1]])
        dirichlet = np.array([0])
        neumann = np.array([1])

    # 2d Einheitsquadrat / 2d unit square
    elif d == 2:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
            ],
        )
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])

    # 3d Einheitswürfel / 3d unit cube
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [0, 0, 1],
                [1, 0, 0],
                [1, 0, 1],
                [0, 1, 0],
                [0, 1, 1],
                [1, 1, 0],
                [1, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 3, 7],
                [0, 2, 3, 7],
                [0, 1, 5, 7],
                [0, 4, 5, 6],
                [0, 2, 6, 7],
                [0, 4, 6, 7],
            ],
        )
        neumann = np.zeros([0, 3])
        dirichlet = np.array(
            [
                [0, 2, 6],
                [1, 5, 6],
                [0, 1, 5],
                [5, 4, 6],
                [0, 4, 6],
                [4, 6, 7],
                [2, 3, 7],
                [3, 7, 6],
                [3, 2, 6],
                [1, 0, 2],
                [0, 2, 3],
            ],
        )

    # L-Gebiet/ L domain
    elif d == 4:
        coord = np.asarray(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
                [-1, 1],
                [-1, 0],
                [-1, -1],
                [0, -1],
            ],
        )
        triangles = np.asarray(
            [
                [0, 1, 2],
                [0, 2, 3],
                [5, 0, 3],
                [5, 3, 4],
                [6, 0, 5],
                [6, 7, 0],
            ],
        )
        dirichlet = np.array(
            [
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [5, 6],
                [6, 7],
                [7, 0],
            ],
        )
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


def FEM(coord:np.array, triangles:np.array, dirichlet:np.array,
        f:callable)-> np.array:
    """Calculation of the finite element solution

    Args:
        coord: coordinates of the triangulation
        dirichlet: mapping of the coordinates to the dirichlet boundary
        triangles: mapping of the coordinates to the triangles
        f: right hand side

    Returns:
        The solution calculated with the finite element method

    """
    nnodes = np.size(coord, 0)
    S = stiffness_matrix(coord, triangles)
    b = RHS_vector(coord, triangles, f)

    # calculate degrees of freedom
    dbnodes = np.unique(dirichlet)
    dof = np.setdiff1d(range(nnodes), dbnodes)
    ndof = np.size(dof)
    R = ssp.csr_matrix(
        (np.ones(ndof), (dof, np.arange(0, ndof))), shape=(nnodes, ndof),
    )
    # restrict right hand side and stiffness matrix to degrees of freedom
    S_inner = (R.transpose() @ S) @ R
    b_inner = R.transpose() @ b

    x = np.zeros(nnodes)
    # get solution on degrees of freedom
    x[dof] = ssp.linalg.spsolve(S_inner, b_inner)
    return x


def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        # local stiffness matrix
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])),
                          shape=(nnodes, nnodes))


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        b[nodes_loc] = b[nodes_loc] + area / 3 * f(mid[0], mid[1])
    return b


def show(coord:np.array, triangles:np.array, u:np.array,
         u_exact:callable) -> None:
    """Plot routine to compare the FEM result with the exact solution"""
    plt.figure(300)
    ax_fe = plt.axes(projection="3d")

    # set color map
    my_cmap = plt.get_cmap("summer")

    # make plot
    ax_fe.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=triangles,
        cmap=my_cmap,
        edgecolor="Gray",
    )

    # labeling
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")

    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection="3d")
    ax_ex.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u_exact(coord[:, 0], coord[:, 1]),
        triangles=triangles,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()

def l2_norm(u:callable,u_h:np.array,triangles:np.array, coord:np.array)-> np.array:
    """Calculation of the error in the L2 norm"""
    err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        err = err + area / 3 * (u(mid[0],mid[1]) -
                                np.mean(u_h[nodes_loc])
                                )**2
    return np.sqrt(np.sum(err))

def plot_l2_enorm(err:np.array, grid:np.array)-> None:
    """Plot routine for the L2 norm"""
    fig,ax = plt.subplots()
    ax.plot(grid,err, label = "L2 norm")
    ax.plot(grid,err[1]*grid**2, label = "Comparison line")
    ax.set_xlabel("Grid size")
    ax.set_ylabel("L2 norm")
    ax.legend()
    plt.show()


# Fehlermessung mit Energienorm
def e_norm_fun(coord:np.array, triangles:np.array, u:np.array, ux:callable,
               uy:callable)-> np.array:
    """Calculation of the error in the energy norm"""
    enorm_err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(coord_loc)
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        enorm_err = (
            enorm_err
            + (
                (
                    ux(mid[0],mid[1])
                    - u[nodes_loc[0]] * grads[0, 0]
                    - u[nodes_loc[1]] * grads[1, 0]
                    - u[nodes_loc[2]] * grads[2, 0]
                )
                ** 2
                + (
                    uy(mid[0],mid[1])
                    - u[nodes_loc[0]] * grads[0, 1]
                    - u[nodes_loc[1]] * grads[1, 1]
                    - u[nodes_loc[2]] * grads[2, 1]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(enorm_err)

def plot_enorm(err:np.array, grid:np.array)-> None:
    """Plot routine for the error norm"""
    fig,ax = plt.subplots()
    ax.plot(grid,err, label = "Energy norm")
    ax.plot(grid,err[1]*grid , label = "Comparison line")
    ax.set_xlabel("Grid size")
    ax.set_ylabel("Energy norm")
    ax.legend()
    plt.show()



#######################################################################
#######################################################################
#######################################################################

def fun_ex(x, y):
    """Exact solution"""
    return (x - x ** 2) * (y - y ** 2)
def fun_f(x, y):
    """Right hand side"""
    return 2 * (x - x ** 2 + (y - y ** 2))

def ux(x, y):
    """Partial derivative"""
    return y * (y - 1) * (2 * x - 1)
def uy(x, y):
    """Partial derivative"""
    return x * (x - 1) * (2 * y - 1)


# example problems
if __name__ == "__main__":
    red = 6
    h = np.zeros(red)
    u_exact=np.vectorize(fun_ex)
    f=np.vectorize(fun_f)

    max_err = np.zeros(red)
    l2_err = np.zeros(red)
    e_err = np.zeros(red)
    coord, triangles, dirichlet, neumann = get_geometry(2)
    for j in range(red):
        coord, triangles, dirichlet, neumann, _, _ = red_refine(
            coord, triangles, dirichlet, neumann,
        )
        x = FEM(coord, triangles, dirichlet, f)
        u_at_nodes = u_exact(coord[:, 0], coord[:, 1])

        max_err[j] = np.max(np.abs(u_at_nodes - x))
        h[j] = np.sqrt(2) / (2**j)
        l2_err[j] = l2_norm(u_exact,x,triangles,coord)
        e_err[j] = e_norm_fun(coord,triangles,x,np.vectorize(ux),np.vectorize(uy))
    print(l2_err)
    plot_l2_enorm(l2_err,h)
    plot_enorm(e_err,h)

    ## plot the solution graph (FEM and exact)
    show(coord, triangles, x, u_exact)
