"""Possible solution for Problem 40"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
import scipy.sparse.linalg
from red_refine import red_refine


# Triangulierung mit Einheitsquadrat mit Neumann Rand
def get_geometry() -> tuple:
    """Get geometry"""
    coord = np.array([[0,0],
                      [1,0],
                      [1,1],
                      [0,1],
                      ])
    triangles = np.array([[0,1,2],
                          [0,2,3],
                          ])
    dirichlet = np.array([[0,1],
                          [2,3],
                          [3,0],
                          ])
    neumann = np.array([[1, 2]])
    return coord, triangles, dirichlet, neumann


def FEM(coord:np.array, triangles:np.array, dirichlet:np.array, neumann:np.array,
        f:callable, u_D:callable) -> np.array:
    """Finite element method with inhomogeneous DB"""
    nnodes=np.size(coord,0)
    A=stiffness_matrix(coord,triangles)
    b=RHS_vector(coord,triangles,f, A, u_D)
    dbnodes=np.unique(dirichlet)
    nbnodes = np.unique(neumann)
    dof=np.setdiff1d(range(nnodes),dbnodes)
    A_inner=A[np.ix_(dof,dof)]
    b_inner=b[dof]
    x=np.zeros(nnodes)
    x[dof]=ssp.linalg.spsolve(A_inner,b_inner)
    return x + u_D(coord[:,0],coord[:,1])

def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        # local stiffness matrix
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])),
                          shape=(nnodes, nnodes))

def RHS_vector(coord:np.array, triangles:np.array, f:callable,
               A: ssp.csr_matrix, u_D:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule with inhom. DB"""
    nnodes=np.size(coord,0)
    b=np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc=coord[nodes_loc,:]
        tmp=np.array([coord_loc[1,:]-coord_loc[0,:] ,
               coord_loc[2,:]-coord_loc[0,:] ])
        area = 0.5 * ( tmp[0,0]*tmp[1,1] - tmp[0,1]*tmp[1,0] )
        mid=1/3*(coord_loc[0,:]+coord_loc[1,:]+coord_loc[2,:])
        b[nodes_loc]=b[nodes_loc]+area/3*f(mid[0],mid[1])
    # anpassen der rechten Seite
    return b - A * u_D(coord[:,0],coord[:,1])

# Fehlermessung mit Energienorm
def e_norm_fun(coord:np.array, triangles:np.array, u:np.array, ux:callable,
               uy:callable)-> np.array:
    """Calculation of the error in the energy norm"""
    enorm_err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(coord_loc)
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        enorm_err = (
            enorm_err
            + (
                (
                    ux(mid[0],mid[1])
                    - u[nodes_loc[0]] * grads[0, 0]
                    - u[nodes_loc[1]] * grads[1, 0]
                    - u[nodes_loc[2]] * grads[2, 0]
                )
                ** 2
                + (
                    uy(mid[0],mid[1])
                    - u[nodes_loc[0]] * grads[0, 1]
                    - u[nodes_loc[1]] * grads[1, 1]
                    - u[nodes_loc[2]] * grads[2, 1]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(enorm_err)

def visualization(coord:np.array, triangles:np.array, u_exact:callable,
                  x:np.array) -> None:
    """Plot routine to compare the FEM result with the exact solution"""
    ax = plt.axes(projection ="3d")
    ax.plot_trisurf(coord[:,0],coord[:,1],x,
                            triangles = triangles,
                            cmap =plt.get_cmap("summer"),
                            edgecolor="Gray")
    ax.set_title("finite element solution")

    plt.figure(300)
    ax = plt.axes(projection ="3d")
    ax.plot_trisurf(coord[:,0],coord[:,1],u_exact(coord[:,0],coord[:,1]),
                            triangles = triangles,
                            cmap =plt.get_cmap("summer"),
                            edgecolor="Gray")
    ax.set_title("exact solution")
    plt.show()

def convergence_history(hlist:np.array,errlist:np.array) -> None:
    """Convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history")
    ax.legend()
    plt.tight_layout()
    plt.show()



#######################################################################
#######################################################################
#######################################################################

def fun(x, y):
    """Exact solution"""
    return 5 + np.sin(np.pi / 2 * x) * np.sin(np.pi * y)
def f(x, y):
    """Right-hand side"""
    return 5 / 4 * np.pi ** 2 * np.sin(np.pi / 2 * x) * np.sin(np.pi * y)
def ux(x, y):
    """Partial derivate"""
    return 1 / 2 * np.pi * np.cos(np.pi / 2 * x) * np.sin(np.pi * y)
def uy(x, y):
    """Partial derivative"""
    return np.pi * np.sin(np.pi / 2 * x) * np.cos(np.pi * y)
def u_D(x,y):
    """Dirichlet boundary"""
    return np.ones(x.shape)*5


if __name__ == "__main__":
    u_exact = np.vectorize(fun)
    u_x = np.vectorize(ux)
    u_y = np.vectorize(uy)

    e_err = np.zeros(6)
    diam = np.zeros(6)

    coord, triangles, dirichlet, neumann = get_geometry()
    for j in range(6):
        coord, triangles, dirichlet,neumann,_,_ = red_refine(coord, triangles,
                                                            dirichlet, neumann)
        # erstellen eines Vektors der 5en enthält
        x = FEM(coord, triangles, dirichlet, neumann, f, u_D)
        e_err[j] = e_norm_fun(coord,triangles,x,ux,uy)
        diam[j] = 0.5 ** (1+j)
    visualization(coord, triangles, fun, x)
    convergence_history(diam,e_err)
