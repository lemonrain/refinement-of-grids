"""V-cycle as a preconditioner"""

import numpy as np
import scipy.sparse as ssp
from precond import get_geometry
from red_refine import red_refine


# Glaettungsmethode
def jacobi(A:ssp.csc_array, b:np.array, u:np.array, nu:int) -> np.array:
    """Smooths u with the Jacobi method"""
    u_smoothed = u
    D = ssp.csr_matrix(
        (A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    Dinv = ssp.csr_matrix(
        (1.0 / A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    R = A - D
    for _ in range(nu):
        u_smoothed = Dinv.dot(b - R.dot(u_smoothed))
    return u_smoothed

def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])), shape=(nnodes, nnodes))


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        b[nodes_loc] = b[nodes_loc] + area / 3 * f(mid[0], mid[1])
    return b


def iterative(A:ssp.csc_matrix, b:np.array, l:int, u_ini:np.array) -> np.array:
    """Subrotine of the v cycle handling the iterative steps

    Args:
        A: Stiffness matrix
        b: Right hand side
        l: Amount of iterations
        u_ini: Initial value for the routine

    Returns:
        Gives back a solution for the PDE

    """
    nu_pre = 3
    nu_post = nu_pre
    b_list = [b]
    A_list = [A]
    u_list = []
    for ell in reversed(range(1, l)):
        # Berechnung der Restriktionsmatrix
        p_t = p1_list[ell].transpose().tocsc()

        # Vorglaettung
        u = jacobi(A, b, u_ini, nu_pre)
        u_list.append(u)

        # Berechnung der Steifigkeitsmatrix auf dem groeberen Gitter
        A_coarse = p_t @ A @ p1_list[ell]
        A_list.append(A_coarse)

        # residuen Berechnung
        b = p_t.dot(b - A.dot(u))
        b_list.append(b)

        A = A_coarse
        # Berechnung des Anfangswertes auf dem gröberen Gitter
        u_ini = np.zeros(b.shape[0])

    u = ssp.linalg.spsolve(A, b)

    b_list.reverse()
    A_list.reverse()
    u_list.reverse()
    for ell in range(1, l):
        # Hochschieben der Loesung auf das naechst feinere Gitter
        u = u_list[ell - 1] + p1_list[ell].dot(u)
        # Nachglaettung
        u = jacobi(A_list[ell], b_list[ell], u, nu_post)
    return u, A_list[-1]


def v_cycle_it(d_tmp:int, l:int, f:callable, r:int) -> tuple:
    """Routine to perform an iterative v cycle

    Args:
        d_tmp: Dimension of the problem here 2
        f: Function for the right hand side
        l: Amount of iterations
        r: Amount of times to repeat the calculation of the result

    Returns:
        Gives back a multigird, a standard FEM solution and the stiffness matrix

    """
    global p1_list
    # Initalisierung einiger Variablen
    p1_list = []
    d = d_tmp
    coord, triangles, dirichlet, neumann = get_geometry(d)
    dirichlet = np.concatenate((dirichlet, neumann))
    neumann = np.zeros([0, dirichlet.shape[1]])
    n_coord_prev = coord.shape[0]
    f_nodes_prev = np.setdiff1d(np.arange(n_coord_prev), np.unique(dirichlet))
    n_f_nodes_prev = np.size(f_nodes_prev)

    for _ in range(l):
        # Anzahl der Verfeinerungen
        ref_tuple = red_refine(coord, triangles, dirichlet, neumann)
        [coord, triangles, dirichlet, neumann, _, p1] = ref_tuple

        # speichern der Prolongationsmatrizen
        n_coord = coord.shape[0]
        f_nodes = np.setdiff1d(np.arange(n_coord), np.unique(dirichlet))
        n_f_nodes = np.size(f_nodes)

        R1 = ssp.csr_matrix(
            (np.ones(n_f_nodes), (f_nodes, np.arange(n_f_nodes))),
            shape=(n_coord, n_f_nodes),
        )
        R2 = ssp.csr_matrix(
            (np.ones(n_f_nodes_prev), (f_nodes_prev, np.arange(n_f_nodes_prev))),
            shape=(n_coord_prev, n_f_nodes_prev),
        )
        mat = (R1.transpose() @ p1) @ R2
        p1_list.append(mat)
        f_nodes_prev = f_nodes
        n_f_nodes_prev = n_f_nodes
        n_coord_prev = n_coord

    uhut_k = np.zeros(n_coord)
    u_k = np.zeros(n_coord)

    S = stiffness_matrix(coord, triangles)
    S = S.tolil()

    # rechte Seite
    b = RHS_vector(coord, triangles, f)

    # A eingeschraenkt auf die Freiheitsgrade
    A = (R1.transpose() @ S) @ R1

    b = R1.transpose().dot(b)

    u_ini = np.zeros(b.shape[0])
    # Berechnung des Ergebnisses
    for _j in range(r):
        u, A = iterative(A, b, l, u_ini)
        u_ini = u

    uhut_k[f_nodes] = u
    u_k[f_nodes] = ssp.linalg.spsolve(A, b)
    return A

