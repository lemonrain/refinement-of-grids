import sys

import numpy as np
import scipy.sparse as ssp
from scipy.sparse.linalg import spilu


def ichol(A):
    A = A.tolil()
    n = A.shape[0]
    for k in range(n):
        A[k, k] = np.sqrt(A[k, k])
        for i in range(k + 1, n):
            if A[i, k] != 0:
                A[i, k] = A[i, k] / A[k, k]
        for i in range(k + 1, n):
            for j in range(i, n):
                if A[i, j] != 0:
                    A[i, j] = A[i, j] - A[i, k] * A[j, k]
    A = ssp.tril(A)
    return A.tocsc()


def sparse_chol(A):
    # sparse LU decomposition
    LU = spilu(A, fill_factor=15, drop_tol=1e-5, diag_pivot_thresh=0)
    return LU.L.dot(ssp.diags(LU.U.diagonal() ** 0.5))


def ichol_2(A):
    n = A.shape[0]
    print(A)
    for i, j in zip(*A.nonzero()):
        if i == j:
            A[j, j] = np.sqrt(A[j, j])
        elif i > j:
            A[i, j] = A[i, j] / A[j, j]

    for k in range(n):
        A[k, k] = np.sqrt(A[k, k])
        for i in range(k + 1, n):
            if A[i, k] != 0:
                A[i, k] = A[i, k] / A[k, k]
        for i in range(k + 1, n):
            for j in range(i, n):
                if A[i, j] != 0:
                    A[i, j] = A[i, j] - A[i, k] * A[j, k]
    A = ssp.tril(A)
    return A.tocsc()


# The input matrix A must be a sparse symmetric positive-definite.
def sparse_cholesky(A):
    A.shape[0]
    LU = spilu(A, diag_pivot_thresh=0)  # sparse LU decomposition

    # check the matrix A is positive definite.
    if (ssp.linalg.eigs(A, which="SM", return_eigenvectors=False) > 10e-8).all():
        return LU.L.dot(ssp.diags(LU.U.diagonal() ** 0.5))
    else:
        sys.exit("The matrix is not positive definite")


if __name__ == "__main__":
    n = 5
    ex = np.ones(n)
    data = np.array([-ex, -ex, 4 * ex, -ex, -ex])
    offsets = np.array([-2, -1, 0, 1, 2])
    A = ssp.dia_matrix((data, offsets), shape=(n, n)).tocsc()

    A = ssp.dia_matrix((data, offsets), shape=(n, n)).tocsc()
    A = np.array([[1, 2], [2, 100]])
    A = ssp.csc_matrix(A)
    L1 = ichol(A)
    print("wiki: ", L1, "\n")
