"""Routine to calculate PDEs with a preconditioned cg method

In the following a routine will be given to calculate saddle point problems with a
preconditioner à la Bramble-Pasciak where the preconditioner itself is calculated
through a incomplete Cholesky-factorization.

"""

import math

import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
import scipy.sparse as ssp
from ichol import ichol
from multigrid_as_precond import iterative
from red_refine import red_refine


def get_geometry(d: int) -> tuple:
    """Gets a dimension and gives back a domain in the given dimension

    Args:
        d: Is the dimension

    Returns:
        The arrays coord containing the coordinates of the domain
        triangles containing the simplices of the given triangulation

    """
    # 2d Einheitsquadrat
    if d == 2:
        coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    # 3d Einheitswürfel
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [1, 0, 0],
                [0, 0, 1],
                [1, 0, 1],
                [0, 1, 0],
                [1, 1, 0],
                [0, 1, 1],
                [1, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 3, 7],
                [0, 1, 5, 7],
                [0, 4, 5, 7],
                [0, 4, 6, 7],
                [0, 2, 6, 7],
                [0, 2, 3, 7],
            ],
        )
        neumann = np.zeros([0, 3])
        dirichlet = np.array(
            [
                [0, 1, 5],
                [0, 4, 5],
                [1, 3, 7],
                [1, 7, 5],
                [2, 3, 7],
                [2, 6, 7],
                [0, 2, 6],
                [0, 4, 6],
                [0, 1, 3],
                [0, 2, 3],
                [4, 6, 7],
                [4, 5, 7],
            ],
        )
    # L-Gebiet
    elif d == 4:
        coord = np.array(
            [[0, 0], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]],
        )
        triangles = np.array(
            [[0, 1, 2], [0, 2, 3], [5, 0, 3], [5, 3, 4], [6, 0, 5], [6, 7, 0]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 0]],
        )
        neumann = np.zeros([0, 2])
    elif d == 5:
        # Quadrat auf (-1,1)^2
        # mit inhomogenen Dirichlet Randbedingungen
        coord = np.array([[-1, -1], [1, -1], [1, 1], [-1, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


def get_area(coord):
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc):
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord, triangles):
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    Ab_loc = np.zeros((nelems, 1))  # bubble part
    Blocal = np.zeros((nelems, 3, 8))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    J1 = np.zeros((nelems, 3, 8))
    J2 = np.zeros((nelems, 3, 8))

    # compute local matrices
    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)

        # P1 part
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        I1[j, :, :] = np.stack((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.stack((nodes_loc, nodes_loc, nodes_loc), axis=1)

        # bubble part
        Ab_loc[j] = area / 180 * np.sum((np.matmul(grads, grads.T)).diagonal())

        # local matrix B
        tmp3 = np.array([np.concatenate((grads[:, 0], grads[:, 1]), axis=0).T])
        Blocal[j, :, :] = (
            area
            * (1 / 3)
            * np.concatenate(
                (np.concatenate((tmp3, tmp3, tmp3), axis=0), (-1 / 20)  * grads),
                axis=1,
            )
        )
        J1[j, :, :] = np.matlib.repmat(np.array([nodes_loc]).T, 1, 8)
        J2[j, :, :] = np.concatenate(
            (
                np.array([nodes_loc]),
                np.array([nnodes + nodes_loc]),
                np.array([[2 * nnodes + j]]),
                np.array([[2 * nnodes + nelems + j]]),
            ),
            axis=1,
        )

    # assemble
    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    A = ssp.csc_matrix((Alocal[0, :], (I1[0, :], I2[0, :])), shape=(nnodes, nnodes))

    # assemble
    Blocal = np.reshape(Blocal, (24 * nelems, 1)).T
    J1 = np.reshape(J1, (24 * nelems, 1)).T
    J2 = np.reshape(J2, (24 * nelems, 1)).T
    B = ssp.csc_matrix(
        (Blocal[0, :], (J1[0, :], J2[0, :])), shape=(nnodes, 2 * (nnodes + nelems)),
    )

    # assemble
    Ab_local = np.reshape(
        np.concatenate((Ab_loc, Ab_loc), axis=1).T, (2 * nelems, 1),
    ).T
    K = np.reshape(range(2 * nelems), (2 * nelems, 1)).T
    Ab = ssp.csc_matrix(
        (Ab_local[0, :], (K[0, :], K[0, :])), shape=(2 * nelems, 2 * nelems),
    )

    # compute nodal areas
    nodalareas = np.zeros((nnodes, 1))
    for j in range(nelems - 1):
        nodes_loc = triangles[j, :]
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        nodalareas[nodes_loc] = nodalareas[nodes_loc] + area

    N = nnodes
    E = nelems
    A = ssp.vstack(
        (
            ssp.hstack((A, ssp.csc_matrix((N, N + 2 * E)))),
            ssp.hstack((ssp.csc_matrix((N, N)), A, ssp.csc_matrix((N, 2 * E)))),
            ssp.hstack((ssp.csc_matrix((2 * E, 2 * N)), Ab)),
        ),
    )

    M = ssp.vstack(
        (
            ssp.hstack((A, B.T, ssp.csc_matrix((2 * N + 2 * E, 1)))),
            ssp.hstack((B, ssp.csc_matrix((N, N)), - 1 / 3 * nodalareas)),
            ssp.hstack(
                (ssp.csc_matrix((1, 2 * N + 2 * E)), 1 / 3 * nodalareas.T, 0),
            ),
        ),
    )
    return M.tocsc()


# rechte Seite
def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    b = np.zeros(3 * nnodes + 2 * nelems + 1)

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        f_mid = f(mid[0], mid[1])
        b[nodes_loc] = b[nodes_loc] + area / 3 * f_mid[0]
        b[nnodes + nodes_loc] = b[nnodes + nodes_loc] + area / 3 * f_mid[1]
        b[2 * nnodes + j] = 9 * area * f_mid[0]
        b[2 * nnodes + nelems + j] = 9 * area * f_mid[1]

    return b

def get_condition_number(A):
    ew1, ev = ssp.linalg.eigsh(A, which="LM")
    ew2, ev = ssp.linalg.eigsh(A, sigma=1e-8)   #<--- takes a long time

    ew1 = abs(ew1)
    ew2 = abs(ew2)

    return ew1.max()/ew2.min()

# Mini FEM
def FEM(coord:np.array, triangles:np.array, dirichlet:np.array, f:callable)-> tuple:
    """Routine to calculate PDEs with Mini FEM"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)

    M = stiffness_matrix(coord, triangles)
    b = RHS_vector(coord, triangles, f)

    N = 2 * nelems + 3 * nnodes + 1
    x = np.zeros(N)

    dbnodes = np.unique(dirichlet)
    inodes = np.setdiff1d(range(nnodes), dbnodes)
    dof = np.concatenate(
        (
            inodes,
            nnodes + inodes,
            2 * nnodes + np.array(range(2 * nelems + nnodes + 1)),
        ),
        axis=0,
    )
    ndof = np.size(dof)
    R = ssp.csc_matrix((np.ones(ndof), (dof, np.arange(ndof))), shape=(N, ndof))


    M_inner = (R.transpose() @ M) @ R
    b_inner = R.transpose() @ b
    x[dof] = ssp.linalg.spsolve(M_inner, b_inner)

    u1 = np.array([x[0:nnodes]]).T
    u2 = np.array([x[nnodes : 2 * nnodes]]).T
    u = np.concatenate((u1, u2), axis=1)
    return x, u


# TODO
def precond_matrix(coord, triangles, dirichlet,red_ref,f):
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    Ab_loc = np.zeros((nelems, 1))  # bubble part
    Blocal = np.zeros((nelems, 3, 8))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    J1 = np.zeros((nelems, 3, 8))
    J2 = np.zeros((nelems, 3, 8))

    # compute local matrices
    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)

        # P1 part
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        I1[j, :, :] = np.stack((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.stack((nodes_loc, nodes_loc, nodes_loc), axis=1)

        # bubble part
        Ab_loc[j] = area / 180 * np.sum((np.matmul(grads, grads.T)).diagonal())

        # local matrix B
        tmp3 = np.array([np.concatenate((grads[:, 0], grads[:, 1]), axis=0).T])
        Blocal[j, :, :] = (
            area
            * (1 / 3)
            * np.concatenate(
                (np.concatenate((tmp3, tmp3, tmp3), axis=0), (-1 / 20) * grads),
                axis=1,
            )
        )
        J1[j, :, :] = np.matlib.repmat(np.array([nodes_loc]).T, 1, 8)
        J2[j, :, :] = np.concatenate(
            (
                np.array([nodes_loc]),
                np.array([nnodes + nodes_loc]),
                np.array([[2 * nnodes + j]]),
                np.array([[2 * nnodes + nelems + j]]),
            ),
            axis=1,
        )

    # assemble
    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    A = ssp.csc_matrix((Alocal[0, :], (I1[0, :], I2[0, :])), shape=(nnodes, nnodes))

    # assemble
    Blocal = np.reshape(Blocal, (24 * nelems, 1)).T
    J1 = np.reshape(J1, (24 * nelems, 1)).T
    J2 = np.reshape(J2, (24 * nelems, 1)).T
    B = ssp.csc_matrix(
        (Blocal[0, :], (J1[0, :], J2[0, :])), shape=(nnodes, 2 * (nnodes + nelems)),
    )

    # assemble
    Ab_local = np.reshape(
        np.concatenate((Ab_loc, Ab_loc), axis=1).T, (2 * nelems, 1),
    ).T
    K = np.reshape(range(2 * nelems), (2 * nelems, 1)).T
    Ab = ssp.csc_matrix(
        (Ab_local[0, :], (K[0, :], K[0, :])), shape=(2 * nelems, 2 * nelems),
    )

    # vertices
    N = nnodes
    # triangles
    E = nelems
    A = ssp.vstack(
        (
            ssp.hstack((A, ssp.csc_matrix((N, N + 2 * E)))),
            ssp.hstack((ssp.csc_matrix((N, N)), A, ssp.csc_matrix((N, 2 * E)))),
            ssp.hstack((ssp.csc_matrix((2 * E, 2 * N)), Ab)),
        ),
    )
    # auf Freiheitsgrade einschränken
    # dann erst cholesky
    A = A.tocsc()

    dbnodes = np.unique(dirichlet)
    inodes = np.setdiff1d(range(N), dbnodes)
    ninodes = np.size(inodes)
    dof = np.concatenate(
        (inodes, nnodes + inodes, 2 * nnodes + np.arange(2 * nelems)), axis=0,
    )
    ndof = np.size(dof)
    R = ssp.csc_matrix(
        (np.ones(ndof), (dof, np.arange(0, ndof))), shape=(2 * N + 2 * E, ndof),
    )
    # A0_inv@A needs to be pos definit -> after n=2 not anymore
    A = (R.transpose() @ A) @ R
    b = RHS_vector(coord, triangles, f)
    b = R.transpose().dot(b)

    u_ini = np.zeros(b.shape[0])

    for _ in range(2):
        u, A = iterative(A, b, red_ref, u_ini)
        u_ini = u

    #L = ichol(A)
    #L_t = ssp.csc_matrix.transpose(L)
    #A0 = L @ L_t
    #A0_inv = ssp.linalg.inv(A0)
    ilu = ssp.linalg.spilu(A,drop_tol=1e-11, fill_factor=2)
    A0_inv = ssp.linalg.inv(ilu.L @ ssp.csc_matrix.transpose(ilu.L)) #ssp.linalg.inv(A0)
    #ssp.linalg.eigs(A0_inv, k=1, which="SM", return_eigenvectors=False)
    #print(get_condition_number(A))
    #print(get_condition_number(A0_inv@A))

    B = B @ R

    # compute nodal areas
    nodalareas = np.zeros((nnodes, 1))
    for j in range(nelems - 1):
        nodes_loc = triangles[j, :]
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        nodalareas[nodes_loc] = nodalareas[nodes_loc] + area
    #nodalareas = nodalareas[inodes]

    M_pre = ssp.vstack(
        (
            ssp.hstack(
                (A0_inv @ A, A0_inv @ B.T, ssp.csr_matrix((2 * ninodes + 2 * E, 1))),
            ),
            ssp.hstack(
                (B @ A0_inv @ A - B, B @ A0_inv @ B.T, -1/3 * nodalareas),
            ),
            ssp.hstack(
                (ssp.csr_matrix((1, 2 * ninodes + 2 * E)),
                 1 / 3 * nodalareas.T, 0),
            ),
        ),
    )

    M = ssp.vstack(
        (
            ssp.hstack(
                (A, B.T, ssp.csr_matrix((2 * ninodes + 2 * E, 1))),
            ),
            ssp.hstack(
                (B, ssp.csc_matrix((N, N)), -1/3 * nodalareas),
            ),
            ssp.hstack(
                (ssp.csr_matrix((1, 2 * ninodes + 2 * E)),
                 1 / 3 * nodalareas.T, 0),
            ),
        ),
    )

    P = ssp.vstack(
        (
            ssp.hstack(
                (A0_inv, ssp.csc_matrix((2 * ninodes + 2 * E, N)),
                 ssp.csr_matrix((2 * ninodes + 2 * E, 1))),
            ),
            ssp.hstack(
                (B @ A0_inv, -ssp.eye(N), ssp.csr_matrix((N,1))),
            ),
            ssp.hstack(
                (ssp.csr_matrix((1, 2 * ninodes + 2 * E)),
                 ssp.csr_matrix((1,N)), 1),
            ),
        ),
    )
    P = P.tocsr()
    #P_2 = (M_pre@ssp.linalg.inv(M)).tocsr()
    #mask = np.abs(P_2.data)<10e-8
    #P_2.data[mask] = 0
    #P_2.eliminate_zeros()
    #P_2.data=np.round(P_2.data,2)
    #P = P.todense()
    #P_2 = P_2.todense()
    #print(P)
    #print(P_2)
    #np.savetxt("values_P.csv", P, fmt="%f", delimiter=",")
    #np.savetxt("values_p2.csv", P_2, fmt="%f", delimiter=",")
    #M_pre.data=np.round(M_pre.data,2)
    PM = P@M.tocsr()
    #PM.data=np.round(PM.data,2)
    #print(M_pre.todense(), "\n")
    #print(PM.todense())
    return M,P



def cg_method(coord, triangles, dirichlet, f):
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    M_inner,P_inv = precond_matrix(coord, triangles, dirichlet,4,f)
    b = RHS_vector(coord, triangles, f)
    N = 2 * nelems + 3 * nnodes + 1
    x = np.zeros(N)

    dbnodes = np.unique(dirichlet)
    inodes = np.setdiff1d(range(nnodes), dbnodes)
    dof = np.concatenate(
        (
            inodes,
            nnodes + inodes,
            2 * nnodes + np.array(range(2 * nelems + nnodes + 1)),
        ),
        axis=0,
    )
    ndof = np.size(dof)
    R = ssp.csc_matrix((np.ones(ndof), (dof, np.arange(ndof))), shape=(N, ndof))

    b_inner = R.transpose() @ b
    b_inner = P_inv.dot(b_inner)

    x[dof],_ = ssp.linalg.cg(M_inner, b_inner)
    u1 = np.array([x[0:nnodes]]).T
    u2 = np.array([x[nnodes : 2 * nnodes]]).T
    u = np.concatenate((u1, u2), axis=1)
    return x, u


# H1 Fehler
def compute_H1_error(coord, triangles, gruex, u):
    H1errSq = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])

        gru_ex_mid = gruex(mid[0], mid[1])
        gruh = (
            u[nodes_loc[0]] * grads[0, :]
            + u[nodes_loc[1]] * grads[1, :]
            + u[nodes_loc[2]] * grads[2, :]
        )
        e = gru_ex_mid - gruh

        H1errSq = H1errSq + area * (e[0] ** 2 + e[1] ** 2)

    return H1errSq


def contour_plot(coord, u_fem, u_cg):
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.tricontourf(coord[:, 0],
        coord[:, 1],
        u_fem[:,0],
        )
    ax1.set_title("fem")
    ax2.tricontourf(coord[:, 0],
        coord[:, 1],
        u_cg[:,0],
        )
    ax2.set_title("preconditioned cg")

    plt.show()


def three_d_plot(coord,u,s):
    plt.figure(figsize=(10, 5))
    ax = plt.axes(projection="3d")
    ax.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u[:, 0],
        triangles=triangles,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    ax.set_title(s)
    plt.show()


#######################################################################
#######################################################################
#######################################################################



#def f(x, y):
#    return np.array([1, 0])

# Dirichlet Rand
def u_D(x):
    return np.zeros(x.shape[0])
def u_h(x,y):
    return (x-x**2)*(y-y**2)
def u_org(x):
    return 2*(x[:,0]-x[:,0]**2+x[:,1]-x[:,1]**2)
def ux(x):
    return x[:,1]*(x[:,1]-1)*(2*x[:,0]-1)
def uy(x):
    return x[:,0]*(x[:,0]-1)*(2*x[:,1]-1)

# Example problem 2
def u1_exact(x, y):
    return 20 * x * y ** 4 - 4 * x ** 5
def u2_exact(x, y):
    return 20 * x ** 4 * y - 4 * y ** 5
def f(x, y):
    return np.array([0, 0])
# Dirichlet Rand
def uD(x, y):
    return np.array([20 * x * y ** 4 - 4 * x ** 5,
                     20 * x ** 4 * y - 4 * y ** 5])
# Gradient für H1 Fehler
def gruex1(x, y):
    return np.array([20 * y ** 4 - 20 * x ** 4,
                     80 * x * y ** 3])
# Gradient für H1 Fehler
def gruex2(x, y):
    return np.array([80 * x ** 3 * y,
                     20 * x ** 4 - 20 * y ** 4])


if __name__ == "__main__":
    nref = 4
    max_err = np.zeros(nref)
    H1_err = np.zeros(nref)
    max_err_cg = np.zeros(nref)
    H1_err_cg = np.zeros(nref)
    diam = np.zeros(nref)
    coord, triangles, dirichlet, neumann = get_geometry(5)

    for j in range(nref):
        red_tuple = red_refine(coord, triangles, dirichlet, neumann)
        [coord, triangles, dirichlet, _, _, _] = red_tuple
        x,u =FEM(coord,triangles,dirichlet,u_h)
        x_cg, u_cg = cg_method(coord, triangles, dirichlet, u_h)
        #H1_err[j]=np.sqrt(compute_H1_error(coord,triangles,gruex1,u[:,0])
        #            +compute_H1_error(coord,triangles,gruex2,u[:,1]))
        #max_err[j] = np.max(np.abs(u1_at_nodes - u[:, 0]))
        diam[j] = 0.5 ** (1 + j)


    contour_plot(coord, u, u_cg)
    three_d_plot(coord,u,"fem")
    three_d_plot(coord,u_cg,"cg")

    ## plot the solution graph (FEM and exact)


