
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d):
    # 2d Einheitsquadrat
    if d == 2:
        coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    # L-Gebiet
    elif d == 4:
        coord = np.array(
            [[0, 0], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]],
        )
        triangles = np.array(
            [[0, 1, 2], [0, 2, 3], [5, 0, 3], [5, 3, 4], [6, 0, 5], [6, 7, 0]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 0]],
        )
        neumann = np.zeros([0, 2])
    elif d == 5:
        # Quadrat auf (-1,1)^2
        # mit inhomogenen Dirichlet Randbedingungen
        coord = np.array([[-1, -1], [1, -1], [1, 1], [-1, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


# Mini FEM
def FEM(coord, triangles, dirichlet, f, uD):
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)

    M = stiffness_matrix(coord, triangles)
    b = RHS_vector(coord, triangles, f)

    x = np.zeros(3 * nnodes + 2 * nelems + 1)

    dbnodes = np.unique(dirichlet)

    for j in dbnodes:
        coord_loc = coord[j, :]
        tmp = uD(coord_loc[0], coord_loc[1])
        x[j] = tmp[0]
        x[nnodes + j] = tmp[1]

    b = b - M.dot(x)

    inodes = np.setdiff1d(range(nnodes), dbnodes)
    dof = np.concatenate(
        (
            inodes,
            nnodes + inodes,
            2 * nnodes + np.array(range(2 * nelems + nnodes + 1)),
        ),
        axis=0,
    )
    M_inner = M[np.ix_(dof, dof)]
    b_inner = b[dof]
    x[dof] = ssp.linalg.spsolve(M_inner, b_inner)

    u1 = np.array([x[0:nnodes]]).T
    u2 = np.array([x[nnodes : 2 * nnodes]]).T
    u = np.concatenate((u1, u2), axis=1)
    return x, u


def get_area(coord):
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc):
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord, triangles):
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    Ab_loc = np.zeros((nelems, 1))  # bubble part
    Blocal = np.zeros((nelems, 3, 8))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    J1 = np.zeros((nelems, 3, 8))
    J2 = np.zeros((nelems, 3, 8))
    j = 0

    # compute local matrices
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)

        # P1 part
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        I1[j, :, :] = np.stack((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.stack((nodes_loc, nodes_loc, nodes_loc), axis=1)

        # bubble part
        Ab_loc[j] = area / 180 * np.sum((np.matmul(grads, grads.T)).diagonal())

        # local matrix B
        tmp3 = np.array([np.concatenate((grads[:, 0], grads[:, 1]), axis=0).T])
        Blocal[j, :, :] = (
            area
            * (1 / 3)
            * np.concatenate(
                (np.concatenate((tmp3, tmp3, tmp3), axis=0), (1 / 60) * grads),
                axis=1,
            )
        )
        J1[j, :, :] = np.matlib.repmat(np.array([nodes_loc]).T, 1, 8)
        J2[j, :, :] = np.concatenate(
            (
                np.array([nodes_loc]),
                np.array([nnodes + nodes_loc]),
                np.array([[2 * nnodes + j]]),
                np.array([[2 * nnodes + nelems + j]]),
            ),
            axis=1,
        )
        j += 1

    # assemble
    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    A = ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])), shape=(nnodes, nnodes))

    # assemble
    Blocal = np.reshape(Blocal, (24 * nelems, 1)).T
    J1 = np.reshape(J1, (24 * nelems, 1)).T
    J2 = np.reshape(J2, (24 * nelems, 1)).T
    B = ssp.csr_matrix(
        (Blocal[0, :], (J1[0, :], J2[0, :])), shape=(nnodes, 2 * (nnodes + nelems)),
    )

    # assemble
    Ab_local = np.reshape(
        np.concatenate((Ab_loc, Ab_loc), axis=1).T, (2 * nelems, 1),
    ).T
    K = np.reshape(range(2 * nelems), (2 * nelems, 1)).T
    Ab = ssp.csr_matrix(
        (Ab_local[0, :], (K[0, :], K[0, :])), shape=(2 * nelems, 2 * nelems),
    )

    # compute nodal areas
    nodalareas = np.zeros((nnodes, 1))
    for j in range(nelems - 1):
        nodes_loc = triangles[j, :]
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        nodalareas[nodes_loc] = nodalareas[nodes_loc] + area

    N = nnodes
    E = nelems
    A = ssp.vstack(
        (
            ssp.hstack((A, ssp.csr_matrix((N, N + 2 * E)))),
            ssp.hstack((ssp.csr_matrix((N, N)), A, ssp.csr_matrix((N, 2 * E)))),
            ssp.hstack((ssp.csr_matrix((2 * E, 2 * N)), Ab)),
        ),
    )

    A = ssp.vstack(
        (
            ssp.hstack((A, B.T, ssp.csr_matrix((2 * N + 2 * E, 1)))),
            ssp.hstack((B, ssp.csr_matrix((N, N)), 1 / 3 * nodalareas)),
            ssp.hstack(
                (ssp.csr_matrix((1, 2 * N + 2 * E)), 1 / 3 * nodalareas.T, 0),
            ),
        ),
    )
    return A.tocsr()


# rechte Seite
def RHS_vector(coord, triangles, f):
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    b = np.zeros(3 * nnodes + 2 * nelems + 1)
    j = 0

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        f_mid = f(mid[0], mid[1])
        b[nodes_loc] = b[nodes_loc] + area / 3 * f_mid[0]
        b[nnodes + nodes_loc] = b[nnodes + nodes_loc] + area / 3 * f_mid[1]
        b[2 * nnodes + j] = 9 * area * f_mid[0]
        b[2 * nnodes + nelems + j] = 9 * area * f_mid[1]
        j += 1

    return b


# H1 Fehler
def compute_H1_error(coord, triangles, gruex, u):
    H1errSq = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])

        gru_ex_mid = gruex(mid[0], mid[1])
        gruh = (
            u[nodes_loc[0]] * grads[0, :]
            + u[nodes_loc[1]] * grads[1, :]
            + u[nodes_loc[2]] * grads[2, :]
        )
        e = gru_ex_mid - gruh

        H1errSq = H1errSq + area * (e[0] ** 2 + e[1] ** 2)

    return H1errSq


#######################################################################
#######################################################################
#######################################################################


def fun(x, y):
    return 20 * x * y ** 4 - 4 * x ** 5
u1_exact = np.vectorize(fun)
def fun(x, y):
    return 20 * x ** 4 * y - 4 * y ** 5
u2_exact = np.vectorize(fun)
def f(x, y):
    return np.array([0, 0])
# Dirichlet Rand
def uD(x, y):
    return np.array([20 * x * y ** 4 - 4 * x ** 5, 20 * x ** 4 * y - 4 * y ** 5])

# Gradient für H1 Fehler
def gruex1(x, y):
    return np.array([20 * y ** 4 - 20 * x ** 4, 80 * x * y ** 3])  # gradient für H1 Fehler
def gruex2(x, y):
    return np.array([80 * x ** 3 * y, 20 * x ** 4 - 20 * y ** 4])

nref = 3
max_err = np.zeros(nref)
H1_err = np.zeros(nref)
diam = np.zeros(nref)
coord, triangles, dirichlet, neumann = get_geometry(5)

for j in range(nref):
    coord, triangles, dirichlet, _, _, _ = red_refine(
        coord, triangles, dirichlet, neumann,
    )
    x, u = FEM(coord, triangles, dirichlet, f, uD)
    u1_at_nodes = u1_exact(coord[:, 0], coord[:, 1])
    u2_at_nodes = u2_exact(coord[:, 0], coord[:, 1])
    max_err[j] = np.max(np.abs(u1_at_nodes - u[:, 0]))
    H1_err[j] = np.sqrt(
        compute_H1_error(coord, triangles, gruex1, u[:, 0])
        + compute_H1_error(coord, triangles, gruex2, u[:, 1]),
    )
    diam[j] = 0.5 ** (1 + j)
print("Maximumsfehlernorm: ", max_err)
print("H1-Fehlernorm: ", H1_err)

## plot the solution graph (FEM and exact)

fig = plt.figure(figsize=(10, 5))
ax = plt.axes(projection="3d")
trisurf = ax.plot_trisurf(
    coord[:, 0],
    coord[:, 1],
    u[:, 0],
    triangles=triangles,
    cmap=plt.get_cmap("summer"),
    edgecolor="Gray",
)
ax.set_title("finite element solution")
plt.show(block=False)

def uex(x, y):
    return (x - x ** 2) * (y - y ** 2)
func2 = np.vectorize(uex)

fig, ax = plt.subplots()
ax.quiver(coord[:, 0], coord[:, 1], u[:, 0], u[:, 1])
ax.set_title("velocity vector field FEM")
plt.show()
plt.loglog(H1_err, max_err, diam, H1_err)
plt.legend(["max", "H1"])
plt.show()
