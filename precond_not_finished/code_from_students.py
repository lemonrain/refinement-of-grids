#Programmcode zum cg-Verfahren

#_________________________
#_________________________
#Aufgabenteil a
#_________________________
#_________________________
#mit Abbruchkriterium
#_________________________

import numpy as np

n=10 #Dimension der Matrix
A = np.empty((n, n))
b= [5 for x in range(n)] #rechte Seite
x= [1 for x in range(n)] #Startvektor
for i in range(n): #Hilbertmatrix
    for j in range(n):
        A[i][j]=1/(i+j+1)

s=np.linalg.solve(A,b) #Referenzl�sung
e=np.sqrt(((x-s).T).dot(A.dot(x-s))) #Fehler im ersten Schritt

def cg(A, b, x):
    r_0=b - A.dot(x)
    p=r_0
    result=42
    k=0
    while result > 1e-11:
        alpha=np.dot(r_0,r_0)/np.dot(p,A.dot(p))
        x=x+alpha*p
        r_1=r_0-alpha* A.dot(p)
        beta=np.dot(r_1,r_1)/np.dot(r_0,r_0)
        r_0=r_1
        p=r_0+beta*p
        result=np.dot(r_0,r_0) #Residuumsnorm
        fehler= (np.sqrt(((x-s).T).dot(A.dot(x-s))))/e #Fehler
        print(fehler," ")
        k+=1 #Iterationsschritte z�hlen
    print(k)

cg(A, b, x)


#________________________
#mit Grafikausgabe
#________________________

import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg as nl

n=4 #Dimension der Matrix
m=10 #Iterationsschritte
A = np.empty((n, n))
b= np.ones(n)
x= np.ones(n)
xAchse=np.empty(m)
yAchse=np.empty(m)
y = np.empty(m)
z = nl.cond(A, 2) #Konditionierung der Matrix
for i in range(n): #A initialisieren
    for j in range(n):
        A[i][j]=1/(i+j+1)

for j in range(m):
    xAchse[j]=j+1 #x-Achse als iterationsschritte in ganzen Zahlen
    y[j] = 2*((np.sqrt(z)-1)/(np.sqrt(z)+1))**j #Berechnung Fehlerabsch�tzung
    j=j+1

s=np.linalg.solve(A,b) #Referenzl�sung
e=np.sqrt(((x-s).T).dot(A.dot(x-s))) #Fehler im ersten Schritt

def cg(A, b, x):
    r_0=b - A.dot(x)
    p=r_0
    k=1
    for k in range(m):
        alpha=np.dot(r_0,r_0)/np.dot(p,A.dot(p))
        x=x+alpha*p
        r_1=r_0-alpha* A.dot(p)
        beta=np.dot(r_1,r_1)/np.dot(r_0,r_0)
        r_0=r_1
        p=r_0+beta*p
        fehler= (np.sqrt(((x-s).T).dot(A.dot(x-s))))/e
        yAchse[k]= fehler #Fehler auf y-Achse auftragen statt Ausgabe
        k+=1

cg(A, b, x)
print(z)

plt.scatter(xAchse, yAchse) #plot Fehler in unserer Berechnung
plt.plot(xAchse, y) #plot Fehlerabsch�tzung
plt.yscale("log")
plt.title("Fehler �ber Iterationen")
plt.grid(True)





#__________________________
#__________________________
#Aufgabenteil b
#__________________________
#__________________________
#mit Abbruchkriterium
#__________________________

import numpy as np
from scipy.sparse import dia_matrix  #Paket f�r d�nnbesetzte Matrizen
from scipy.sparse import linalg as ssl

n=1000
A = dia_matrix((n, n))
b= [0 for x in range(n)] #rechte Seite
x= [1 for x in range(n)] #Startvektor
ex = np.ones(n)
data = np.array([-ex, -ex, 4*ex, -ex, -ex])
offsets = np.array([-2, -1, 0, 1, 2])
B=dia_matrix((data, offsets), shape=(n, n)).toarray() #Bandmatrix initialisieren


def cg(A, b, x):
    r_0=b - A.dot(x) #Residuum
    p=r_0
    result=42
    k=0
    while result > 1e-11:
        alpha=np.dot(r_0,r_0)/np.dot(p, A.dot(p))
        x=x+alpha*p
        r_1=r_0 - alpha*A.dot(p)
        beta=np.dot(r_1,r_1)/np.dot(r_0,r_0)
        r_0=r_1
        p=r_0+beta*p
        result=np.dot(r_0,r_0) #Residuumsnorm
        k+=1 #Iterationsschritte z�hlen
    print(k)

cg(B,b,x)
z=ssl.spsolve(B,b) #Referenzl�sung
fehler= (np.sqrt(((x-z).T).dot(B.dot(x-z))))


#__________________________
#mit Grafikausgabe
#__________________________

import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg as nl
from scipy.sparse import dia_matrix
from scipy.sparse import linalg as ssl

n=8000 #Dimension der Matrix
m=2500 #Iterationsschritte

A = dia_matrix((n, n))
b= [0 for x in range(n)]
x= [1 for x in range(n)]
ex = np.ones(n)
data = np.array([-ex, -ex, 4*ex, -ex, -ex])
offsets = np.array([-2, -1, 0, 1, 2])
B=dia_matrix((data, offsets), shape=(n, n)).toarray() #Bandmatrix

y = np.empty(m) #Vektor f�r Fehlerabsch�tzung
z = nl.cond(B, 2) #Konditionszahl

xAchse=np.empty(m)
yAchse=np.empty(m)
for j in range(m):
    xAchse[j]=j+1 #Iterationsschritte
    y[j] = 2*((np.sqrt(z)-1)/(np.sqrt(z)+1))**j #FEhlerabsch�tzung
    j=j+1


def cg(A, b, x):
    r_0=b - A.dot(x)
    p=r_0
    for k in range(m):
        alpha=np.dot(r_0,r_0)/np.dot(p, A.dot(p))
        x=x+alpha*p
        r_1=r_0 - alpha*A.dot(p)
        beta=np.dot(r_1,r_1)/np.dot(r_0,r_0)
        r_0=r_1
        p=r_0+beta*p
        result=np.dot(r_0,r_0)
        yAchse[k]= result #Residuumsnorm auf yAchsen Vektor
        k+=1
    print(k)

cg(B,b,x)
z=ssl.spsolve(B,b) #Referenzl�sung
fehler= (np.sqrt(((x-z).T).dot(B.dot(x-z))))
print(fehler) #absoluter Fehler im letzten Schritt

#graphische Ausgabe
plt.scatter(xAchse, yAchse) #Fehler in unserer Berechnung
plt.plot(xAchse, y) #Fehlerabsch�tzung
plt.yscale("log")
plt.title("Fehler �ber Iterationen")
plt.grid(True)





#_________________________
#_________________________
#Aufgabenteil c
#_________________________

import numpy as np
from scipy.sparse import dia_matrix  #Paket f�r d�nnbesetzte Matrizen
from scipy.sparse import linalg as ssl

n=1000
D= [0.0 for x in range(n)]
b= [5 for x in range(n)]
x= [1 for x in range(n)]
ex = np.ones(n)
data = np.array([-ex, -ex, 4*ex, -ex, -ex])
offsets = np.array([-2, -1, 0, 1, 2])
A=dia_matrix((data, offsets), shape=(n, n)).toarray()          #Bandmatrix initialisieren
data = np.array([0*ex, 0*ex, 0*ex])
offsets = np.array([-2, -1, 0])
L=dia_matrix((data, offsets), shape=(n, n)).toarray()          #Bandmatrix initialisieren



def unvollCholesky(M, m):       #unvollst�ndige Cholesky-Zerlegung
    for s in range(m):
        D[s] = M[s][s]
        for k in range(s):
            D[s] = D[s] - D[k]*L[s][k]*L[s][k]
        for z in range(s+1, m):
            if M[z][s]!=0:
                L[z][s] = M[z][s]
                for u in range(s):
                    L[z][s] = L[z][s] - L[z][u]*L[s][u]*D[u]
                L[z][s] = L[z][s]/D[s]
        L[s][s] = 1
    for a in range(m):
        for b in range(a+1):
            L[a][b] = L[a][b]*np.sqrt(D[b])
    return L



s=ssl.spsolve(A,b)          #Vergleichswerte, um die Fehler des pcg-Verfahrens zu ermitteln
e=np.sqrt(((x-s).T).dot(A.dot(x-s)))          #Vergleichswerte, um die Fehler des pcg-Verfahrens zu ermitteln
def pcg(A, b, x):           #pcg-Verfahren
    L = unvollCholesky(A, n)            #Aufruf unvollst�ndige Cholesky-Zerlegung
    r=b - A.dot(x)          #Initialisierung der Variablen
    q_1=ssl.spsolve(L.T,r)
    q=ssl.spsolve(L,q_1)
    p=q
    result=42
    k=1
    while result > 1e-11:           #Abbruchkriterium
        z=A.dot(p)
        alpha=np.dot(r,q)/np.dot(p,z)
        x=x+alpha*p
        r_1=r-alpha*z
        w=ssl.spsolve(L.T,r_1)
        q_1=ssl.spsolve(L,w)
        beta=np.dot(r_1,q_1)/np.dot(r,q)
        r=r_1
        q=q_1
        p=q+beta*p
        result=np.dot(r,r)
        fehler= (np.sqrt(((x-s).T).dot(A.dot(x-s))))/e          #Fehlerabgleich mittels den Vergleichswerten
        print(fehler," ")           #Ausgabe der Fehlerentwicklung
        k+=1
    print(x)            #Ausgabe der numerischen L�sung
    print(k)            #Ausgabe der Anzahl an Iterationen




#HAUPTPROGRAMM PCG
pcg(A, b, x)
