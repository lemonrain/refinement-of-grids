"""Subroutine for the rgb_refinement function"""
import numpy as np


def edge_data_2d(triangles:np.array, dirichlet:np.array) -> tuple:
    """Calculates all edges between nodes

    Args:
        triangles: manner of the triangles of the triangulation
        dirichlet: dirichlet boundary values


    Returns:
        edges as well as matrices to regain old form after refinement

    """
    nE = triangles.shape[0]
    new_edges = 3 * nE
    new_dirichlet = dirichlet.shape[0]
    edges = np.stack(
        (
            triangles[:, 1:3],
            np.stack((triangles[:, 0], triangles[:, -1]), axis=1),
            triangles[:, 0:2],
        ),
        axis=1,
    )
    edges = np.vstack((np.reshape(edges, (-1, 2)), dirichlet))
    edges, edge_numbers = np.unique(np.sort(edges), return_inverse=True, axis=0)
    el2edges = np.reshape(edge_numbers[0:new_edges], (-1, 3))
    Db2edges = np.reshape(
        edge_numbers[new_edges : new_dirichlet + new_edges], (-1, 1),
    )
    return edges, el2edges, Db2edges
