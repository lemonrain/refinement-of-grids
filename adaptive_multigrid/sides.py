"""Subroutine for the comp_estimators function"""
import numpy as np


def sides(triangles:np.array, dirichlet:np.array, neumann:np.array) -> tuple:
    """Provides data structures related to the sides of elements

    Args:
        triangles: manner of the triangles of the triangulation
        dirichlet: dirichlet boundary values
        neumann: neumann boundary values

    Returns:
        The sides themselves, their signs and the sides specific to each triangle
        and boundary. With sides we mean basically each line in the triangulation
        and for example in triangles it means the edges of each individual triangle
        The sign of a side is positve if it belongs to the boundary

    """
    nE = triangles.shape[0]
    d = triangles.shape[1] - 1
    nD = dirichlet.shape[0]
    nN = neumann.shape[0]
    # dimension = 2
    if d == 2:
        t_sides = np.vstack(
            (
                triangles[:, 1:3],
                np.stack((triangles[:, -1], triangles[:, 0]), axis=1),
                triangles[:, 0:2],
            ),
        )
    # dimension = 3
    else:
        t_sides = np.vstack(
            (
                np.stack(
                    (triangles[:, 1], triangles[:, 3], triangles[:, 2]), axis=1,
                ),
                np.stack(
                    (triangles[:, 0], triangles[:, 2], triangles[:, 3]), axis=1,
                ),
                np.stack(
                    (triangles[:, 0], triangles[:, 3], triangles[:, 1]), axis=1,
                ),
                np.stack(
                    (triangles[:, 0], triangles[:, 1], triangles[:, 2]), axis=1,
                ),
            ),
        )

    triangles_sides, i2, j = np.unique(
        np.sort(t_sides), return_index=True, return_inverse=True, axis=0,
    )
    sides = np.reshape(j, (nE, d + 1), order="F")
    nS = triangles_sides.shape[0]
    sign_sides = np.ones((d + 1) * nE)
    sign_sides[i2] = -1
    sign_sides = np.reshape(sign_sides, (nE, d + 1), order="F")
    _, j2 = np.unique(
        np.sort(np.vstack((triangles_sides, dirichlet, neumann))),
        return_inverse=True,
        axis=0,
    )
    sides_dirichlet = j2[range(nS, nS + nD)]
    sides_neumann = j2[range(nS + nD, nS + nD + nN)]

    sides_edges = np.zeros((triangles_sides.shape[0], 2))
    sides_edges[:, 0] = np.remainder(i2, nE) + 1

    i_inner = np.setdiff1d(range((d + 1) * nE), i2)
    sides_edges[j[i_inner], 1] = np.remainder(i_inner, nE) + 1
    return (
        sides,
        sign_sides,
        triangles_sides,
        sides_dirichlet,
        sides_neumann,
        sides_edges,
    )


# test to see how the subroutine works on its own
if __name__ == "__main__":
    coord = np.array(
        [[-1, -1], [0, -1], [-1, 0], [0, 0], [1, 0], [-1, 1], [0, 1], [1, 1]],
    )
    triangles = np.array(
        [[0, 1, 3], [3, 2, 0], [2, 3, 6], [6, 5, 2], [3, 4, 7], [7, 6, 3]],
    )
    dirichlet = np.array(
        [[0, 1], [1, 3], [3, 4], [4, 7], [7, 6], [6, 5], [5, 2], [2, 0]],
    )
    neumann = np.zeros([0, 2])
    a = sides(triangles, dirichlet, neumann)
