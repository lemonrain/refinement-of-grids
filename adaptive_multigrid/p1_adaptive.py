"""Routine to make an adapitve rgb refined grid with example functions"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from comp_estimator import comp_estimators
from rgb_refinement import rgb_refinement
from scipy.sparse.linalg import spsolve


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    # 2d Einheitsquadrat
    if d == 2:
        coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
        triangles = np.array([[2, 0, 1], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    # L-Gebiet
    elif d == 4:
        coord = np.array(
            [[0, 0], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]],
        )
        triangles = np.array(
            [[2, 0, 1], [0, 2, 3], [3, 5, 0], [5, 3, 4], [6, 0, 5], [0, 6, 7]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 0]],
        )
        neumann = np.zeros([0, 2])
    # anderes L-Gebiet
    elif d == 5:
        coord = np.array(
            [[-1, -1], [0, -1], [-1, 0], [0, 0], [1, 0], [-1, 1], [0, 1], [1, 1]],
        )
        triangles = np.array(
            [[0, 1, 3], [3, 2, 0], [2, 3, 6], [6, 5, 2], [3, 4, 7], [7, 6, 3]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 3], [3, 4], [4, 7], [7, 6], [6, 5], [5, 2], [2, 0]],
        )
        neumann = np.zeros([0, 2])

    return coord, triangles, dirichlet, neumann


# Darstellung des Ergebnisses
def show(coord:np.array, triangles:np.array, u:np.array, error_bound:float,
         eps_stop:float) -> None:
    """Plot routine"""
    plt.close()
    plt.figure()
    ax_fe = plt.axes(projection="3d")

    my_cmap = plt.get_cmap("viridis")

    ax_fe.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=triangles,
        linewidth=0.1,
        cmap=my_cmap,
        edgecolor="Black",
    )

    # Set labels in plot
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.view_init(90, 0)
    plt.draw()
    if error_bound > eps_stop:
        plt.pause(0.30)
    else:
        ax_fe.set_title("Error bound reached")
        plt.show()


def get_area(coord:np.array) -> float:
    """Calculation of the area of a trinagle"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        # local stiffness matrix
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])),
                          shape=(nnodes, nnodes))


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        b[nodes_loc] = b[nodes_loc] + area / 3 * f(mid[0], mid[1])
    return b


def p1_adaptive(red:int, coord:np.array, dirichlet:np.array, triangles:np.array,
                f:callable, ux:callable, uy:callable) -> tuple:
    """Calculates the solution with FEM over a red green blue triangulation

    Args:
        red: number of refinements
        coord: coordinates of the triangulation
        dirichlet: mapping of the coordinates to the dirichlet boundary
        triangles: mapping of the coordinates to the triangles
        f: right hand side
        ux: partial derivative to x
        uy: partial derivative to y

    Returns:
        Calculates the solution and returns it together with the list of errors and
        the degrees of freedom

    """
    theta = 0.5
    eps_stop = 5e-2
    error_bound = 1
    err_list = []
    dof_list = []
    # generate a base rgb triangulation
    for _ in range(red):
        marked = np.ones(triangles.shape[0])
        [coord, triangles, dirichlet] = rgb_refinement(
            coord, triangles, dirichlet, marked,
        )

    # as long as the error is bigger than a chosen variable
    # continuation of the algorithm
    while error_bound > eps_stop:
        nnodes = coord.shape[0]
        dbnodes = np.unique(dirichlet)
        dof = np.setdiff1d(range(nnodes), dbnodes)
        u = np.zeros(nnodes)
        S = stiffness_matrix(coord, triangles)
        b = RHS_vector(coord, triangles, f)

        # restriction to degrees of freedom
        ndof = np.size(dof)
        R = ssp.csr_matrix(
            (np.ones(ndof), (dof, np.arange(0, ndof))), shape=(nnodes, ndof),
        )
        S_inner = (R.transpose() @ S) @ R
        b_inner = R.transpose() @ b

        # solving of the system
        u[dof] = spsolve(S_inner, b_inner)

        # calculation of the error
        err_list.append(e_norm_fun(coord, triangles, ux, uy, u))

        dof_list.append(ndof)

        eta = comp_estimators(coord, triangles, dirichlet, neumann, u, f)
        error_bound = np.sqrt(np.sum(eta**2))
        show(coord,triangles,u, error_bound, eps_stop)
        marked = eta > theta * np.max(eta)

        # partwise refinement by utilization of the marked array
        if error_bound > eps_stop:
            [coord, triangles, dirichlet] = rgb_refinement(
                coord, triangles, dirichlet, marked,
            )

    return u, err_list, dof_list


def e_norm_fun(coord:np.array, triangles:np.array, ux:callable,
               uy:callable, u:callable) -> float:
    """Function to calculate the energy norm"""
    enorm_err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(coord_loc)
        area = get_area(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        enorm_err = (
            enorm_err
            + (
                (
                    ux(mid[0], mid[1])
                    - u[nodes_loc[0]] * grads[0, 0]
                    - u[nodes_loc[1]] * grads[1, 0]
                    - u[nodes_loc[2]] * grads[2, 0]
                )
                ** 2
                + (
                    uy(mid[0], mid[1])
                    - u[nodes_loc[0]] * grads[0, 1]
                    - u[nodes_loc[1]] * grads[1, 1]
                    - u[nodes_loc[2]] * grads[2, 1]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(enorm_err)

# example problems
if __name__ == "__main__":
    coord, triangles, dirichlet, neumann = get_geometry(4)

    # example for a singular solution
    def fun_sing(x, y):
        """Exact solution"""
        r = np.sqrt(x**2 + y**2)
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        return (1 - x**2) * (1 - y**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)

    def f_sing(x, y):
        """Right hand side"""
        r = np.sqrt(x**2 + y**2)
        if r == 0:
            val = 0
        else:
            phi = np.arctan2(y, x)
            if phi < 0:
                phi = 2 * np.pi + phi
            # Produktformel Laplace(uv)=uLapl v + 2gradu.gradv+v Lapl u
            part1 = (
                -2
                * ((1 - x**2) + (1 - y**2))
                * r ** (2 / 3)
                * np.sin(2 / 3 * phi)
            )
            dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
            dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
            part2 = -2 * x * (1 - y**2) * (
                dudr * x / r - dudphi * np.sin(phi) / r
            ) - 2 * y * (1 - x**2) * (dudr * y / r + dudphi * np.cos(phi) / r)
            val = part1 + 2 * part2
        return -val

    def ux_sing(x, y):
        """Partial derivative"""
        r = np.sqrt(x**2 + y**2)
        if r == 0:
            val = 0
        else:
            phi = np.arctan2(y, x)
            if phi < 0:
                phi = 2 * np.pi + phi
            part1 = -2 * x * (1 - y**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)
            dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
            dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
            part2 = (
                (1 - x**2)
                * (1 - y**2)
                * (dudr * np.cos(phi) - dudphi * np.sin(phi) / r)
            )
            val = part1 + part2
        return val

    def uy_sing(x, y):
        """Partial derivative"""
        r = np.sqrt(x**2 + y**2)
        if r == 0:
            val = 0
        else:
            phi = np.arctan2(y, x)
            if phi < 0:
                phi = 2 * np.pi + phi
            part1 = -2 * y * (1 - x**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)
            dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
            dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
            part2 = (
                (1 - x**2)
                * (1 - y**2)
                * (dudr * np.sin(phi) + dudphi * np.cos(phi) / r)
            )
            val = part1 + part2
        return val

    # Good example for the unit square smooth solution
    def fun_ex_smooth1(x, y):
        """Exact solution"""
        return (x - x ** 2) * (y - y ** 2)
    def f_smooth1(x, y):
        """Right hand side"""
        return 2 * (x - x ** 2 + (y - y ** 2))

    def ux_smooth1(x, y):
        """Partial derivative"""
        return y * (y - 1) * (2 * x - 1)
    def uy_smooth1(x, y):
        """Partial derivative"""
        return x * (x - 1) * (2 * y - 1)

    # Good example for L domain for a smooth solution
    def fun_ex_smooth2(x, y):
        """Exact solution"""
        return np.sin(np.pi * x) * np.sin(np.pi * y)
    def f_smooth2(x, y):
        """Right hand side"""
        return 2 * np.pi ** 2 * np.sin(np.pi * x) * np.sin(np.pi * y)

    def ux_smooth2(x, y):
        """Partial derivative"""
        return np.pi * np.cos(np.pi * x) * np.sin(np.pi * y)
    def uy_smooth2(x, y):
        """Partial derivative"""
        return np.pi * np.cos(np.pi * y) * np.sin(np.pi * x)

    red = 1
    u, enorm_err, dof = p1_adaptive(
        red, coord, dirichlet, triangles, f_smooth2, ux_smooth2, uy_smooth2,
        )

    print("||u-u_k||_E = ", enorm_err)

    fig, ax_err1 = plt.subplots()
    ax_err1.loglog(dof, enorm_err, "-x")
    plt.setp(
        ax_err1,
        xticks=[10**0, 10**1, 10**2, 10**3, 10**4],
        yticks=[10 ** (-2), 10 ** (-1), 10 ** (0), 10**1],
    )
    ax_err1.set_xlabel("$dof$")
    ax_err1.set_ylabel("e-norm error")
    ax_err1.legend(["e-Norm with $u_k$ and $u$"])
    plt.show()
