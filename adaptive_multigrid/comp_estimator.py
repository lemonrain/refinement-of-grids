"""Subroutine for the adaptive approximation of the poisson problem"""
import math

import numpy as np
from sides import sides


def comp_estimators(
        coord:np.array,
        triangles:np.array,
        dirichlet:np.array,
        neumann:np.array,
        u:np.array,
        f: any,
        ) -> np.array:
    """Calculates the refinement indicators

    Args:
        coord: coordinates of the triangulation
        triangles: manner of the triangles of the triangulation
        dirichlet: dirichlet boundary values
        neumann: neumann boundary values
        u: start vector
        f: right hand side to be used

    Returns:
        The refinement indicators

    """
    # get the sides of the triangulation
    side, _, triangles_sides, sides_dirichlet, _, _ = sides(
        triangles, dirichlet, neumann,
    )
    d = coord.shape[1]
    nE = triangles.shape[0]
    nS = triangles_sides.shape[0]
    eta_S = np.zeros(nS)
    eta_T_sq = np.zeros(nE)
    for j, tri, sid in enumerate(zip(triangles, side)):
        X_T = np.vstack((np.ones(d + 1), coord[tri, :].T))
        grads_T = np.linalg.solve(X_T, np.vstack((np.zeros(d), np.identity(d))))
        vol_T = np.linalg.det(X_T) / math.factorial(d)
        h_T = vol_T ** (1 / d)
        mp_T = np.sum(coord[tri, :], axis=0) / (d + 1)
        eta_T_sq[j] = h_T**2 * vol_T * (f(mp_T[0], mp_T[1]))
        nabla_u_T = grads_T.T @ u[tri]
        normal_times_area = -grads_T * vol_T * d
        eta_S[sid] = (
            h_T ** ((2 - d) / 2) * eta_S[sid] + normal_times_area @ nabla_u_T
        )
    eta_S[sides_dirichlet] = 0
    eta_S_T_sq = np.sum(eta_S[side] ** 2, axis=1)
    return np.sqrt(eta_S_T_sq + eta_T_sq)

# test to see how the subroutine works on its own
if __name__ == "__main__":
    coord = np.array(
        [[-1, -1], [0, -1], [-1, 0], [0, 0], [1, 0], [-1, 1], [0, 1], [1, 1]],
    )
    triangles = np.array(
        [[0, 1, 3], [3, 2, 0], [2, 3, 6], [6, 5, 2], [3, 4, 7], [7, 6, 3]],
    )
    dirichlet = np.array(
        [[0, 1], [1, 3], [3, 4], [4, 7], [7, 6], [6, 5], [5, 2], [2, 0]],
    )
    neumann = np.zeros([0, 2])
    u = np.zeros(coord.shape[0])

    def f(x, y):
        x = np.array([[x], [y]])
        return np.ones(x.shape[1])

    eta = comp_estimators(coord, triangles, dirichlet, neumann, u, f)
    print(eta)
