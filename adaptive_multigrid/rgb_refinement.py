"""Routine to calculate the red-green-blue refinement of a triangulation"""
import numpy as np
from edge_data_2d import edge_data_2d


def rgb_refinement(coord:np.array, triangles:np.array,
                   dirichlet:np.array, marked:np.array) -> tuple:
    """Refines the triangulation with the red-green-blue refinement

    Args:
        coord: coordinates of the triangulation
        triangles: manner of the triangles of the triangulation
        dirichlet: dirichlet boundary values
        marked: array containing the edges to be refined


    Returns:
        Refined coord, triangles and dirichlet

    """
    edges, el2edges, Db2edges = edge_data_2d(triangles, dirichlet)
    new_c = coord.shape[0]
    new_e = edges.shape[0]
    tmp = 1
    marked_edges = np.zeros(new_e)
    marked_edges[np.reshape(el2edges[marked == 1, 0:3], (1, -1), order="F")] = 1
    while tmp > 0:
        tmp = np.count_nonzero(marked_edges)
        el2marked_edges = marked_edges[el2edges]
        el2marked_edges[el2marked_edges[:, 0] + el2marked_edges[:, 2] > 0, 1] = 1
        marked_edges[el2edges[el2marked_edges == 1]] = 1
        tmp = np.count_nonzero(marked_edges) - tmp
    new_nodes = np.zeros(new_e)
    new_nodes[marked_edges == 1] = np.arange(np.count_nonzero(marked_edges)) + new_c
    new_ind = new_nodes[el2edges]

    # new indicees
    red = np.all(
        [[new_ind[:, 0] > 0], [new_ind[:, 1] > 0], [new_ind[:, 2] > 0]], axis=0,
    )[0, :]
    blue1 = np.all(
        [[new_ind[:, 0] > 0], [new_ind[:, 1] > 0], [new_ind[:, 2] == 0]], axis=0,
    )[0, :]
    blue3 = np.all(
        [[new_ind[:, 0] == 0], [new_ind[:, 1] > 0], [new_ind[:, 2] > 0]], axis=0,
    )[0, :]
    green = np.all(
        [[new_ind[:, 0] == 0], [new_ind[:, 1] > 0], [new_ind[:, 2] == 0]], axis=0,
    )[0, :]
    remain = np.all(
        [[new_ind[:, 0] == 0], [new_ind[:, 1] == 0], [new_ind[:, 2] == 0]], axis=0,
    )[0, :]

    # red refinement
    #     .              .
    #    / \            / \
    #   /   \          .---.
    #  /     \        / \ / \
    # .-------.      .---.---.
    triangles_red = np.vstack(
        (
            triangles[red, 0],
            new_ind[red, -1:0:-1].T,
            np.stack((new_ind[red, 1], new_ind[red, 0]), axis=1).T,
            triangles[red, 2],
            new_ind[red, 2],
            triangles[red, 1],
            new_ind[red, 0],
            new_ind[red, :].T,
        ),
    )
    triangles_red = np.reshape(triangles_red, (3, -1), order="F").T
    # blue refinement
    #     .              .
    #    / \            /|\
    #   /   \          / | .
    #  /     \        /  |/ \
    # .-------.      .---.---.
    triangles_blue1 = np.vstack(
        (
            triangles[blue1, 1],
            new_ind[blue1, 1],
            triangles[blue1, 0],
            triangles[blue1, 1],
            new_ind[blue1, 0:2].T,
            np.stack((new_ind[blue1, 1], new_ind[blue1, 0]), axis=1).T,
            triangles[blue1, 2],
        ),
    )
    triangles_blue1 = np.reshape(triangles_blue1, (3, -1), order="F").T

    #     .              .
    #    / \            /|\
    #   /   \          . | \
    #  /     \        / \|  \
    # .-------.      .---.---.
    triangles_blue3 = np.vstack(
        (
            triangles[blue3, 0],
            new_ind[blue3, 2],
            new_ind[blue3, 1],
            new_ind[blue3, 1],
            new_ind[blue3, 2],
            triangles[blue3, 1],
            triangles[blue3, 2],
            new_ind[blue3, 1],
            triangles[blue3, 1],
        ),
    )

    triangles_blue3 = np.reshape(triangles_blue3, (3, -1), order="F").T

    # green refinement
    #     .              .
    #    / \            /|\
    #   /   \          / | \
    #  /     \        /  |  \
    # .-------.      .---.---.
    triangles_green = np.vstack(
        (
            triangles[green, 1],
            new_ind[green, 1],
            triangles[green, 0],
            triangles[green, 2],
            new_ind[green, 1],
            triangles[green, 1],
        ),
    )

    triangles_green = np.reshape(triangles_green, (3, -1), order="F").T

    # combination of all refinements of the triangles and the not refined triangles
    triangles = np.vstack(
        (
            triangles[remain, :],
            triangles_red,
            triangles_blue1,
            triangles_blue3,
            triangles_green,
        ),
    ).astype(int)

    # new coordinates
    new_coord = 0.5 * (
        coord[edges[marked_edges == 1, 0], :] + coord[edges[marked_edges == 1, 1], :]
    )
    coord = np.vstack((coord, new_coord))

    # new dirichket doundary values
    new_dirichlet = new_nodes[Db2edges]
    ref = np.all([new_dirichlet > 0], axis=0)[:, 0]
    old_dirichlet = dirichlet[~ref, :]
    new_dirichlet = np.vstack(
        (
            dirichlet[ref, 0],
            new_dirichlet[ref][:, 0],
            new_dirichlet[ref][:, 0],
            dirichlet[ref, 1],
        ),
    )

    new_dirichlet = np.reshape(new_dirichlet, (2, -1), order="F").T
    dirichlet = np.vstack((old_dirichlet, new_dirichlet)).astype(int)
    return coord, triangles, dirichlet


# test to see how the refinement works on its own
if __name__ == "__main__":
    coord = np.array(
        [[-1, -1], [0, -1], [-1, 0], [0, 0], [1, 0], [-1, 1], [0, 1], [1, 1]],
    )
    triangles = np.array(
        [[0, 1, 3], [3, 2, 0], [2, 3, 6], [6, 5, 2], [3, 4, 7], [7, 6, 3]],
    )
    dirichlet = np.array(
        [[0, 1], [1, 3], [3, 4], [4, 7], [7, 6], [6, 5], [5, 2], [2, 0]],
    )
    marked = np.ones(triangles.shape[0])
    coord, triangles, dirichlet = rgb_refinement(coord, triangles, dirichlet, marked)
