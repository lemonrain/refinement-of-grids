# Numerics Code Snippets Repository: A GitLab Reference Hub

In this repository several different code snippets are available, as well as a beginner tutorial on the usage of python specifically in numerics.

There are also different solutions to exercises from lectures available.

## Getting Started

To get the code up and running it is advised to install anaconda (see [here](https://docs.anaconda.com/free/anaconda/install/index.html)) and to install VS Code 
(see [here](https://code.visualstudio.com/download)). To get all the necessary packages there is a yaml file available with which
we can create an environment which we then have to activate.

With this enviornment all the given python files should work.

Step by step the set-up for the anaconda environment is as follows:
* `conda env create -f numerics_conda.yml`
* `conda activate numerics_cond`
* in VS Code go to `settings` -> `command palette` -> `Python: Select interpreter` and select the conda enviornment
* run the code snippet

## Remark

A lot of the code is based on th book `Numerical Approximation of Partial Differential Equation` by Sören Bartels (ISBN 978-3-319-32353-4)
