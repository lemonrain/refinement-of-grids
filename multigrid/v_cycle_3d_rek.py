"""Routine to calculate the v cycle for 3D domains recursivly"""
import math

import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    # 2d Einheitsquadrat
    if d == 2:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
            ],
        )
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    # 3d Einheitswürfel
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [1, 0, 0],
                [0, 0, 1],
                [1, 0, 1],
                [0, 1, 0],
                [1, 1, 0],
                [0, 1, 1],
                [1, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 3, 7],
                [0, 1, 5, 7],
                [0, 4, 5, 7],
                [0, 4, 6, 7],
                [0, 2, 6, 7],
                [0, 2, 3, 7],
            ],
        )
        neumann = np.zeros([0, 3])
        dirichlet = np.array(
            [
                [0, 1, 5],
                [0, 4, 5],
                [1, 3, 7],
                [1, 7, 5],
                [2, 3, 7],
                [2, 6, 7],
                [0, 2, 6],
                [0, 4, 6],
                [0, 1, 3],
                [0, 2, 3],
                [4, 6, 7],
                [4, 5, 7],
            ],
        )
    # L-Gebiet
    elif d == 4:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
                [-1, 1],
                [-1, 0],
                [-1, -1],
                [0, -1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
                [5, 0, 3],
                [5, 3, 4],
                [6, 0, 5],
                [6, 7, 0],
            ],
        )
        dirichlet = np.array(
            [
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [5, 6],
                [6, 7],
                [7, 0],
            ],
        )
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


# Glaettungsmethode
def jacobi(A:ssp.csc_matrix, b:np.array, u:np.array, nu:int) -> np.array:
    """Smooths u with the Jacobi method"""
    u_smoothed = u
    D = ssp.csr_matrix(
        (A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    Dinv = ssp.csr_matrix(
        (1.0 / A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    R = A - D
    for _ in range(nu):
        u_smoothed = Dinv.dot(b - R.dot(u_smoothed))
    return u_smoothed


def show_3d(x:np.array, y:np.array, z:np.array, b:np.array, u_exact:callable)-> None:
    """Plot routine"""
    c = u_exact(x, y, z)
    plt.figure(300)
    ax_ex = plt.axes(projection="3d")

    # Create color map
    color_map = plt.get_cmap("spring")

    color_norm = colors.Normalize(vmin=np.min(b), vmax=np.max(c))
    # Create scatter plot and colorbar
    scatter_plot = ax_ex.scatter3D(x, y, z, c=c, cmap=color_map, norm=color_norm)

    plt.colorbar(scatter_plot)

    # adding title and labels
    ax_ex.set_title("exact solution")
    ax_ex.set_xlabel("X-axis")
    ax_ex.set_ylabel("Y-axis")
    ax_ex.set_zlabel("Z-axis")

    plt.figure(301)
    ax_fem = plt.axes(projection="3d")

    # Create scatter plot and colorbar
    scatter_plot = ax_fem.scatter3D(x, y, z, c=b, cmap=color_map, norm=color_norm)

    plt.colorbar(scatter_plot)

    # adding title and labels
    ax_fem.set_title("fem")
    ax_fem.set_xlabel("X-axis")
    ax_fem.set_ylabel("Y-axis")
    ax_fem.set_zlabel("Z-axis")

    plt.show()
    return 0


# Darstellung des Ergebnisses
def show_p1(coord:np.array, tets:np.array, u:np.array, fun:callable)-> None:
    """Plot routine"""
    u_exact = np.vectorize(fun)
    plt.figure(300)
    ax_m = plt.axes(projection="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_m.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=tets,
        cmap=my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_m.set_xlabel("x")
    ax_m.set_ylabel("y")
    ax_m.set_zlabel("z")
    ax_m.set_title("multigrid solution")

    plt.figure(301)
    ax_ex = plt.axes(projection="3d")
    ax_ex.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u_exact(coord[:, 0], coord[:, 1]),
        triangles=tets,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()


def get_gradients(number_of_triangle:int, coord:np.array, tets:np.array)-> np.array:
    """Gradient calculation"""
    nodes_loc = tets[number_of_triangle, :]
    coord_loc = coord[nodes_loc, :]
    tmp1 = np.concatenate((np.array([[1, 1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, tets:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(tets, 0)
    d_coord = np.size(coord, 1)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 4, 4))
    I1 = np.zeros((nelems, 4, 4))
    I2 = np.zeros((nelems, 4, 4))

    for j in range(nelems):
        nodes_loc = tets[j, :]
        coord_loc = coord[nodes_loc, :]
        A_t = np.concatenate(
            (np.ones(d_coord + 1).reshape((1, 4)), np.transpose(coord_loc)), axis=0
        )
        vol = np.abs(np.linalg.det(A_t) / math.factorial(d_coord))

        grads = get_gradients(j, coord, tets)
        Alocal[j, :, :] = vol * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        I1[j, :, :] = np.concatenate(
            (nodes_loc, nodes_loc, nodes_loc, nodes_loc), axis=0
        )
        I2[j, :, :] = np.concatenate(
            (nodes_loc.T, nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1
        )
    Alocal = np.reshape(Alocal, (16 * nelems, 1)).T
    I1 = np.reshape(I1, (16 * nelems, 1)).T
    I2 = np.reshape(I2, (16 * nelems, 1)).T
    A = ssp.csc_matrix((Alocal[0, :], (I1[0, :], I2[0, :])), shape=(nnodes, nnodes))
    A.eliminate_zeros()
    return A


def RHS_vector(coord:np.array, tets:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nelems = np.size(tets, 0)
    nnodes = np.size(coord, 0)
    d = np.size(coord, 1)
    b = np.zeros(nnodes)
    vol = np.zeros((nelems, 1))

    for j in range(nelems):
        nodes_loc = tets[j, :]
        coord_loc = coord[nodes_loc, :]
        arr = np.transpose(coord_loc)
        A_t = np.concatenate((np.ones(d + 1).reshape((1, 4)), arr), axis=0)
        vol[j] = np.abs(np.linalg.det(A_t) / math.factorial(d))
        mid = (
            1
            / 4
            * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :] + coord_loc[3, :])
        )

        b[nodes_loc] = b[nodes_loc] + vol[j] / 4 * f(mid[0], mid[1], mid[2])
    return b


def MG(A:ssp.csc_matrix, b:np.array, ell:int, u_ini:np.array)-> np.array:
    """Subrotine of the v cycle handling the rekursive steps

    Args:
        A: Stiffness matrix
        b: Right hand side
        ell: Amount of iterations
        u_ini: Initial value for the routine

    Returns:
        Gives back a solution for the PDE

    """
    global p1_list
    nu_pre = 3
    nu_post = nu_pre
    ell_0 = 0
    if ell == ell_0:
        # direktes Loesen auf dem groebsten Gitter
        u = ssp.linalg.spsolve(A, b)
    else:
        # Berechnung der Restriktionsmatrix
        p_t = p1_list[ell].transpose().tocsc()

        # Vorglaettung
        u = jacobi(A, b, u_ini, nu_pre)

        # Berechnung der Steifigkeitsmatrix auf dem groeberen Gitter
        A_coarse = p_t @ A @ p1_list[ell]

        # residuen Berechnung
        r_coarse = p_t.dot(b - A.dot(u))

        # Berechnung des Anfangswertes auf dem gröberen Gitter
        u_ini_coarse = np.zeros(r_coarse.shape[0])

        # rekursiver Aufruf
        c = MG(A_coarse, r_coarse, ell - 1, u_ini_coarse)

        # Hochschieben der Loesung auf das naechst feinere Gitter
        u = u + (p1_list[ell].dot(c))

        # Nachglaettung
        u = jacobi(A, b, u, nu_post)
    return u


def v_cycle_3d(d_tmp:int, l:int, f:callable, r:int) -> tuple:
    """Routine to perform a recursive v cycle

    Args:
        d_tmp: Dimension of the problem here 2
        f: Function for the right hand side
        l: Amount of iterations
        r: Amount of times to repeat the calculation of the result

    Returns:
        Gives back a multigird, a standard FEM solution and the stiffness matrix

    """
    global p1_list
    global d

    # Initalisierung einiger Variablen
    p1_list = []
    d = d_tmp
    coord, tets, dirichlet, neumann = get_geometry(d)
    dirichlet = np.concatenate((dirichlet, neumann))
    neumann = np.zeros([0, dirichlet.shape[1]])
    n_coord_prev = coord.shape[0]
    f_nodes_prev = np.setdiff1d(np.arange(n_coord_prev), np.unique(dirichlet))
    n_f_nodes_prev = np.size(f_nodes_prev)

    for k in range(l):
        # Anzahl der Verfeinerungen
        ref_tuple = red_refine(coord, tets, dirichlet, neumann)
        [coord, tets, dirichlet, neumann, _, p1] = ref_tuple

        # speichern der Prolongationsmatrizen
        p1 = p1.tolil()
        n_coord = coord.shape[0]
        f_nodes = np.setdiff1d(np.arange(n_coord), np.unique(dirichlet))
        n_f_nodes = np.size(f_nodes)

        R1 = ssp.csr_matrix(
            (np.ones(n_f_nodes), (f_nodes, np.arange(n_f_nodes))),
            shape=(n_coord, n_f_nodes),
        )
        R2 = ssp.csr_matrix(
            (np.ones(n_f_nodes_prev), (f_nodes_prev, np.arange(n_f_nodes_prev))),
            shape=(n_coord_prev, n_f_nodes_prev),
        )
        mat = (R1.transpose() @ p1) @ R2
        p1_list.append(mat)

        f_nodes_prev = f_nodes
        n_f_nodes_prev = n_f_nodes
        n_coord_prev = n_coord

        uhut_k = np.zeros(n_coord)
        u_k = np.zeros(n_coord)
        S = stiffness_matrix(coord, tets)
        S = S.tolil()

        # rechte Seite
        b = RHS_vector(coord, tets, f)

        # A eingeschraenkt auf die Freiheitsgrade
        A = (R1.transpose() @ S) @ R1

        b = R1.transpose().dot(b)

        # Berechnung des Ergebnisses
        if k == 0:
            u_kr = np.zeros(b.shape[0])
            u_kr = ssp.linalg.spsolve(A, b)
        else:
            u_kr = mat.dot(u_kr)
            for _g in range(r):
                u_kr = MG(A, b, k, u_kr)

    uhut_k[f_nodes] = u_kr
    u_k[f_nodes] = ssp.linalg.spsolve(A, b)

    # Darstellung des Ergebnisses

    return uhut_k, u_k, S


# -----------------------------------------------------------------------------


def f(x, y, z):
    """Exact solution"""
    return (x - x**2) * (y - y**2) * (z - z**2)


u_exact = np.vectorize(f)


def f(x, y, z):
    """Right hand side"""
    return (
        2 * (x - x**2) * (z - z**2)
        + 2 * (x - x**2) * (y - y**2)
        + 2 * (y - y**2) * (z - z**2)
    )


def ux(x, y, z):
    """Partial derivative of x"""
    return (1 - 2 * x) * (y - 1) * y * (z - 1) * z


def uy(x, y, z):
    """Partial derivative of y"""
    return (x - 1) * x * (1 - 2 * y) * (z - 1) * z


def uz(x, y, z):
    """Partial derivative of z"""
    return (x - 1) * x * (y - 1) * y * (1 - 2 * z)


def get_vol(coord:np.array, d_coord:int)->float:
    """Calculate volume of a tetraeder"""
    A_t = np.concatenate(
        (np.ones(d_coord + 1).reshape((1, 4)), np.transpose(coord)), axis=0
    )
    return np.abs(np.linalg.det(A_t) / math.factorial(d_coord))


def e_norm_fun(coord:np.array, tets:np.array, ux:callable, uy:callable, uz:callable,
               u:np.array)->float:
    """Calculation of the error in the energy-norm"""
    nelems = np.size(tets, 0)
    d_coord = np.size(coord, 1)
    enorm_err = 0
    for i in range(nelems):
        nodes_loc = tets[i, :]
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(i, coord, tets)
        area = get_vol(coord_loc, d_coord)
        mid = (
            1
            / 4
            * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :] + coord_loc[3, :])
        )
        enorm_err = (
            enorm_err
            + (
                (
                    ux(mid[0], mid[1], mid[2])
                    - u[nodes_loc[0]] * grads[0, 0]
                    - u[nodes_loc[1]] * grads[1, 0]
                    - u[nodes_loc[2]] * grads[2, 0]
                    - u[nodes_loc[3]] * grads[3, 0]
                )
                ** 2
                + (
                    uy(mid[0], mid[1], mid[2])
                    - u[nodes_loc[0]] * grads[0, 1]
                    - u[nodes_loc[1]] * grads[1, 1]
                    - u[nodes_loc[2]] * grads[2, 1]
                    - u[nodes_loc[3]] * grads[3, 1]
                )
                ** 2
                + (
                    uz(mid[0], mid[1], mid[2])
                    - u[nodes_loc[0]] * grads[0, 2]
                    - u[nodes_loc[1]] * grads[1, 2]
                    - u[nodes_loc[2]] * grads[2, 2]
                    - u[nodes_loc[3]] * grads[3, 2]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(enorm_err)


if __name__ == "__main__":
    d = 3
    k = 6
    r = 3
    coord, tets, dirichlet, neumann = get_geometry(d)
    enorm_err1 = np.zeros(k)
    enorm_err2 = np.zeros(k)
    enorm_err3 = np.zeros(k)
    h = np.zeros(k)
    max_err1 = np.zeros(k)
    max_err2 = np.zeros(k)
    max_err3 = np.zeros(k)

    for j in range(1, k):
        red_tuple = red_refine(coord, tets, dirichlet, neumann)
        [coord, tets, dirichlet, _, _, _] = red_tuple
        uhut_k, u_k, A = v_cycle_3d(d, j, f, r)
        u = u_exact(coord[:, 0], coord[:, 1], coord[:, 2])

        max_err1[j] = np.max(np.abs(u_k - uhut_k))
        max_err2[j] = np.max(np.abs(u - u_k))
        max_err3[j] = np.max(np.abs(u - uhut_k))
        enorm_err1[j] = np.sqrt(
            np.abs((u_k - uhut_k).transpose().dot(A.dot(u_k - uhut_k)))
        )
        enorm_err2[j] = e_norm_fun(coord, tets, ux, uy, uz, u_k)
        enorm_err3[j] = e_norm_fun(coord, tets, ux, uy, uz, uhut_k)

        # Gittergröße
        h[j] = np.sqrt(2) / (2**j)

    m = 2
    enorm_err1 = enorm_err1[m:k]
    enorm_err2 = enorm_err2[m:k]
    enorm_err3 = enorm_err3[m:k]
    max_err1 = max_err1[m:k]
    max_err2 = max_err2[m:k]
    max_err3 = max_err3[m:k]
    h = h[m:k]

    print("||u_k-uhut_k||_E = ", enorm_err1)
    print("||u-u_k||_E = ", enorm_err2)
    print("||u-uhut_k||_E = ", enorm_err3)
    print("||u_k-uhut_k||_M = ", max_err1)
    print("||u-u_k||_M = ", max_err2)
    print("||u-uhut_k||_M = ", max_err3)

    fig, ax_err1 = plt.subplots()
    ax_err1.loglog(h, enorm_err1, "-x")
    ax_err1.loglog(h, enorm_err2, "-x")
    plt.setp(
        ax_err1,
        xticks=[
            10 ** (-3),
            10 ** (-2),
            10 ** (-1),
            10 ** (-0),
        ],
        yticks=[
            10 ** (-4),
            10 ** (-3),
            10 ** (-2),
            10 ** (-1),
            10 ** (0),
        ],
    )
    ax_err1.set_xlabel("$h$")
    ax_err1.set_ylabel("E-Norm Fehler")
    ax_err1.legend([r"E-Norm mit $u_k$ und $\hat{u}_k$", "E-Norm mit $u$ und $u_k$"])
    plt.show()
