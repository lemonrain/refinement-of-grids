"""V cycle like in Numerical Approximation of Partial Differential Equations"""

import math

import matplotlib.colors as colors
import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
import scipy.sparse as ssp
from mpl_toolkits.mplot3d import Axes3D
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    # Einheitsquadrat
    if d == 2:
        coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1]])
        neumann = np.array([[1, 2], [2, 3], [3, 0]])
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [1, 0, 0],
                [1, 1, 0],
                [0, 1, 0],
                [0, 0, 1],
                [1, 0, 1],
                [1, 1, 1],
                [0, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2, 6],
                [0, 5, 1, 6],
                [0, 4, 5, 6],
                [0, 7, 4, 6],
                [0, 3, 7, 6],
                [0, 2, 3, 6],
            ],
        )
        neumann = np.array([[0, 1, 2], [0, 3, 2]])
        dirichlet = np.array(
            [
                [1, 2, 6],
                [1, 6, 5],
                [0, 1, 5],
                [4, 5, 6],
                [0, 7, 4],
                [4, 6, 7],
                [0, 3, 7],
                [3, 6, 7],
                [2, 3, 6],
            ],
        )
    # L-Gebiet
    elif d == 4:
        coord = np.asarray(
            [[0, 0], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]],
        )
        triangles = np.asarray(
            [[0, 1, 2], [0, 2, 3], [5, 0, 3], [5, 3, 4], [6, 0, 5], [6, 7, 0]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 0]],
        )
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


# Glättungsroutine könnte genau so gut jacobsen oder gaus sein
# für verschiedene Zwecke verschiedene Glättungsverfahren
# nu Anzahl der Glättungen
def richardson(A:ssp.csc_matrix, b:np.array, u:np.array, nu:int, ell:int)-> np.array:
    """Smooths u with the Richardson method"""
    global d
    h = 2 ** (-ell)
    omega = h ** (d - 2) / 10
    for _k in range(nu):
        u = u - omega * (A @ u - b)
    return u

def jacobi(A:ssp.csc_matrix, b:np.array, u:np.array, nu:int) -> np.array:
    """Smooths u with the Jacobi method"""
    u_smoothed = u
    D = ssp.csr_matrix(
        (A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    Dinv = ssp.csr_matrix(
        (1.0 / A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    R = A - D
    for _ in range(nu):
        u_smoothed = Dinv @ (b - R @ u_smoothed)
    return u_smoothed


def show_p1_3d(coord:np.array, u:np.array, fun:callable)-> None:
    """Plot routine for 3D domains"""
    u_exact = np.vectorize(fun)

    x = coord[:, 0]
    y = coord[:, 1]
    z = coord[:, 2]
    c = u_exact(x, y, z)

    fig = plt.figure(300)
    ax_ex = Axes3D(fig)

    # configuring colorbar
    color_norm = colors.Normalize(vmin=np.min(u), vmax=np.max(c))

    # Creat color map
    color_map = plt.get_cmap("spring")

    # Create scatter plot and colorbar
    scatter_plot = ax_ex.scatter3D(x, y, z, c=c, cmap=color_map, norm=color_norm)

    plt.colorbar(scatter_plot)

    # adding title and labels
    ax_ex.set_title("exact solution")
    ax_ex.set_xlabel("X-axis")
    ax_ex.set_ylabel("Y-axis")
    ax_ex.set_zlabel("Z-axis")

    fig = plt.figure(301)
    ax_fem = Axes3D(fig)

    # Create scatter plot and colorbar
    scatter_plot = ax_fem.scatter3D(x, y, z, c=u, cmap=color_map, norm=color_norm)

    plt.colorbar(scatter_plot)

    # adding title and labels
    ax_fem.set_title("fem")
    ax_fem.set_xlabel("X-axis")
    ax_fem.set_ylabel("Y-axis")
    ax_fem.set_zlabel("Z-axis")

    plt.show()


# Darstellung des Ergebnisses
def show_p1_2d(coord:np.array, triangles:np.array, u:callable,
         fun:callable) -> None:
    """Plot routine for 2D domains"""
    u_exact = np.vectorize(fun)
    plt.figure(300)
    ax_m = plt.axes(projection="3d")

    # Creating color map

    my_cmap = plt.get_cmap("summer")

    # Creating plot
    ax_m.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=triangles,
        cmap=my_cmap,
        edgecolor="Gray",
    )

    # Adding labels
    ax_m.set_xlabel("x")
    ax_m.set_ylabel("y")
    ax_m.set_zlabel("z")
    ax_m.set_title("multigrid solution")

    plt.figure(301)
    ax_ex = plt.axes(projection="3d")
    ax_ex.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u_exact(coord[:, 0], coord[:, 1]),
        triangles=triangles,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    # Adding labels
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()


# rekursive Funktion umzu berechnen
# die Anzahl der Glättungen wird manuell festgelegt und man kann der Vor-und
# Nachglättung verschiedene Werte zuweisen
# in p1_list stehen die Prolongationsmatrizen aus ihnen werden die Rekursionsmatrizen
# durch Transponieren berechnet -> eine Möglichkeit
# ell ist das feinste Gitter
# ell_0 das gröbste Gitter, lösen direkt auf dem gröbsten
# A Steifigkeitsmatrix auf dem feinsten Gitter durch die Prolongationsmatrizen und
# Restrikionsmatrizen können wir die restlichen berechnen
def rec_func(A:ssp.csc_matrix, b:np.array, ell:int)-> np.array:
    """Subrotine of the v cycle handling the rekursive steps

    Args:
        A: Stiffness matrix
        b: Right hand side
        l: Amount of iterations
        u_ini: Initial value for the routine

    Returns:
        Gives back a solution for the PDE

    """
    global p1_list
    nu_pre = 4
    nu_post = 4

    # wenn man beim L-Gebiet ell_0 auf 4 setzt sieht es gut aus sonst nicht
    ell_0 = 2
    if ell == ell_0:
        # direktes Lösen auf dem gröbsten Gitter
        u = ssp.linalg.spsolve(A, b)
    else:
        u_ini = np.zeros(b.shape[0])

        # Vorglättung
        u = richardson(A, b, u_ini, nu_pre, ell)

        # Berechnung der Rekursionsmatrix
        p_t = p1_list[ell - 1].transpose().tocsc()

        # Berechnung der Steifigkeitsmatrix auf dem gröberen Gitter
        A_coarse = p_t @ A @ p1_list[ell - 1]
        print(A_coarse)

        # error?
        r_coarse = p_t @ (b - A @ u)

        # rekursiver Aufruf
        c = rec_func(A_coarse, r_coarse, ell - 1)

        # Hochschieben der Lösung auf das nächst feinere Gitter
        u = u + (p1_list[ell - 1] @ c)  # .reshape(((p1_list[ell-1] @ c).shape[0],1))

        # Nachglättung
        u = richardson(A, b, u, nu_post, ell)
    return u


# Berechnung der Steifigkeitsmatrix und der Massematrix
def fe_matrices(coord:np.array, triangles:np.array) -> tuple:
    """Routine to calculate the stiffness matrix and the mass matrix"""
    nelems = np.size(triangles, 0)
    d_coord = np.size(coord, 1)
    nnodes = np.size(coord, 0)

    ctr = 0
    ctr_max = (d_coord + 1) ** 2 * nelems

    Slocal = np.zeros((ctr_max, 1))
    Mlocal = np.zeros((ctr_max, 1))
    I1 = np.zeros((ctr_max, 1))
    I2 = np.zeros((ctr_max, 1))
    m_loc = (np.ones((d_coord + 1, d_coord + 1)) + np.eye(d_coord + 1)) / (
        (d_coord + 1) * (d_coord + 2)
    )
    area = np.zeros((nelems, 1))

    for j in range(nelems):
        arr = np.transpose(coord[triangles[j, :], :])
        A_t = np.concatenate(
            (np.ones(d_coord + 1).reshape((1, d_coord + 1)), arr), axis=0,
        )
        area[j] = np.linalg.det(A_t) / math.factorial(d_coord)
        grads = np.linalg.solve(
            A_t, np.concatenate((np.zeros((1, d_coord)), np.eye(d_coord))),
        )
        for m in range(d_coord + 1):
            for n in range(d_coord + 1):
                Slocal[ctr] = area[j] * np.matmul(grads[m, :], grads[n, :].T)
                Mlocal[ctr] = area[j] * m_loc[m, n]
                I1[ctr] = triangles[j, m]
                I2[ctr] = triangles[j, n]
                ctr = ctr + 1
    S = ssp.csc_matrix((Slocal[:, 0], (I1[:, 0], I2[:, 0])), shape=(nnodes, nnodes))
    M = ssp.csc_matrix((Mlocal[:, 0], (I1[:, 0], I2[:, 0])), shape=(nnodes, nnodes))
    return S, M


def multigrid(d_tmp:int, l:int, f:callable, fun:callable) -> None:
    """Routine to perform an iterative v cycle

    Args:
        d_tmp: Dimension of the problem here 2
        f: Function for the right hand side
        l: Amount of iterations
        fun: Exact function

    Returns:
        Gives back a multigird, a standard FEM solution and the stiffness matrix

    """
    global p1_list
    global d

    # Initalisierung einiger Variablen
    p1_list = []
    d = d_tmp
    coord, triangles, dirichlet, neumann = get_geometry(d)
    dirichlet = np.concatenate((dirichlet, neumann))
    neumann = np.zeros([0, dirichlet.shape[1]])
    n_coord = coord.shape[0]
    dim = coord.shape[1]
    f_nodes_prev = np.setdiff1d(np.arange(n_coord), np.unique(dirichlet))

    for _ in range(l):
        # Anzahl der Verfeinerungen
        coord, triangles, dirichlet, neumann, _, p1 = red_refine(
            coord, triangles, dirichlet, neumann,
        )

        # speichern der Prolongationsmatrizen
        p1 = p1.tolil()
        n_coord = coord.shape[0]
        f_nodes = np.setdiff1d(np.arange(n_coord), np.unique(dirichlet))
        mat = p1[
            np.ix_(f_nodes, f_nodes_prev)
        ]  # get_sparse_out_of_bigger_sparse(f_nodes, f_nodes_prev, p1)
        mat = mat.tocsc()
        p1_list.append(mat)

        f_nodes_prev = f_nodes

    u = np.zeros(n_coord)
    [S, M] = fe_matrices(coord, triangles)  # okay
    M = M.tolil()
    S = S.tolil()

    # right-hand side
    if dim == 3:
        b = M[f_nodes, :] * f(coord[:, 0], coord[:, 1], coord[:, 2])
    elif dim == 2:
        b = M[f_nodes, :] * f(coord[:, 0], coord[:, 1])

    # A eingeschränkt auf degrees of freedom
    A = S[
        np.ix_(f_nodes, f_nodes)
    ]  # get_sparse_out_of_bigger_sparse(f_nodes, f_nodes, S) # übeltäter
    A = A.tocsc()
    A.eliminate_zeros()
    print(A)
    # Berechnung des Ergebnisses
    u[f_nodes] = rec_func(A, b, l)
    if dim == 3:
        show_p1_3d(coord, triangles, dirichlet, neumann, u, fun)
    elif dim == 2:
        show_p1_2d(coord, triangles, dirichlet, neumann, u, fun)
    return 0


# example 3d
def fun_3d(x, y, z):
    """Exact solution"""
    return (x - x**2) * (y - y**2) * (z - z**2)

def f_3d(x, y, z):
    """Right hand side"""
    return (
        2 * (x - x**2) * (z - z**2)
        + 2 * (x - x**2) * (y - y**2)
        + 2 * (y - y**2) * (z - z**2)
    )

# example 2d
def fun_ex_smooth1(x, y):
    """Exact solution"""
    return (x - x ** 2) * (y - y ** 2)
def f_smooth1(x, y):
    """Right hand side"""
    return 2 * (x - x ** 2 + (y - y ** 2))

# Beispiel 2d L-Gebiet
def fun_ex_smooth2(x, y):
    """Exact solution"""
    return np.sin(np.pi * x) * np.sin(np.pi * y)
def f_smooth2(x, y):
    """Right hand side"""
    return 2 * np.pi ** 2 * np.sin(np.pi * x) * np.sin(np.pi * y)

if __name__ == "__main__":
    multigrid(4, 4, f_smooth2, fun_ex_smooth2)
