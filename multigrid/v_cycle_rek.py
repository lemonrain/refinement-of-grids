"""Routine to calculate an estimate with the rekurivse v cycle in 2D"""
import math

import matplotlib.pyplot as plt
import numpy as np
import numpy.matlib
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    # 2d Einheitsquadrat
    if d == 2:
        coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
        triangles = np.array([[0, 1, 2], [0, 2, 3]])
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])
    # L-Gebiet
    elif d == 4:
        coord = np.asarray(
            [[0, 0], [1, 0], [1, 1], [0, 1], [-1, 1], [-1, 0], [-1, -1], [0, -1]],
        )
        triangles = np.asarray(
            [[0, 1, 2], [0, 2, 3], [5, 0, 3], [5, 3, 4], [6, 0, 5], [6, 7, 0]],
        )
        dirichlet = np.array(
            [[0, 1], [1, 2], [2, 3], [3, 4], [4, 5], [5, 6], [6, 7], [7, 0]],
        )
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


# Glaettungsroutine koennte genau so gut jacobsen oder gaus sein
# fuer verschiedene Zwecke verschiedene Glaettungsverfahren


# Glaettungsmethode
def jacobi(A:ssp.csc_array, b:np.array, u:np.array, nu:int) -> np.array:
    """Smooths u with the Jacobi method"""
    u_smoothed = u
    D = ssp.csr_matrix(
        (A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    Dinv = ssp.csr_matrix(
        (1.0 / A.diagonal(), (range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1], A.shape[1]),
    )
    R = A - D
    for _ in range(nu):
        u_smoothed = Dinv.dot(b - R.dot(u_smoothed))
    return u_smoothed


# Darstellung des Ergebnisses
def show_p1(coord:np.array, triangles:np.array, u:np.array, fun: callable,
         u_exact:callable) -> None:
    """Plot routine"""
    u_exact = np.vectorize(fun)
    plt.figure(300)
    ax_m = plt.axes(projection="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_m.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=triangles,
        cmap=my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_m.set_xlabel("x")
    ax_m.set_ylabel("y")
    ax_m.set_zlabel("z")
    ax_m.set_title("multigrid solution")

    plt.figure(301)
    ax_ex = plt.axes(projection="3d")
    ax_ex.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u_exact(coord[:, 0], coord[:, 1]),
        triangles=triangles,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()


# rekursive Funktion um zu berechnen
# die Anzahl der Glaettungen wird manuell festgelegt und
# man kann der Vor-und Nachglaettung verschiedene Werte zuweisen
# in p1_list stehen die Prolongationsmatrizen, aus ihnen werden
# die Rekursionsmatrizen durch Transponieren berechnet -> eine Moeglichkeit
# ell ist das feinste Gitter
# ell_0 das groebste Gitter, loesen direkt auf dem groebsten
# A Steifigkeitsmatrix auf dem feinsten Gitter durch die Prolongationsmatrizen
# und Restrikionsmatrizen koennen wir die restlichen berechnen
def MG(A:ssp.csc_matrix, b:np.array, ell:int, u_ini:np.array)-> np.array:
    """Subrotine of the v cycle handling the rekursive steps

    Args:
        A: Stiffness matrix
        b: Right hand side
        ell: Amount of iterations
        u_ini: Initial value for the routine

    Returns:
        Gives back a solution for the PDE

    """
    global p1_list
    nu_pre = 3
    nu_post = nu_pre
    ell_0 = 0
    if ell == ell_0:
        # direktes Loesen auf dem groebsten Gitter
        u = ssp.linalg.spsolve(A, b)
    else:
        # Berechnung der Restriktionsmatrix
        p_t = p1_list[ell].transpose().tocsc()

        # Vorglaettung
        u = jacobi(A, b, u_ini, nu_pre)

        # Berechnung der Steifigkeitsmatrix auf dem groeberen Gitter
        A_coarse = p_t @ A @ p1_list[ell]

        # residuen Berechnung
        r_coarse = p_t.dot(b - A.dot(u))

        # Berechnung des Anfangswertes auf dem gröberen Gitter
        u_ini_coarse = np.zeros(r_coarse.shape[0])

        # rekursiver Aufruf
        c = MG(A_coarse, r_coarse, ell - 1, u_ini_coarse)

        # Hochschieben der Loesung auf das naechst feinere Gitter
        u = u + (p1_list[ell].dot(c))

        # Nachglaettung
        u = jacobi(A, b, u, nu_post)
    return u


# Berechnung der Steifigkeitsmatrix
def fe_matrices(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    d_coord = np.size(coord, 1)
    nnodes = np.size(coord, 0)

    ctr = 0
    ctr_max = (d_coord + 1) ** 2 * nelems

    Slocal = np.zeros((ctr_max, 1))
    I1 = np.zeros((ctr_max, 1))
    I2 = np.zeros((ctr_max, 1))
    area = np.zeros((nelems, 1))

    for j in range(nelems):
        arr = np.transpose(coord[triangles[j, :], :])
        A_t = np.concatenate(
            (np.ones(d_coord + 1).reshape((1, d_coord + 1)), arr), axis=0,
        )
        area[j] = np.linalg.det(A_t) / math.factorial(d_coord)
        grads = np.linalg.solve(
            A_t, np.concatenate((np.zeros((1, d_coord)), np.eye(d_coord))),
        )
        for m in range(d_coord + 1):
            for n in range(d_coord + 1):
                Slocal[ctr] = area[j] * np.matmul(grads[m, :], grads[n, :].T)
                I1[ctr] = triangles[j, m]
                I2[ctr] = triangles[j, n]
                ctr = ctr + 1
    S = ssp.csr_matrix((Slocal[:, 0], (I1[:, 0], I2[:, 0])), shape=(nnodes, nnodes))
    S.eliminate_zeros()
    return S


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    d = np.size(coord, 1)
    b = np.zeros(nnodes)
    area = np.zeros((nelems, 1))

    for j in range(nelems):
        nodes_loc = triangles[j, :]
        coord_loc = coord[nodes_loc, :]
        arr = np.transpose(coord_loc)
        A_t = np.concatenate((np.ones(d + 1).reshape((1, d + 1)), arr), axis=0)
        area[j] = np.linalg.det(A_t) / math.factorial(d)
        mid = (1 / 3) * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        b[nodes_loc] = b[nodes_loc] + (area[j] / 3) * f(mid[0], mid[1])
    return b


def full_multigrid(d_tmp:int, l:int, f:callable, r:int) -> tuple:
    """Routine to perform a recursive v cycle

    Args:
        d_tmp: Dimension of the problem here 2
        f: Function for the right hand side
        l: Amount of iterations
        r: Amount of times to repeat the calculation of the result

    Returns:
        Gives back a multigird, a standard FEM solution and the stiffness matrix

    """
    global p1_list
    global d

    # Initalisierung einiger Variablen
    p1_list = []
    d = d_tmp
    coord, triangles, dirichlet, neumann = get_geometry(d)
    dirichlet = np.concatenate((dirichlet, neumann))
    neumann = np.zeros([0, dirichlet.shape[1]])
    n_coord_prev = coord.shape[0]
    f_nodes_prev = np.setdiff1d(np.arange(n_coord_prev), np.unique(dirichlet))
    n_f_nodes_prev = np.size(f_nodes_prev)

    for k in range(l):
        # Anzahl der Verfeinerungen
        ref_tuple = red_refine(coord, triangles, dirichlet, neumann)
        [coord, triangles, dirichlet, neumann, _, p1] = ref_tuple

        # speichern der Prolongationsmatrizen
        n_coord = coord.shape[0]
        f_nodes = np.setdiff1d(np.arange(n_coord), np.unique(dirichlet))
        n_f_nodes = np.size(f_nodes)

        R1 = ssp.csr_matrix(
            (np.ones(n_f_nodes), (f_nodes, np.arange(n_f_nodes))),
            shape=(n_coord, n_f_nodes),
        )
        R2 = ssp.csr_matrix(
            (np.ones(n_f_nodes_prev), (f_nodes_prev, np.arange(n_f_nodes_prev))),
            shape=(n_coord_prev, n_f_nodes_prev),
        )
        mat = (R1.transpose() @ p1) @ R2
        p1_list.append(mat)

        f_nodes_prev = f_nodes
        n_f_nodes_prev = n_f_nodes
        n_coord_prev = n_coord

        uhut_k = np.zeros(n_coord)
        u_k = np.zeros(n_coord)
        S = fe_matrices(coord, triangles)
        S = S.tolil()

        # rechte Seite
        b = RHS_vector(coord, triangles, f)

        # A eingeschraenkt auf die Freiheitsgrade
        A = (R1.transpose() @ S) @ R1

        b = R1.transpose().dot(b)

        # Berechnung des Ergebnisses
        if k == 0:
            u_kr = np.zeros(b.shape[0])
            u_kr = ssp.linalg.spsolve(A, b)
        else:
            u_kr = mat.dot(u_kr)
            for _g in range(r):
                u_kr = MG(A, b, k, u_kr)

    uhut_k[f_nodes] = u_kr
    u_k[f_nodes] = ssp.linalg.spsolve(A, b)

    # Darstellung des Ergebnisses

    return uhut_k, u_k, S

# example for a singular solution
def fun_sing(x, y):
    """Exact solution"""
    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    if phi < 0:
        phi = 2 * np.pi + phi
    return (1 - x**2) * (1 - y**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)

def f_sing(x, y):
    """Right hand side"""
    r = np.sqrt(x**2 + y**2)
    if r == 0:
        val = 0
    else:
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        # Produktformel Laplace(uv)=uLapl v + 2gradu.gradv+v Lapl u
        part1 = (
            -2
            * ((1 - x**2) + (1 - y**2))
            * r ** (2 / 3)
            * np.sin(2 / 3 * phi)
        )
        dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
        dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
        part2 = -2 * x * (1 - y**2) * (
            dudr * x / r - dudphi * np.sin(phi) / r
        ) - 2 * y * (1 - x**2) * (dudr * y / r + dudphi * np.cos(phi) / r)
        val = part1 + 2 * part2
    return -val

def ux_sing(x, y):
    """Partial derivative of x"""
    r = np.sqrt(x**2 + y**2)
    if r == 0:
        val = 0
    else:
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        part1 = -2 * x * (1 - y**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)
        dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
        dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
        part2 = (
            (1 - x**2)
            * (1 - y**2)
            * (dudr * np.cos(phi) - dudphi * np.sin(phi) / r)
        )
        val = part1 + part2
    return val

def uy_sing(x, y):
    """Partial derivative of y"""
    r = np.sqrt(x**2 + y**2)
    if r == 0:
        val = 0
    else:
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        part1 = -2 * y * (1 - x**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)
        dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
        dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
        part2 = (
            (1 - x**2)
            * (1 - y**2)
            * (dudr * np.sin(phi) + dudphi * np.cos(phi) / r)
        )
        val = part1 + part2
    return val

#-------------------------------------------------------------------------

# Good example for the unit square smooth solution
def fun_ex_smooth1(x, y):
    """Exact solution"""
    return (x - x ** 2) * (y - y ** 2)
def f_smooth1(x, y):
    """Right hand side"""
    return 2 * (x - x ** 2 + (y - y ** 2))

def ux_smooth1(x, y):
    """Partial derivative of x"""
    return y * (y - 1) * (2 * x - 1)
def uy_smooth1(x, y):
    """Partial derivative of y"""
    return x * (x - 1) * (2 * y - 1)

# Good example for L domain for a smooth solution
def fun_ex_smooth2(x, y):
    """Exact solution"""
    return np.sin(np.pi * x) * np.sin(np.pi * y)
def f_smooth2(x, y):
    """Right hand side"""
    return 2 * np.pi ** 2 * np.sin(np.pi * x) * np.sin(np.pi * y)

def ux_smooth2(x, y):
    """Partial derivative of x"""
    return np.pi * np.cos(np.pi * x) * np.sin(np.pi * y)
def uy_smooth2(x, y):
    """Partial derivative of y"""
    return np.pi * np.cos(np.pi * y) * np.sin(np.pi * x)


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle"""
    tmp = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (tmp[0, 0] * tmp[1, 1] - tmp[0, 1] * tmp[1, 0])


def e_norm_fun(coord:np.array, triangles:np.array, ux:callable, uy:callable,
               u:np.array)->float:
    """Calculation of the error in the energy-norm"""
    enorm_err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(coord_loc)
        area = get_area(coord_loc)
        mid = 1 / 3 * (coord_loc[0, :] + coord_loc[1, :] + coord_loc[2, :])
        enorm_err = (
            enorm_err
            + (
                (
                    ux(mid[0], mid[1])
                    - u[nodes_loc[0]] * grads[0, 0]
                    - u[nodes_loc[1]] * grads[1, 0]
                    - u[nodes_loc[2]] * grads[2, 0]
                )
                ** 2
                + (
                    uy(mid[0], mid[1])
                    - u[nodes_loc[0]] * grads[0, 1]
                    - u[nodes_loc[1]] * grads[1, 1]
                    - u[nodes_loc[2]] * grads[2, 1]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(enorm_err)


if __name__ == "__main__":
    # muss je nach dem auf 4 oder 2 angepasst werden
    d = 2
    # k ist die Anzahl an Verfeinerungen
    k = 5
    # Anzahl an Optimierungen für den initial Input
    r = 3
    coord1, triangles1, dirichlet1, neumann1 = get_geometry(d)
    enorm_err1 = np.zeros(k)
    enorm_err2 = np.zeros(k)
    enorm_err3 = np.zeros(k)
    h = np.zeros(k)

    for j in range(1, k):
        red_tuple = red_refine(coord1, triangles1, dirichlet1, neumann1)
        [coord1, triangles1, dirichlet1, _, _, _] = red_tuple
        uhut_k, u_k, A = full_multigrid(d, j, f_smooth1, r)

        enorm_err1[j] = np.sqrt(
            np.abs((u_k - uhut_k).transpose().dot(A.dot(u_k - uhut_k))),
        )
        enorm_err2[j] = e_norm_fun(coord1, triangles1, ux_smooth1, uy_smooth1, u_k)
        enorm_err3[j] = e_norm_fun(coord1, triangles1, ux_smooth1, uy_smooth1, uhut_k)

        # Gittergröße
        h[j] = np.sqrt(2) / (2**j)

    m = 2
    enorm_err1 = enorm_err1[m:k]
    enorm_err2 = enorm_err2[m:k]
    enorm_err3 = enorm_err3[m:k]
    h = h[m:k]

    print("||u_k-uhut_k||_E = ", enorm_err1)
    print("||u-u_k||_E = ", enorm_err2)
    print("||u-uhut_k||_E = ", enorm_err3)

    fig, ax_err1 = plt.subplots()
    ax_err1.loglog(h, enorm_err1, "-x")
    ax_err1.loglog(h, enorm_err2, "-x")
    plt.setp(
        ax_err1,
        xticks=[10 ** (-3), 10 ** (-2), 10 ** (-1), 10 ** (-0)],
        yticks=[10 ** (-4), 10 ** (-3), 10 ** (-2), 10 ** (-1), 10 ** (0)],
    )
    ax_err1.set_xlabel("$h$")
    ax_err1.set_ylabel("E-Norm Fehler")
    ax_err1.legend([r"E-Norm mit $u_k$ und $\hat{u}_k$", "E-Norm mit $u$ und $u_k$"])
    plt.show()
