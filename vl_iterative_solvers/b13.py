"""Possible solution for the exercise B13"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
import scipy.sparse.linalg
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    if d == 1:
        coord = np.array([[0], [1]])
        triangles = np.array([[0, 1]])
        dirichlet = np.array([0])
        neumann = np.array([1])

    # 2d Einheitsquadrat / 2d unit square
    elif d == 2:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
            ],
        )
        dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
        neumann = np.zeros([0, 2])

    # 3d Einheitswürfel / 3d unit cube
    elif d == 3:
        coord = np.array(
            [
                [0, 0, 0],
                [0, 0, 1],
                [1, 0, 0],
                [1, 0, 1],
                [0, 1, 0],
                [0, 1, 1],
                [1, 1, 0],
                [1, 1, 1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 3, 7],
                [0, 2, 3, 7],
                [0, 1, 5, 7],
                [0, 4, 5, 6],
                [0, 2, 6, 7],
                [0, 4, 6, 7],
            ],
        )
        neumann = np.zeros([0, 3])
        dirichlet = np.array(
            [
                [0, 2, 6],
                [1, 5, 6],
                [0, 1, 5],
                [5, 4, 6],
                [0, 4, 6],
                [4, 6, 7],
                [2, 3, 7],
                [3, 7, 6],
                [3, 2, 6],
                [1, 0, 2],
                [0, 2, 3],
            ],
        )

    # L-Gebiet/ L domain
    elif d == 4:
        coord = np.array(
            [
                [0, 0],
                [1, 0],
                [1, 1],
                [0, 1],
                [-1, 1],
                [-1, 0],
                [-1, -1],
                [0, -1],
            ],
        )
        triangles = np.array(
            [
                [0, 1, 2],
                [0, 2, 3],
                [5, 0, 3],
                [5, 3, 4],
                [6, 0, 5],
                [6, 7, 0],
            ],
        )
        dirichlet = np.array(
            [
                [0, 1],
                [1, 2],
                [2, 3],
                [3, 4],
                [4, 5],
                [5, 6],
                [6, 7],
                [7, 0],
            ],
        )
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann


def FEM(coord:np.array, triangles:np.array, dirichlet:np.array,
        f:callable)-> np.array:
    """Calculation of the finite element solution

    Args:
        coord: coordinates of the triangulation
        dirichlet: mapping of the coordinates to the dirichlet boundary
        triangles: mapping of the coordinates to the triangles
        f: right hand side

    Returns:
        The solution calculated with the finite element method

    """
    nnodes = np.size(coord, 0)
    S = stiffness_matrix(coord, triangles)
    b = RHS_vector(coord, triangles, f)

    # calculate degrees of freedom
    dbnodes = np.unique(dirichlet)
    dof = np.setdiff1d(range(nnodes), dbnodes)
    ndof = np.size(dof)
    R = ssp.csr_matrix(
        (np.ones(ndof), (dof, np.arange(0, ndof))), shape=(nnodes, ndof),
    )
    # restrict right hand side and stiffness matrix to degrees of freedom
    S_inner = (R.transpose() @ S) @ R
    b_inner = R.transpose() @ b

    x = np.zeros(nnodes)
    # get solution on degrees of freedom
    x[dof] = scipy.sparse.linalg.spsolve(S_inner, b_inner)
    return x


def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        # local stiffness matrix
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])),
                          shape=(nnodes, nnodes))


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        b[nodes_loc] = b[nodes_loc] + area / 3 * f(mid[0], mid[1])
    return b

def show(coord:np.array, triangles:np.array, u:np.array,
         u_exact:callable) -> None:
    """Plot routine to compare the FEM result with the exact solution"""
    plt.figure(300)
    ax_fe = plt.axes(projection="3d")

    # set color map
    my_cmap = plt.get_cmap("summer")

    # make plot
    ax_fe.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=triangles,
        cmap=my_cmap,
        edgecolor="Gray",
    )

    # labeling
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")

    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection="3d")
    ax_ex.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u_exact(coord[:, 0], coord[:, 1]),
        triangles=triangles,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()


def convergence_history(hlist:np.array,errlist:np.array, spec: str) -> None:
    """Convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history"+spec)
    ax.legend()
    plt.tight_layout()
    plt.show()


# Fehlermessung mit Energienorm
def e_norm_fun(coord:np.array, triangles:np.array, u:np.array,
               u_ex: float) -> np.array:
    """Calculation of the error in the energy norm"""
    enorm_err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(coord_loc)
        area = get_area(coord_loc)
        enorm_err = (
            enorm_err
            + (
                (
                    u[nodes_loc[0]] * grads[0, 0]
                    + u[nodes_loc[1]] * grads[1, 0]
                    + u[nodes_loc[2]] * grads[2, 0]
                )
                ** 2
                + (
                    u[nodes_loc[0]] * grads[0, 1]
                    + u[nodes_loc[1]] * grads[1, 1]
                    + u[nodes_loc[2]] * grads[2, 1]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(u_ex - enorm_err)


#######################################################################
#######################################################################
#######################################################################


if __name__ == "__main__":

    def f_fun(x,y):
        """Right hand side"""
        return 1


    f = np.vectorize(f_fun)
    red = 6
    h1_err_sq = np.zeros(red)
    h1_err_ld = np.zeros(red)
    diam = np.zeros(red)
    coord_sq, triangles_sq, dirichlet_sq,neumann_sq = get_geometry(2)
    coord_ld, triangles_ld, dirichlet_ld,neumann_ld = get_geometry(4)
    for j in range(red):
        coord_sq, triangles_sq, dirichlet_sq,neumann_sq,_,_ = red_refine(coord_sq,
                                                                         triangles_sq,
                                                                         dirichlet_sq,
                                                                         neumann_sq)
        coord_ld, triangles_ld, dirichlet_ld,neumann_ld,_,_ = red_refine(coord_ld,
                                                                         triangles_ld,
                                                                         dirichlet_ld,
                                                                         neumann_ld)

        x_sq = FEM(coord_sq, triangles_sq, dirichlet_sq,f)
        x_ld = FEM(coord_ld, triangles_ld, dirichlet_ld,f)

        h1_err_sq[j] = e_norm_fun(coord_sq, triangles_sq, x_sq, 0.2140750232)

        h1_err_ld[j] = e_norm_fun(coord_ld, triangles_ld, x_ld, 0.2140750232)

        diam[j] = 0.5 ** (1+j)

    # plot the convergence history
    convergence_history(diam,h1_err_ld, " on the L domain")
    convergence_history(diam,h1_err_sq, " on the unit square")
    print(h1_err_sq)
    print(h1_err_ld)
