"""Possible solution for exercise B7"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse
import scipy.sparse.linalg
from numpy import matlib
from scipy.sparse import csr_matrix, spdiags


def FDM_coords(n:int) -> tuple:
    """Calculation of the coordinates (L-shaped domain)"""
    N=(n+1)**2
    coord_x=np.reshape(matlib.repmat(2*(1/n*np.arange(0,n+1)-.5),1,n+1),(N,1),order="F")
    coord_y=np.reshape(matlib.repmat(2*(1/n*np.arange(0,n+1)-.5),n+1,1),(N,1),order="F")
    return coord_x, coord_y

def FDM_data(n:int,f:callable) -> tuple:
    """Calculation of stiffness matrix, right hand side and grid size (L-shaped domain)"""
    h=2/n #for the L-shaped domain
    N=(n+1)**2
    data = np.array([-np.ones(N),-np.ones(N),4*np.ones(N),-np.ones(N),-np.ones(N)])
    diags = np.array([-n-1,-1,0, 1,n+1])
    A=spdiags(data, diags, N, N).tocsr()
    coord_x, coord_y=FDM_coords(n)
    b=h**2*np.reshape(f(coord_x,coord_y),(n+1)**2)
    return A, b, h

def restrict2dof(n:int, coord_x:np.array, coord_y:np.array) -> tuple:
    """Calculation of the degrees of freedom (L-shaped domain)"""
    N=(n+1)**2
    #identify boundary nodes
    bottom=np.arange(0,n/2)
    top=np.arange(n*(n+1),N)
    left=(n+1)*np.arange(1,n)
    right=np.concatenate( (n/2+(n+1)*np.arange(1,n/2), \
           np.arange((N-1)/2,(n/2+1)*(n+1)), \
           n/2*(n+1)+n+(n+1)*np.arange(1,n/2) ))
    bdry=np.concatenate((bottom,right,top,left))
    #nodes outside the domain
    outer = np.intersect1d(np.where(coord_x>0),np.where(coord_y<0))
    #degrees of freedom
    dof=np.setdiff1d(range(N),bdry)
    dof=np.setdiff1d(dof,outer)
    ndof=np.size(dof)
    R=csr_matrix((np.ones(ndof),(dof,np.arange(0,ndof))),shape = (N,ndof))
    return R, outer

def solveFDM(n:int,f:callable,coord_x:np.array, coord_y:np.array) -> tuple:
    """Solving of the equation with the finite difference method (L-shaped domain)"""
    A, b, h = FDM_data(n,f)
    R,outer=restrict2dof(n,coord_x, coord_y)
    A_inner=(R.transpose()@A)@R
    b_inner=R.transpose()@b
    x=R@scipy.sparse.linalg.spsolve(A_inner,b_inner)
    return x, h, outer



#-------9 point stencil-------------

def FDM_data_9point(n:int,f:callable,laplace_f:callable) -> tuple:
    """Calculation of stiffness matrix, right hand side and grid size (L-shaped domain and 9 point stencil)"""
    h=2/n
    N=(n+1)**2
    data = np.array([-np.ones(N),-4*np.ones(N),-np.ones(N),-4*np.ones(N),
                     20*np.ones(N),-4*np.ones(N),-np.ones(N),-4*np.ones(N),-np.ones(N)])
    diags = np.array([-n-2,-n-1,-n,-1,0, 1,n,n+1,n+2])
    A=spdiags(data, diags, N, N).tocsr()
    coord_x, coord_y=FDM_coords(n)
    b=6*h**2*np.reshape(f(coord_x,coord_y),
                        (n+1)**2) + 0.5*h**4*np.reshape(laplace_f(coord_x,coord_y),
                                                        (n+1)**2)

    return A, b, h



def solveFDM_9point(n:int,f:callable,laplace_f:callable,coord_x:np.array,
                    coord_y:np.array) -> tuple:
    """Solving of the equation with the finite difference method (L-shaped domain and 9 point stencil)"""
    A, b, h = FDM_data_9point(n,f,laplace_f)
    R,outer=restrict2dof(n,coord_x, coord_y)
    A_inner=(R.transpose()@A)@R

    b_inner=R.transpose()@b
    x=R@scipy.sparse.linalg.spsolve(A_inner,b_inner)
    return x, h, outer

#-----------------------------------------------------------------------
    # Visualization
#-----------------------------------------------------------------------

def comparison_convergence(e_5point:np.array,e_9point:np.array,J:np.array) -> None:
    """Visualization of the convergences of the 5 point and 9 point stencil"""
    fig, ax = plt.subplots()
    ax.plot(J,e_5point,label = "Error 5 point stencil")
    ax.plot(J,e_9point,label = "Error 9 point stencil")
    ax.set_xlabel("Grid points")
    ax.set_ylabel("Maximum Error norm")
    ax.legend()
    plt.tight_layout()
    plt.show()

def show(coord_x:np.array, coord_y:np.array, u5:np.array, u9:np.array,
         uex4nodes:np.array, outer:np.array) -> None:
    """Visulization of the exact solution and the approximated ones"""
    plt.figure(300)
    ax_fe = plt.axes(projection ="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_fe.plot_trisurf(
        np.delete(coord_x[:,0], outer), np.delete(coord_y[:,0], outer),
        np.delete(u5, outer),
        cmap = my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.set_title("5 point stencil")

    plt.figure(302)
    ax_fe = plt.axes(projection ="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_fe.plot_trisurf(
        np.delete(coord_x[:,0], outer), np.delete(coord_y[:,0], outer),
        np.delete(u9, outer),
        cmap = my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.set_title("9 point stencil")


    plt.figure(301)
    ax_ex = plt.axes(projection ="3d")
    ax_ex.plot_trisurf(
        np.delete(coord_x[:,0], outer), np.delete(coord_y[:,0], outer),
        uex4nodes,
        cmap = plt.get_cmap("summer"),
        edgecolor ="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()



#------ Function definition -------

def solution_func(x,y):
    """Exact solution for purposes of comparison"""
    return np.sin(np.pi*x)*np.sin(np.pi*y)

def func(x,y):
    """Right-hand side function f"""
    return 2*np.pi**2*np.sin(np.pi*x)*np.sin(np.pi*y)

def laplace_func(x,y):
    """Double laplacian solution_func"""
    return 4*np.pi**4*np.sin(np.pi*x)*np.sin(np.pi*y)


# example for a singular solution
def solution_func_s(x, y):
    """Exact solution for purposes of comparison"""
    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    if phi < 0:
        phi = 2 * np.pi + phi
    return (1 - x**2) * (1 - y**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)

def func_s(x,y):
    """Right-hand side function f"""
    r = np.sqrt(x**2 + y**2)
    if r == 0:
        val = 0
    else:
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        val = 2/3*r**(2/3)*(5*(-2+r**2)+2*r**2*np.cos(4*phi/3)
                            +2*r**2*np.cos(8*phi/3))*np.sin(2*phi/3)
    return -val

def laplace_func_s(x,y):
    """Double laplacian solution_func"""
    r = np.sqrt(x**2 + y**2)
    if r == 0:
        val = 0
    else:
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        val = 8/9 *r**(2/3)*(20*np.sin(2*phi/3)-3*np.sin(10*phi/3))
    return val

#-------- Numerical experiment --------------------------

if __name__ == "__main__":
    func_vectorized = np.vectorize(func_s)
    laplace_func_vectorized = np.vectorize(laplace_func_s)
    solution_func_vectorized = np.vectorize(solution_func_s)
    J_list = []
    e_5point_list = []
    e_9point_list = []

    for i in range(2,6):
        J = pow(2,i)
        J_list.append(J)

        coord_x, coord_y=FDM_coords(J)
        U_5point, h, outer_5 = solveFDM(J,func_vectorized,coord_x,coord_y)
        U_9point, h, outer_9 = solveFDM_9point(J,func_vectorized,
                                             laplace_func_vectorized,coord_x,coord_y)

        uex4nodes=np.reshape(solution_func_vectorized(coord_x,coord_y),(J+1)**2)
        uex4nodes[np.ix_(outer_5)]=0

        e_5point = uex4nodes-U_5point
        e_9point = uex4nodes-U_9point

        uex4nodes = np.delete(uex4nodes, outer_5)

        error_5point = np.linalg.norm(e_5point, ord=np.inf)
        error_9point = np.linalg.norm(e_9point, ord=np.inf)

        e_5point_list.append(error_5point)
        e_9point_list.append(error_9point)
        print("J: "+str(J))
        print("error 5point: "+str(error_5point))
        print("error 9point: "+str(error_9point))
    comparison_convergence(e_5point_list,e_9point_list,J_list)
    show(coord_x,coord_y,U_5point,U_9point,uex4nodes,outer_5)

