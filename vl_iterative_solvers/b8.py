"""Possible solution for exercise B8"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from b3 import FDM_coords, FDM_data, restrict2dof, solveFDM


def jacobi(A:ssp.csr_matrix, b:np.array, nu:int) -> np.array:
    """Jacobi method"""
    u = np.zeros(b.shape[0])
    D = ssp.csr_matrix(
        (A.diagonal(),(range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1],A.shape[1]),
    )
    Dinv = ssp.csr_matrix(
        (1./A.diagonal(),(range(A.shape[0]), range(A.shape[1]))),
        shape = (A.shape[1],A.shape[1]),
    )
    LR = A-D
    for _ in range(nu):
        u = Dinv.dot(b-LR.dot(u))
    return u

def gauss_seidel(A:ssp.csr_matrix, b:np.array, nu:int) -> np.array:
    """Gauss-Seidel method"""
    u = np.zeros(b.shape[0])
    D = ssp.csr_matrix(
        (A.diagonal(),(range(A.shape[0]), range(A.shape[1]))),
        shape=(A.shape[1],A.shape[1]),
    )
    R = ssp.triu(A,k=1)
    L = ssp.tril(A,k=-1)
    DL_inv = ssp.linalg.inv(D+L)
    for _ in range(nu):
        u = DL_inv.dot(b-R.dot(u))
    return u


#-----------------------------------------------------------------------
    # Visualization
#-----------------------------------------------------------------------
def convergence_history(errlist:np.array, m:int, lab:str) -> None:
    """Plot of the convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.semilogy(range(m),errlist[0,:],label="$h=2^2$")
    ax.semilogy(range(m),errlist[1,:],label="$h=2^3$")
    ax.semilogy(range(m),errlist[2,:],label="$h=2^4$")
    ax.semilogy(range(m),errlist[3,:],label="$h=2^5$")
    ax.set_yticks([10e-1,10e-2,10e-3,10e-4])
    ax.set_xlabel("iterations")
    ax.set_ylabel("error")
    ax.set_title("convergence history " + lab)
    ax.legend()

    plt.tight_layout()
    plt.show()



#-------- function definition --------------------------

def uex_fun(x,y):
    """Exact solution"""
    return np.exp(x)*(x-x**2)*(y-y**2)

def f_fun(x,y):
    """Right-hand side"""
    return -np.exp(x)*x*(x*(y**2-y+2)+3*y**2-3*y-2)

#-------- the numerical experiment --------------------------

if __name__ == "__main__":
    u_exact = np.vectorize(uex_fun)
    f = np.vectorize(f_fun)

    coarse = 2 #2^coarse intervals per axis
    fine = 5
    m = 10
    n_meshes = fine-coarse+1
    hlist = np.zeros(n_meshes)
    errlist_jac = np.zeros((n_meshes,m))
    errlist_gauss = np.zeros((n_meshes,m))
    for j in range(n_meshes):
        lvl = coarse+j
        n = 2**(lvl)
        x, h = solveFDM(n,f)
        A, b, h = FDM_data(n,f)
        coord_x, coord_y = FDM_coords(n)
        R = restrict2dof(n)
        A = (R.transpose()@A)@R
        b = R.transpose()@b
        uex4nodes = np.reshape(u_exact(coord_x,coord_y),(n+1)**2)

        # compute errors
        for k in range(m):
            u_jac = R @ jacobi(A,b,k)
            u_gau = R @ gauss_seidel(A,b,k)
            errlist_jac[j,k] = np.max(np.abs(x-u_jac))
            errlist_gauss[j,k] = np.max(np.abs(x-u_gau))

        hlist[j] = h
    convergence_history(errlist_gauss,m,"gauss")
    convergence_history(errlist_jac,m,"jacobi")

