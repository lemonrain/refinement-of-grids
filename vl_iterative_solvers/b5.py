"""Possible solution for exercise B5"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse
import scipy.sparse.linalg
from numpy import matlib
from scipy.sparse import csr_matrix, spdiags


def uex_fun(x,y):
    """Exact solution"""
    return np.exp(x)*(x-x**2)*(y-y**2)

def f_fun(x,y):
    """Right-hand side"""
    return -np.exp(x)*x*(x*(y**2-y+2)+3*y**2-3*y-2)

# example for a singular solution
def fun_sing(x, y):
    """Exact solution for a singular problem"""
    r = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    if phi < 0:
        phi = 2 * np.pi + phi
    return (1 - x**2) * (1 - y**2) * r ** (2 / 3) * np.sin(2 / 3 * phi)

def f_sing(x, y):
    """Right hand side for a singular problem"""
    r = np.sqrt(x**2 + y**2)
    if r == 0:
        val = 0
    else:
        phi = np.arctan2(y, x)
        if phi < 0:
            phi = 2 * np.pi + phi
        # Produktformel Laplace(uv)=uLapl v + 2gradu.gradv+v Lapl u
        part1 = (
            -2
            * ((1 - x**2) + (1 - y**2))
            * r ** (2 / 3)
            * np.sin(2 / 3 * phi)
        )
        dudr = 2 / 3 * r ** (-1 / 3) * np.sin(2 / 3 * phi)
        dudphi = r ** (2 / 3) * 2 / 3 * np.cos(2 / 3 * phi)
        part2 = -2 * x * (1 - y**2) * (
            dudr * x / r - dudphi * np.sin(phi) / r
        ) - 2 * y * (1 - x**2) * (dudr * y / r + dudphi * np.cos(phi) / r)
        val = part1 + 2 * part2
    return -val

def FDM_coords(n:int) -> None:
    """Calculation of the coordinates (L-shaped domain)"""
    N = (n+1)**2
    coord_x = np.reshape(matlib.repmat(2*(1/n*np.arange(0,n+1)-.5),1,n+1),(N,1),order="F")
    coord_y = np.reshape(matlib.repmat(2*(1/n*np.arange(0,n+1)-.5),n+1,1),(N,1),order="F")
    return coord_x, coord_y

def FDM_data(n:int,f:callable) -> tuple:
    """Calculation of stiffness matrix, right hand side and grid size (L-shaped domain)"""
    h = 2/n
    N = (n+1)**2
    data = np.array([-np.ones(N),-np.ones(N),4*np.ones(N),-np.ones(N),-np.ones(N)])
    diags = np.array([-n-1,-1,0, 1,n+1])
    A = spdiags(data, diags, N, N).tocsr()
    coord_x, coord_y = FDM_coords(n)
    b = h**2*np.reshape(f(coord_x,coord_y),(n+1)**2)
    return A, b, h

def restrict2dof(n:int, coord_x:np.array, coord_y:np.array) -> tuple:
    """Calculation of the degrees of freedom (L-shaped domain)"""
    N = (n+1)**2
    #identify boundary nodes
    bottom = np.arange(0,n/2)
    top = np.arange(n*(n+1),N)
    left = (n+1)*np.arange(1,n)
    right = np.concatenate((n/2+(n+1)*np.arange(1,n/2),
                          np.arange((N-1)/2,(n/2+1)*(n+1)),
                          n/2*(n+1)+n+(n+1)*np.arange(1,n/2)))
    bdry = np.concatenate((bottom,right,top,left))
    #nodes outside the domain
    outer = np.intersect1d(np.where(coord_x>0),np.where(coord_y<0))
    #degrees of freedom
    dof = np.setdiff1d(range(N),bdry)
    dof = np.setdiff1d(dof,outer)
    ndof = np.size(dof)
    R = csr_matrix((np.ones(ndof),(dof,np.arange(0,ndof))),shape = (N,ndof))
    return R, outer

def solveFDM(n:int,f:callable,coord_x:np.array, coord_y:np.array) -> tuple:
    """Solving of the equation with the finite difference method (L-shaped domain)"""
    A, b, h = FDM_data(n,f)
    R,outer = restrict2dof(n,coord_x, coord_y)
    A_inner = (R.transpose()@A)@R
    b_inner = R.transpose()@b
    x = R@scipy.sparse.linalg.spsolve(A_inner,b_inner)
    return x, h, outer

#-----------------------------------------------------------------------
    # Visualization
#-----------------------------------------------------------------------
def show(coord_x:np.array, coord_y:np.array, u:np.array, uex4nodes:np.array,
         outer:np.array) -> None:
    """Routine to compare exact and computed solution"""
    plt.figure(300)
    ax_fe = plt.axes(projection ="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_fe.plot_trisurf(
        np.delete(coord_x[:,0], outer), np.delete(coord_y[:,0], outer),
        np.delete(u, outer),
        cmap = my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection ="3d")
    ax_ex.plot_trisurf(
        np.delete(coord_x[:,0], outer), np.delete(coord_y[:,0], outer),
        uex4nodes,
        cmap = plt.get_cmap("summer"),
        edgecolor ="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()

def convergence_history(hlist:np.array,errlist:np.array)-> None:
    """Plotting the convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history")
    ax.legend()
    plt.tight_layout()
    plt.show()



#-------- the numerical experiment --------------------------
if __name__ == "__main__":
    coarse = 2 #2^coarse intervals per axis
    fine = 4
    n_meshes = fine-coarse+1

    hlist = np.zeros(n_meshes)
    errlist = np.zeros(n_meshes)

    u_exact = np.vectorize(fun_sing)
    f = np.vectorize(f_sing)

    for j in range(n_meshes):
        lvl = coarse+j
        n = 2**(lvl)
        coord_x, coord_y = FDM_coords(n)
        x, h,outer = solveFDM(n,f,coord_x, coord_y)
        hlist[j] = h

        uex4nodes = np.reshape(u_exact(coord_x,coord_y),(n+1)**2)
        uex4nodes[np.ix_(outer)] = 0

        e = uex4nodes-x
        uex4nodes = np.delete(uex4nodes, outer)
        maxerr = np.max(np.abs(e))
        errlist[j] = maxerr
        print("mesh ",j+1,"\b/",n_meshes,"","%10.3E" %(n+1)**2," nodes",
            "  h=",f"{h:10.3E}","  max error=",f"{maxerr:10.3E}")

    convergence_history(hlist,errlist)
    show(coord_x,coord_y,x,uex4nodes,outer)
