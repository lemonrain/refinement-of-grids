"""Possible solution for exercise B9"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from b8 import gauss_seidel
from numpy import matlib
from tqdm import tqdm


# exact solution for purposes of comparison
def uex_fun(x,y):
    """Exact solution"""
    return np.exp(x)*x*y*(1-x)*(1-y)

# right-hand side function f
def f_fun(x,y):
    """Right-hand side"""
    return -np.exp(x)*x*(x*(y**2-y+2)+3*y**2-3*y-2)

def FDM_coords(n:int) -> tuple:
    """Calculation of the coordinates"""
    N=(n+1)**2
    coord_x=np.reshape(matlib.repmat(1/n*np.arange(0,n+1),1,n+1),(N,1),order="F")
    coord_y=np.reshape(matlib.repmat(1/n*np.arange(0,n+1),n+1,1),(N,1),order="F")
    return coord_x, coord_y

def FDM_data(n:int,f:callable) -> tuple:
    """Calculation of stiffness matrix, right hand side and grid size"""
    h=1/n
    N=(n+1)**2
    data = np.array([-np.ones(N),-np.ones(N),4*np.ones(N),-np.ones(N),-np.ones(N)])
    diags = np.array([-n-1,-1,0, 1,n+1])
    A=ssp.spdiags(data, diags, N, N).tocsr()
    coord_x, coord_y=FDM_coords(n)
    b=h**2*np.reshape(f(coord_x,coord_y),(n+1)**2)
    return A, b, h

def restrict2dof(n:int) -> ssp.csr_matrix:
    """Calculation of the degrees of freedom"""
    N=(n+1)**2
    bottom=np.arange(0,n+1)
    top=np.arange(n*(n+1),N)
    left=(n+1)*np.arange(1,n)
    right=left+n
    bdry=np.concatenate((bottom,right,top,left))
    dof=np.setdiff1d(range(N),bdry)
    ndof=np.size(dof)
    return ssp.csr_matrix((np.ones(ndof),(dof,np.arange(0,ndof))),shape = (N,ndof))


def pcg(n_iter:int, A:ssp.csr_matrix, b:np.array) -> np.array:
    """Preconditioned cg-method with Gauss-Seidel"""
    pcg_residuals=np.zeros(n_iter)
    x=np.zeros(b.size)
    normb=np.sqrt(np.dot(b,b))
    g=A*x-b
    d=-gauss_seidel(A,g,5)
    tmp=-np.dot(g,d)
    for m in range(n_iter):
        alpha=tmp/(np.dot(d,A*d))
        x=x+alpha*d
        g=g+alpha*A*d
        Cg=gauss_seidel(A,g,5)
        tmp2=np.dot(g,Cg)
        beta=tmp2/tmp
        d=-Cg+beta*d
        tmp=tmp2
        pcg_residuals[m]=np.sqrt(np.dot(g,g))/normb

    return pcg_residuals

def cg(A:ssp.csr_matrix, b:np.array, n_iter:int) -> np.array:
    """Cg-method"""
    cg_residuals=np.zeros(n_iter)
    x=np.zeros(b.size)
    g=A*x-b
    d=-g
    tmp=-np.dot(g,d)
    normb=np.sqrt(np.dot(b,b))
    for m in range(n_iter):
        alpha=tmp/(np.dot(d,A*d))
        x=x+alpha*d
        g=g+alpha*A*d
        tmp2=np.dot(g,g)
        beta=tmp2/tmp
        d=-g+beta*d
        tmp=tmp2
        cg_residuals[m]=np.sqrt(np.dot(g,g))/normb
    return cg_residuals

#-----------------------------------------------------------------------
    # Visualization
#-----------------------------------------------------------------------
def visualization(pcg_res:np.array, cg_res:np.array, n_iter:int,
                  h_list:np.array) -> None:
    """Plotting the convergence history"""
    plt.figure()
    ax = plt.axes()
    for pcg,cg, h in zip(pcg_res,cg_res,h_list):
        ax.semilogy(range(n_iter),pcg,
                    label=f"pcg residuals: mesh {h}",marker="x")
        ax.semilogy(range(n_iter),cg,label=f"cg residuals: mesh {h}",marker="x")
        ax.set_xlabel("iteration")
        ax.set_ylabel("relative residual")
        ax.legend()
    plt.tight_layout()
    plt.show()



#-------- the numerical experiment --------------------------

if __name__ == "__main__":
    u_exact=np.vectorize(uex_fun)
    f=np.vectorize(f_fun)

    n_iter=30
    coarse=2 #2^coarse intervals per axis
    fine = 6
    n_meshes=fine-coarse+1
    hlist=np.zeros(n_meshes)
    pcg_res=np.zeros((n_meshes,n_iter))
    cg_res=np.zeros((n_meshes,n_iter))

    for j in tqdm(range(n_meshes)):
        lvl=coarse+j
        n=2**(lvl)
        coord_x,coord_y = FDM_coords(n)
        A, b, h = FDM_data(n,f)
        S=restrict2dof(n)
        A=(S.transpose()@A)@S
        b=S.transpose()@b
        hlist[j]=h
        uex4nodes=np.reshape(u_exact(coord_x,coord_y),(n+1)**2)

        #pcg
        pcg_res[j,:] = pcg(n_iter,A,b)

        #cg
        cg_res[j,:] = cg(A,b,n_iter)
    visualization(pcg_res,cg_res,n_iter,hlist)
