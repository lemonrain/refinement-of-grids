"""Possible solution for exercise B6"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
import scipy.sparse.linalg
from numpy import matlib


def FDM_coords(n:int) -> tuple:
    """Calculation of the coordinates"""
    N=(n+1)**2
    coord_x=np.reshape(matlib.repmat(1/n*np.arange(0,n+1),1,n+1),(N,1),order="F")
    coord_y=np.reshape(matlib.repmat(1/n*np.arange(0,n+1),n+1,1),(N,1),order="F")
    return coord_x, coord_y

def restrict2dof(n:int) -> ssp.csr_matrix:
    """Calculation of the degrees of freedom"""
    N=(n+1)**2
    bottom=np.arange(n+1)
    top=np.arange(n*(n+1),N)
    left=(n+1)*np.arange(1,n)
    right=left+n
    bdry=np.concatenate((bottom,right,top,left))
    dof=np.setdiff1d(range(N),bdry)
    ndof=np.size(dof)

    return ssp.csr_matrix((np.ones(ndof),(dof,np.arange(ndof))),shape = (N,ndof))


#-------5 point stencil-------------

def FDM_data(n:int, f:callable) -> tuple:
    """Calculation of stiffness matrix, right hand side and grid size (5 point stencil)"""
    h=1/n
    N=(n+1)**2
    data = np.array([-np.ones(N),-np.ones(N),4*np.ones(N),-np.ones(N),-np.ones(N)])
    diags = np.array([-n-1,-1,0, 1,n+1])
    A=ssp.spdiags(data, diags, N, N).tocsr()
    coord_x, coord_y=FDM_coords(n)
    b=h**2*np.reshape(f(coord_x,coord_y),(n+1)**2)
    return A, b, h


def solveFDM(n:int, f:callable) -> tuple:
    """Solving of the equation with the finite difference method (5 point stencil)"""
    A, b, h = FDM_data(n,f)
    R=restrict2dof(n)
    A_inner=(R.transpose()@A)@R
    b_inner=R.transpose()@b
    x=R@scipy.sparse.linalg.spsolve(A_inner,b_inner)
    return x, h


#-------9 point stencil-------------

def FDM_data_9point(n:int, f:callable, delta_f:callable) -> tuple:
    """Calculation of stiffness matrix, right hand side and grid size (9 point stencil)"""
    h=1/n
    N=(n+1)**2
    data = np.array([-np.ones(N),-4*np.ones(N),-np.ones(N),-4*np.ones(N),20*np.ones(N),-4*np.ones(N),-np.ones(N),-4*np.ones(N),-np.ones(N)])
    diags = np.array([-n-2,-n-1,-n,-1,0, 1,n,n+1,n+2])
    A=ssp.spdiags(data, diags, N, N).tocsr()
    coord_x, coord_y=FDM_coords(n)
    b=6*h**2*np.reshape(f(coord_x,coord_y),(n+1)**2) + 0.5*h**4*np.reshape(delta_f(coord_x,coord_y),(n+1)**2)

    #print(A.todense())
    return A, b, h



def solveFDM_9point(n:int, f:callable, delta_f:callable) -> tuple:
    """Solving of the equation with the finite difference method (9 point stencil)"""
    A, b, h = FDM_data_9point(n,f,delta_f)
    R=restrict2dof(n)
    A_inner=(R.transpose()@A)@R

    #print(A_inner.todense())
    b_inner=R.transpose()@b
    x=R@ssp.linalg.spsolve(A_inner,b_inner)
    return x, h

#-----------------------------------------------------------------------
    # Visualization
#-----------------------------------------------------------------------

def comparison_convergence(e_5point:np.array,e_9point:np.array,J:np.array) -> None:
    """Comparison of the convergence rates"""
    fig, ax = plt.subplots()
    ax.plot(J,e_5point,label = "Error 5 point stencil")
    ax.plot(J,e_9point,label = "Error 9 point stencil")
    ax.set_xlabel("Grid points")
    ax.set_ylabel("Maximum Error norm")
    ax.legend()
    plt.tight_layout()
    plt.show()

def show(coord_x:np.array, coord_y:np.array, u:np.array, uex4nodes:np.array) -> None:
    """Routine to compare exact and computed solution"""
    plt.figure(300)
    ax_fe = plt.axes(projection ="3d")

    # Erstellen der color map

    my_cmap = plt.get_cmap("summer")

    # Erstellen des PLots
    ax_fe.plot_trisurf(coord_x[:,0], coord_y[:,0],
        u,
        cmap = my_cmap,
        edgecolor="Gray",
    )

    # Beschriftung
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")
    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection ="3d")
    ax_ex.plot_trisurf(
        coord_x[:,0],coord_y[:,0],
        uex4nodes,
        cmap = plt.get_cmap("summer"),
        edgecolor ="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()



#------ Function definition -------

def ex_func(x,y):
    """Exact soltion"""
    return (np.exp(x) *  (x- x**2) * (y- y**2))

def func(x,y):
    """Right-hand side"""
    return -np.exp(x)*x*(x*(y**2 - y + 2) + 3*(y**2) - 3*y -2)

def delta_func(x,y):
    """Double laplacian ex_func"""
    return -np.exp(x)*(x**2 *(y**2 - y + 4) + x * (7*y**2 -7*y + 12) + 8*(y-1)*y)




#-------- Numerical experiment --------------------------

if __name__ == "__main__":
    func_vectorized = np.vectorize(func)
    delta_func_vectorized = np.vectorize(delta_func)
    solution_func_vectorized = np.vectorize(ex_func)
    J_list = []
    e_5point_list = []
    e_9point_list = []

    for i in range(2,6):
        J = pow(2,i)
        J_list.append(J)

        U_5point, h = solveFDM(J,func_vectorized)
        U_9point, h = solveFDM_9point(J,func_vectorized,delta_func_vectorized)

        coord_x, coord_y=FDM_coords(J)
        uex4nodes=np.reshape(solution_func_vectorized(coord_x,coord_y),(J+1)**2)

        e_5point = uex4nodes-U_5point
        e_9point = uex4nodes-U_9point


        error_5point = np.linalg.norm(e_5point, ord=np.inf)
        error_9point = np.linalg.norm(e_9point, ord=np.inf)

        e_5point_list.append(error_5point)
        e_9point_list.append(error_9point)
        print("J: "+str(J))
        print("error 5point: "+str(error_5point))
        print("error 9point: "+str(error_9point))
    comparison_convergence(e_5point_list,e_9point_list,J_list)
    show(coord_x,coord_y,U_5point,uex4nodes)

