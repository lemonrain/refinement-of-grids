"""BPX preconditioner"""
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as ssp
from red_refine import red_refine


def get_geometry(d:int) -> tuple:
    """Generates an example of a triangulation within a given dimension

    Args:
        d: Dimension of the triangulation

    Returns:
        Gives back coordinates of nodes, triangles defined by the nodes
        Dirichlet and Neumann boundary defined by the nodes

    """
    # 2d Einheitsquadrat
    if d == 2:
        coord = np.array([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
        ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
        ])
        dirichlet = np.array([[0,1],
        [1,2],
        [2,3],
        [3,0]])
        neumann = np.zeros([0, 2])
    # 3d Einheitswürfel
    elif d == 3:
        coord = np.array([
            [0,0,0],
            [1,0,0],
            [0,0,1],
            [1,0,1],
            [0,1,0],
            [1,1,0],
            [0,1,1],
            [1,1,1],
            ])
        triangles = np.array([
            [0,1,3,7],
            [0,1,5,7],
            [0,4,5,7],
            [0,4,6,7],
            [0,2,6,7],
            [0,2,3,7],
        ])
        neumann = np.zeros([0,3])
        dirichlet = np.array([
            [0,1,5],
            [0,4,5],
            [1,3,7],
            [1,7,5],
            [2,3,7],
            [2,6,7],
            [0,2,6],
            [0,4,6],
            [0,1,3],
            [0,2,3],
            [4,6,7],
            [4,5,7],
            ])
    # L-Gebiet
    elif d == 4:
        coord = np.array([
            [0,0],
            [1,0],
            [1,1],
            [0,1],
            [-1,1],
            [-1,0],
            [-1,-1],
            [0,-1],
            ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
            [5,0,3],
            [5,3,4],
            [6,0,5],
            [6,7,0],
            ])
        dirichlet= np.array([
            [0,1],
            [1,2],
            [2,3],
            [3,4],
            [4,5],
            [5,6],
            [6,7],
            [7,0],
            ])
        neumann= np.zeros([0, 2])
    elif d == 5:
        # Quadrat auf (-1,1)^2
        # mit inhomogenen Dirichlet Randbedingungen
        coord = np.array([
            [-1,-1],
            [1,-1],
            [1,1],
            [-1,1],
            ])
        triangles = np.array([
            [0,1,2],
            [0,2,3],
            ])
        dirichlet = np.array([
            [0,1],
            [1,2],
            [2,3],
            [3,0],
            ])
        neumann = np.zeros([0, 2])
    return coord, triangles, dirichlet, neumann

def show(coord:np.array, triangles:np.array, u:np.array,
         u_exact:callable) -> None:
    """Plot routine to compare the FEM result with the exact solution"""
    plt.figure(300)
    ax_fe = plt.axes(projection="3d")

    # set color map
    my_cmap = plt.get_cmap("summer")

    # make plot
    ax_fe.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u,
        triangles=triangles,
        cmap=my_cmap,
        edgecolor="Gray",
    )

    # labeling
    ax_fe.set_xlabel("x")
    ax_fe.set_ylabel("y")
    ax_fe.set_zlabel("z")

    ax_fe.set_title("finite element solution")

    plt.figure(301)
    ax_ex = plt.axes(projection="3d")
    ax_ex.plot_trisurf(
        coord[:, 0],
        coord[:, 1],
        u_exact(coord[:, 0], coord[:, 1]),
        triangles=triangles,
        cmap=plt.get_cmap("summer"),
        edgecolor="Gray",
    )
    ax_ex.set_xlabel("x")
    ax_ex.set_ylabel("y")
    ax_ex.set_zlabel("z")

    ax_ex.set_title("exact solution")
    plt.show()



def get_area(coord:np.array) -> float:
    """Calculation of the area of a triangle with coordinates coord"""
    T = np.array([coord[1, :] - coord[0, :], coord[2, :] - coord[0, :]])
    return 0.5 * (T[0, 0] * T[1, 1] - T[0, 1] * T[1, 0])


def get_gradients(coord_loc:np.array) -> np.array:
    """Gradient calculation"""
    tmp1 = np.concatenate((np.array([[1, 1, 1]]), coord_loc.T), axis=0)
    tmp2 = np.array([[0, 0], [1, 0], [0, 1]])
    return np.linalg.solve(tmp1, tmp2)


def stiffness_matrix(coord:np.array, triangles:np.array) -> ssp.csr_matrix:
    """Routine to calculate the stiffness matrix"""
    nelems = np.size(triangles, 0)
    nnodes = np.size(coord, 0)
    Alocal = np.zeros((nelems, 3, 3))
    I1 = np.zeros((nelems, 3, 3))
    I2 = np.zeros((nelems, 3, 3))

    for j,nodes_loc in enumerate(triangles):
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        grads = get_gradients(coord_loc)
        # local stiffness matrix
        Alocal[j, :, :] = area * np.matmul(grads, grads.T)
        nodes_loc = np.array([nodes_loc])
        # indice matrices
        I1[j, :, :] = np.concatenate((nodes_loc, nodes_loc, nodes_loc), axis=0)
        I2[j, :, :] = np.concatenate((nodes_loc.T, nodes_loc.T, nodes_loc.T), axis=1)

    Alocal = np.reshape(Alocal, (9 * nelems, 1)).T
    I1 = np.reshape(I1, (9 * nelems, 1)).T
    I2 = np.reshape(I2, (9 * nelems, 1)).T
    return ssp.csr_matrix((Alocal[0, :], (I1[0, :], I2[0, :])),
                          shape=(nnodes, nnodes))


def RHS_vector(coord:np.array, triangles:np.array, f:callable) -> np.array:
    """Calculation of the right hand side over the midpoint rule"""
    nnodes = np.size(coord, 0)
    b = np.zeros(nnodes)

    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        b[nodes_loc] = b[nodes_loc] + area / 3 * f(mid[0], mid[1])
    return b


def apply_bpx(res:np.array,p_list:list,h_list:list,d:int) -> np.array:
    """Calculation of the BPX preconditioner"""
    C = np.zeros(res.shape[0])
    for p,h in zip(p_list, h_list):
        C = C + h**(2-d)*p@p.transpose()@res
    return C

def cg_precond(u:np.array, A:ssp.csc_matrix, b:np.array, p_list:list,
               h_list:list, dim:int) -> np.array:
    """Cg-method with utilization of a preconditioner"""
    res = b-A@u
    z = apply_bpx(res,p_list,h_list,dim)
    d = z
    rz_old = res.transpose()@z
    eps = 1e-4
    while np.sqrt(res.transpose()@z) > eps:
        alpha = rz_old/(d.transpose()@A@d)
        u = u+alpha*d
        res = res-alpha*A@d
        z = apply_bpx(res,p_list,h_list,dim)
        rz_new = res.transpose()@z
        beta = rz_new/rz_old
        d = z+beta*d
        rz_old = rz_new
    return u

def bpx_precond_cg(n:int,d:int,f:callable,u_exact:callable) -> tuple:
    """Function to combine all the above steps"""
    coord, triangles, dirichlet,neumann = get_geometry(d)
    n_coord_prev = np.size(coord,0)
    dof_prev = np.setdiff1d(range(n_coord_prev),np.unique(dirichlet))
    n_dof_prev = np.size(dof_prev)
    h_list = np.zeros(n)
    p1_list = []
    p_list = []
    for j in range(n):
        red_tuple = red_refine(coord,triangles, dirichlet, neumann)
        [coord, triangles, dirichlet,neumann,_,p1] = red_tuple
        n_coord = np.size(coord,0)
        dof = np.setdiff1d(range(n_coord),np.unique(dirichlet))
        n_dof = np.size(dof)

        R1 = ssp.csr_matrix(
            (np.ones(n_dof),(dof,np.arange(n_dof))),
            shape = (n_coord,n_dof),
        )
        R2 = ssp.csr_matrix(
            (np.ones(n_dof_prev),(dof_prev,np.arange(n_dof_prev))),
            shape = (n_coord_prev,n_dof_prev),
        )
        mat = (R1.transpose()@p1)@R2
        p1_list.append(mat)

        dof_prev = dof
        n_dof_prev = n_dof
        n_coord_prev = n_coord
        h_list[j] = 2**(-(j+1))
    p_list.append(ssp.eye(n_dof))
    p1_list.reverse()
    for p,p1 in zip(p_list,p1_list):
        p_list.append(p@p1)
    p_list = p_list[0:-1]
    p_list.reverse()
    S = stiffness_matrix(coord, triangles)
    S_inner = (R1.transpose()@S)@R1

    b = RHS_vector(coord, triangles,f)
    b_inner = R1.transpose()@b
    u = np.zeros(n_coord)
    u[dof] = cg_precond(u[dof],S_inner,b_inner,p_list,h_list,d)
    show(coord, triangles, u, u_exact)
    return u, coord, triangles

def convergence_history(hlist:np.array,errlist:np.array) -> None:
    """Convergence history"""
    plt.figure()
    ax = plt.axes()
    ax.loglog(1./hlist,errlist,label="error",marker="x")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**2,label="slope -2",
              linestyle="dotted")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist,label="slope -1",
              linestyle="dashed")
    ax.loglog(1./hlist,errlist[0]/hlist[0]*hlist**(2/3),label="slope -2/3",
              linestyle="dashed")
    ax.set_xlabel("1/h")
    ax.set_title("convergence history")
    ax.legend()
    plt.tight_layout()
    plt.show()


# Fehlermessung mit Energienorm
def e_norm_fun(coord:np.array, triangles:np.array, u:np.array, ux:callable,
               uy:callable)-> np.array:
    """Calculation of the error in the energy norm"""
    enorm_err = 0
    for nodes_loc in triangles:
        coord_loc = coord[nodes_loc, :]
        grads = get_gradients(coord_loc)
        area = get_area(coord_loc)
        mid = 1/3*np.sum(coord_loc,axis=0)
        enorm_err = (
            enorm_err
            + (
                (
                    ux(mid[0],mid[1])
                    - u[nodes_loc[0]] * grads[0, 0]
                    - u[nodes_loc[1]] * grads[1, 0]
                    - u[nodes_loc[2]] * grads[2, 0]
                )
                ** 2
                + (
                    uy(mid[0],mid[1])
                    - u[nodes_loc[0]] * grads[0, 1]
                    - u[nodes_loc[1]] * grads[1, 1]
                    - u[nodes_loc[2]] * grads[2, 1]
                )
                ** 2
            )
            * area
        )
    return np.sqrt(enorm_err)


def uex_fun(x,y):
        """Exact solution for purposes of comparison"""
        return (x-x**2)*(y-y**2)

def f_fun(x,y):
    """Right hand side"""
    return 2 * (x - x ** 2 + (y - y ** 2))

def ux(x,y):
    """Partial derivative"""
    return y * (y - 1) * (2 * x - 1)

def uy(x,y):
    """Partial derivative"""
    return x * (x - 1) * (2 * y - 1)

if __name__ == "__main__":
    u_exact=np.vectorize(uex_fun)
    f=np.vectorize(f_fun)
    max_err = np.zeros(6)
    red = 4
    d = 2

    h1_err_sq=np.zeros(red)
    diam=np.zeros(red)
    for n in range(1,red+1):
        u,coord, triangles = bpx_precond_cg(n,d,f,u_exact)
        h1_err_sq[n-1] = e_norm_fun(coord, triangles, u, ux, uy)

        diam[n-1] = 0.5 ** (1+n)

    # plot the convergence history
    convergence_history(diam,h1_err_sq)
    print(h1_err_sq)


