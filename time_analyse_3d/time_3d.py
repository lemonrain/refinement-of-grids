"""Time comparison for v-cycle and fem on 3d domains"""
import timeit

import numpy as np
from fem_3d import FEM
from red_refine import red_refine
from v_cycle_3d import get_geometry, v_cycle_3d


def f(x, y, z):
    """Exact solution"""
    return (x - x**2) * (y - y**2) * (z - z**2)


u_exact = np.vectorize(f)


def f(x, y, z):
    """Right hand side"""
    return (
        2 * (x - x**2) * (z - z**2)
        + 2 * (x - x**2) * (y - y**2)
        + 2 * (y - y**2) * (z - z**2)
    )


def ux(x, y, z):
    """Partial derivative of x"""
    return (1 - 2 * x) * (y - 1) * y * (z - 1) * z


def uy(x, y, z):
    """Partial derivative of y"""
    return (x - 1) * x * (1 - 2 * y) * (z - 1) * z


def uz(x, y, z):
    """Partial derivative of z"""
    return (x - 1) * x * (y - 1) * y * (1 - 2 * z)


if __name__ == "__main__":
    # muss je nach dem auf 4 oder 2 angepasst werden
    d = 3
    # k ist die Anzahl an Verfeinerungen
    k = 4
    # Anzahl an Optimierungen für den initial Input
    r = 3

    start = timeit.default_timer()
    uhut_k, A = v_cycle_3d(d, k, f, r)
    stop = timeit.default_timer()
    print("Zeit iterativ Multigrid: ", stop - start)

    start = timeit.default_timer()
    for _j in range(1, k):
        coord1, triangles1, dirichlet1, neumann1 = get_geometry(d)
        red_tuple = red_refine(coord1, triangles1, dirichlet1, neumann1)
        [coord1, triangles1, dirichlet1, _, _, _] = red_tuple
    u_k, A = FEM(coord1, triangles1, dirichlet1, f)
    stop = timeit.default_timer()
    print("Zeit direkte FEM: ", stop - start)
