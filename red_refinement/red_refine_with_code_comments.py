"""Red refinment Code with extensive comments"""
import numpy as np
import scipy as py
from edge_data import edge_data
from refinement_rule import refinement_rule
from scipy.sparse import csr_matrix


# red refinement
def red_refine(coord, triangles, dirichlet, neumann):
    # Spalten und Zeilen werden extrahiert
    [nE, _] = triangles.shape  # nE = |row_triangle|
    [nC, d] = coord.shape  # nC = |row_coord|, d = |colum_coord|

    # Aufruf der Funktion edge_data
    # edges enthält alle direkten Verbindungen zwischen den Knoten
    # el2edges enthält die Indizes, um die Verbindungen zwischen den Kanten
    # rekonstruieren zu können
    # Db2edges/ Nb2edges ist dafür da, um die Verbindungen des Dirichlet-/
    # Neumannrandes rekonstruieren zu können
    edges, el2edges, Db2edges, Nb2edges = edge_data(triangles, dirichlet, neumann)
    # cast edges to int
    edges = edges.astype(int)

    [nS, _] = edges.shape  # nS = |row_edges|
    new_nodes = nC + np.transpose(
        np.arange(nS),
    )  # transponiertes vom Vektor mit den Werten 0 bis nS-1 + nC
    mat1 = new_nodes[el2edges]  # Schreibe new_nodes[i] auf i = el2egdes[j, k]
    new_indices = mat1.reshape(
        el2edges.shape, order="F",
    ).copy()  # Benutze Fortran, um Einträge Spaltenweise einzutragen

    # Aufruf der Funktion refinement_rule, hier ist die Art und Weise, wie die
    # Verfeinerung vorgenommen wird gespeichert
    idx_elements = refinement_rule(d)

    # die Verfeinerung der Dreiecke wird durchgeführt
    triangles = np.concatenate(
        (triangles, new_indices), axis=1,
    )  # Matrizen werden untereinander zusammengefügt
    # durch triangles[:, idx_elements] erhält man eine Matrix, die aus den Spalten
    # von triangles besteht und deren auftreten durch die Elemente in idx_elements
    # bestimmt wird
    triangles = np.transpose(triangles[:, idx_elements])
    triangles = np.transpose(
        triangles.reshape(d + 1, -1, order="F").copy(),
    )  # Benutze Fortran, um Einträge Spaltenweise einzutragen

    # die neuen Knoten werden erzeugt
    # durch coord[edges[:, j]] erhält man eine Matrix m
    # mit Einträgen m[k,i] = coord[edges[k, j],i]
    new_coord = 0.5 * (coord[edges[:, 0]] + coord[edges[:, 1]])
    coord = np.concatenate(
        (coord, new_coord), axis=0,
    )  # Matizen werden nebeneinander zusammengefügt

    # erzeugen des neuen Neumann- und Dirichletrand
    idx_boundary = refinement_rule(d - 1)  # Aufruf der Funktion refinement_rule
    if (
        np.size(dirichlet, 0) > 0
    ):  # wenn keine Dirichlet Ränder vorhanden sind, wird das hier abgefangen
        mat2 = new_nodes[Db2edges]  # Schreibe new_nodes[i] auf i = Db2egdes[j, k]
        new_dirichlet = mat2.reshape(
            Db2edges.shape, order="F",
        ).copy()  # Benutze Fortran, um Einträge Spaltenweise einzutragen
        dirichlet = np.concatenate(
            (dirichlet, new_dirichlet), axis=1,
        )  # Matizen werden untereinander zusammengefügt
        # durch dirichlet[:, idx_boundary] erhält man eine Matrix, die aus den
        # Spalten von dirichlet besteht und deren auftreten durch die Elemente in
        # idx_boundary bestimmt wird
        dirichlet = np.transpose(dirichlet[:, idx_boundary])
        dirichlet = np.transpose(
            dirichlet.reshape(d, -1, order="F").copy(),
        )  # Benutze Fortran, um Einträge Spaltenweise einzutragen
    if (
        np.size(neumann, 0) > 0
    ):  # wenn keine Neumann Ränder vorhanden sind, wird das hier abgefangen
        mat3 = new_nodes[Nb2edges]  # Schreibe new_nodes[i] auf i = Nb2egdes[j, k]
        new_neumann = mat3.reshape(
            Nb2edges.shape, order="F",
        ).copy()  # Benutze Fortran, um Einträge Spaltenweise einzutragen
        neumann = np.concatenate(
            (neumann, new_neumann), axis=1,
        )  # Matizen werden untereinander zusammengefügt
        # durch neumann[:, idx_boundary] erhält man eine Matrix, die aus den Spalten
        # von neumann besteht und deren auftreten durch die Elemente in idx_boundary
        # bestimmt wird
        neumann = np.transpose(neumann[:, idx_boundary])
        neumann = np.transpose(
            neumann.reshape(d, -1, order="F").copy(),
        )  # Benutze Fortran, um Einträge Spaltenweise einzutragen

    # erzeugen von Sparse- Martizen und das anschließende schreiben in eine
    # komprimierte Form,
    # in der nur die Teile der Matrix angegeben werde, die nicht Null sind und deren
    # Platz in der Matrix
    # diese erlauben uns, wenn wir eine finite Elemente Funktion auf dem groben
    # Gitter haben,
    # diese auf das feine Gitter zu legen über Matrix- Vektor- Multiplikation
    # p0 ist die für stückweise konstante Funktion
    # p1 ist die für stetige stückweise affine Funktionen
    p0 = get_sparse_matrix_p0(nE, d, triangles)  # Aufruf der Funktion
    p1 = get_sparse_matrix_p1(nC, new_nodes, edges, nS)  # Aufruf der Funktion
    return coord, triangles, dirichlet, neumann, p0, p1


def edge_data(triangles, dirichlet, neumann):
    # Festlegung der Werte nach dem Pseudocode
    [nE, nV] = triangles.shape  # nE = |row_t|, nV = |colum_t|
    d = nV - 1
    idx = np.array([0, 1, 3, 6])  # erstelle numpy array
    boundary = np.concatenate((dirichlet, neumann), axis=0)  # [;]
    new_edges = (
        idx[d] * nE
    )  # Python beginnt bei 0, dehalb wird idx[d] genommen, anders als beim
    # Ausgangsalgorithmus
    new_dirichlet = idx[d - 1] * dirichlet.shape[0]
    new_neumann = idx[d - 1] * neumann.shape[0]

    # erhalte alle Verbindungen zwischen den Knoten
    edges = get_edges(d, triangles, boundary)  # aufruf der switcher Funktion

    # sortiere edges, gebe jeweils nur die erste Verbindung für die Knoten zurück
    # edge_numbers sind die Indizes, um das alte Array rekonstruieren zu können
    edges, edge_numbers = np.unique(np.sort(edges), return_inverse=True, axis=0)

    # forme edge_numbers in ein Array der Form von new_edges um
    el2edges = get_el2edges(new_edges, edge_numbers, idx, d)
    # erhalte die Indzies, um den Dirichletrand rekonstruieren zu können
    Db2edges = get_Db2edges(new_edges, edge_numbers, new_dirichlet, idx, d)
    # erhalte die Indzies, um den Neumannrand rekonstruieren zu können
    Nb2edges = get_Nb2edges(
        new_edges, edge_numbers, new_dirichlet, new_neumann, idx, d,
    )
    return edges, el2edges, Db2edges, Nb2edges


# switch case durch dictionaries, da es in python keine switch Funktion gibt
# d = 1 -> case_1
# d = 2 -> case_2
# d = 3 -> case_3
def get_edges(d, triangles, boundary):
    switcher = {
        1: case_1,
        2: case_2,
        3: case_3,
    }
    func = switcher.get(d)
    return func(triangles, boundary)


# auslagerung der cases
def case_1(A, B):
    return A


def case_2(A, B):
    return np.concatenate((get_matrix(A, [0, 1, 0, 2, 1, 2]), B))


def case_3(A, B):
    edges = np.transpose(get_matrix(A, [0, 1, 0, 2, 0, 3, 1, 2, 1, 3, 2, 3]))
    return np.concatenate(
        edges, get_matrix(B, [0, 1, 0, 2, 1, 2]), axis=0,
    )  # Matritzen werden nebeneinander zusammengefügt


# von einer gegebenen Matrix werden die Spalten, die in der gegebenen Liste stehen
# hintereinander gereiht und dann in die gewünschte Form gebracht
def get_matrix(A, list_of_colums):
    new_A = np.transpose(
        A[:, list_of_colums],
    )  # ergibt eine Liste mit den Spalten, die wir aufrufen
    reshaped_A = new_A.reshape(
        2, -1, order="F",
    ).copy()  # damit erziehlt man den reshape() von matlab mit Fortran
    return np.transpose(reshaped_A)


def get_el2edges(new_edges, edge_numbers, idx, d):
    edge_numbers = np.transpose(
        edge_numbers[0:new_edges],
    )  # nehme nur die edge_numbers von 0,..., new_edges-1
    el2edges = edge_numbers.reshape(
        idx[d], -1, order="F",
    ).copy()  # Benutze Fortran, um Einträge Spaltenweise einzutragen
    return np.transpose(el2edges)


def get_Db2edges(new_edges, edge_numbers, new_dirichlet, idx, d):
    ind1 = np.arange(new_dirichlet) + new_edges
    edge_numbers1 = edge_numbers[
        ind1
    ]  # speichere edge_numbers an der Stelle ind1[0] und ind1[1]
    edge_numbers1 = np.transpose(edge_numbers1)
    Db2edges = edge_numbers1.reshape(
        idx[d - 1], -1, order="F",
    ).copy()  # Benutze Fortran, um Einträge Spaltenweise einzutragen
    return np.transpose(Db2edges)


def get_Nb2edges(new_edges, edge_numbers, new_dirichlet, new_neumann, idx, d):
    ind2 = np.arange(new_neumann) + new_edges + new_dirichlet
    edge_numbers2 = edge_numbers[
        ind2
    ]  # speichere edge_numbers an der Stelle ind2[0] und ind2[1]
    edge_numbers2 = np.transpose(edge_numbers2)
    Nb2edges = edge_numbers2.reshape(
        idx[d - 1], -1, order="F",
    ).copy()  # Benutze Fortran, um Einträge Spaltenweise einzutragen
    return np.transpose(Nb2edges)


# je nach Dimension d wird entschieden, wie die Verfeinerung ablaufen soll
def refinement_rule(d):
    switcher = {
        0: 0,
        1: [0, 2, 2, 1],
        2: [0, 3, 4, 4, 5, 2, 3, 1, 5, 5, 4, 3],
        3: [
            0,
            4,
            5,
            6,
            4,
            1,
            7,
            8,
            5,
            7,
            2,
            9,
            6,
            8,
            9,
            3,
            4,
            5,
            6,
            8,
            8,
            5,
            7,
            4,
            5,
            6,
            8,
            9,
            9,
            7,
            8,
            5,
        ],
    }
    return switcher.get(d)


def get_sparse_matrix_p0(nE, d, triangles):
    # np.tile ist repmat in python, d.h np.arange(nE) wird 2^d mal untereinander und
    # einmal nebeneinander geschrieben
    col = np.tile(np.arange(nE), (2**d, 1)).reshape(1, -1, order="F").copy()
    col = col[0]  # bringe col in die gleiche Form, wie die restlichen Vektoren
    [t, _] = triangles.shape  # neu berechnete Dreiecke
    row = np.arange(t)  # bilde numpy array mit Werten 0,...,t-1
    data = np.full(t, 1)  # wiederhole t mal die 1
    # erstelle Matrix in der Göße, des größten Wertes aus row + 1 und dem größten
    # Wert aus col + 1
    # mit den Werten aus data, jeweils an der Stelle [row[i], col[i]] für data[i]
    sparseMatrix = csr_matrix(
        (data, (row, col)), shape=(max(row) + 1, max(col + 1)),
    ).toarray()  # max + 1, da python bei 0 beginnt
    # bringe die erstellte Matrix in die Form (Zeile, Spalte) Wert
    return py.sparse.csc_matrix(sparseMatrix)


def get_sparse_matrix_p1(nC, new_nodes, edges, nS):
    t = np.arange(nC)  # bilde numpy array mit Werten 0,...,nC-1
    mat1 = np.concatenate(
        (t, new_nodes, new_nodes),
    )  # Matrizen werden nebeneinander zusammengefügt
    mat2 = np.concatenate(
        (t, edges[:, 0], edges[:, 1]),
    )  # Matrizen werden nebeneinander zusammengefügt
    mat3 = np.concatenate(
        (np.full(nC, 1), 0.5 * np.full(2 * nS, 1)),
    )  # Matrizen werden nebeneinander zusammengefügt
    sparseMatrix = csr_matrix(
        (mat3, (mat1, mat2)), shape=(nC + nS, nC),
    ).toarray()  # hier ist die Größe der Matrix gegeben
    # bringe die erstellte Matrix in die Form (Zeile, Spalte) Wert
    return py.sparse.csc_matrix(sparseMatrix)
