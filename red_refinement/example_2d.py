"""Example for red refinement on a 2d domain"""
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import numpy as np
from red_refine import red_refine

# triangulation of the unit square
coord = np.array([[0, 0], [1, 0], [1, 1], [0, 1]])
triangles = np.array([[0, 1, 2], [0, 2, 3]])
dirichlet = np.array([[0, 1], [1, 2], [2, 3], [3, 0]])
neumann = np.zeros([0, 2])

# refinement through red refine
n = 1
for _ in range(n):
    coord, triangles, dirichlet, neumann, _, _ = red_refine(
        coord, triangles, dirichlet, neumann,
    )
# plotting of the refined triangulation
tri = mtri.Triangulation(coord[:, 0], coord[:, 1], triangles)
plt.triplot(tri)
plt.show()
