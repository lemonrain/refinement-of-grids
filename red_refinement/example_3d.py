"""Example for red refinement on a 3d domain"""
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as a3
import numpy as np
from red_refine import red_refine


def get_faces(tets:np.array)->np.array:
    """Get faces of the triangulation"""
    n = 4 * tets.shape[0]
    tris = np.zeros([n, 3], dtype=int)
    i = 0
    for tet in tets:
        tris[i] = [tet[0], tet[2], tet[1]]
        tris[i + 1] = [tet[0], tet[3], tet[1]]
        tris[i + 2] = [tet[0], tet[2], tet[3]]
        tris[i + 3] = [tet[1], tet[2], tet[3]]
        i = i + 4
    tris = np.sort(tris)
    return np.unique(tris, axis=0)

def plot_3d_cube(coord:np.array, triangles:np.array)-> None:
    """Plotting of the cube"""
    fig = plt.figure()
    axes = plt.axes(projection="3d")
    tris = get_faces(triangles)
    vts = coord[tris, :]
    tri = a3.art3d.Poly3DCollection(vts)
    tri.set_alpha(0.2)
    tri.set_color("grey")
    axes.add_collection3d(tri)
    axes.plot(coord[:, 0], coord[:, 1], coord[:, 2], "ko")
    axes.set_axis_off()
    axes.set_aspect("auto")
    plt.show()



# triangulation of a cube
coord = np.array(
    [
        [0, 0, 0],
        [0, 0, 1],
        [1, 0, 0],
        [1, 0, 1],
        [0, 1, 0],
        [0, 1, 1],
        [1, 1, 0],
        [1, 1, 1],
    ],
)
triangles = np.array(
    [
        [0, 1, 3, 7],
        [0, 2, 3, 7],
        [0, 1, 5, 7],
        [0, 4, 5, 6],
        [0, 2, 6, 7],
        [0, 4, 6, 7],
    ],
)
neumann = np.zeros([0, 3])
dirichlet = np.array(
    [
        [0, 2, 6],
        [1, 5, 6],
        [0, 1, 5],
        [5, 4, 6],
        [0, 4, 6],
        [4, 6, 7],
        [2, 3, 7],
        [3, 7, 6],
        [3, 2, 6],
        [1, 0, 2],
        [0, 2, 3],
    ],
)

# plotting the cube
plot_3d_cube(coord,triangles)

# refinement of the cube
n = 1
for _ in range(n):
    coord, triangles, dirichlet, neumann, _, _ = red_refine(
        coord, triangles, dirichlet, neumann,
    )

# plotting the refined cube
plot_3d_cube(coord,triangles)
