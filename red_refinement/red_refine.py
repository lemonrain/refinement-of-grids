"""Routine to calculate the red refinement of a triangulation"""
import numpy as np
import scipy.sparse as ssp
from edge_data import edge_data
from refinement_rule import refinement_rule


def red_refine(coord:np.array, triangles:np.array, dirichlet:np.array,
               neumann:np.array)->tuple:
    """Refines the triangulation with the red refinement

    Args:
        coord: coordinates of the triangulation
        triangles: manner of the triangles of the triangulation
        dirichlet: dirichlet boundary values
        neumann: neumann boundary values


    Returns:
        Refined coord, triangles, dirichlet and neumann
        as well as p0 and p1

    """
    [nE, _] = triangles.shape
    [nC, d] = coord.shape

    # get edge data
    edges, el2edges, Db2edges, Nb2edges = edge_data(triangles, dirichlet, neumann)
    edges = edges.astype(int)

    [nS, _] = edges.shape
    new_nodes = nC + np.transpose(np.arange(nS))
    mat1 = new_nodes[el2edges]
    new_indices = mat1.reshape(el2edges.shape, order="F").copy()

    idx_elements = refinement_rule(d)

    # generate refined triangles
    triangles = np.concatenate((triangles, new_indices), axis=1)
    triangles = np.transpose(triangles[:, idx_elements])
    triangles = np.transpose(triangles.reshape(d + 1, -1, order="F").copy())

    # generate new coordinates
    new_coord = 0.5 * (coord[edges[:, 0]] + coord[edges[:, 1]])
    coord = np.concatenate((coord, new_coord), axis=0)

    # generate new boundary values
    idx_boundary = refinement_rule(d - 1)
    if np.size(dirichlet, 0) > 0:
        mat2 = new_nodes[Db2edges]
        new_dirichlet = mat2.reshape(Db2edges.shape, order="F").copy()
        if new_dirichlet.size:
            dirichlet = np.concatenate((dirichlet, new_dirichlet), axis=1)
            dirichlet = np.transpose(dirichlet[:, idx_boundary])
            dirichlet = np.transpose(dirichlet.reshape(d, -1, order="F").copy())
    if np.size(neumann, 0) > 0:
        mat3 = new_nodes[Nb2edges]
        new_neumann = mat3.reshape(Nb2edges.shape, order="F").copy()
        if new_neumann.size:
            neumann = np.concatenate((neumann, new_neumann), axis=1)
            neumann = np.transpose(neumann[:, idx_boundary])
            neumann = np.transpose(neumann.reshape(d, -1, order="F").copy())

    # get prolongation matrices
    p0 = get_sparse_matrix_p0(nE, d, triangles)
    p1 = get_sparse_matrix_p1(nC, new_nodes, edges, nS)
    return coord, triangles, dirichlet, neumann, p0, p1


def get_sparse_matrix_p0(nE:int, d:int, triangles:np.array)-> ssp.csc:
    """Prolongation matrix for piecewise constant functions

    Used for computing the coefficients of a given
    finite element function on the coarse triangulation with respect to the nodal
    basis on the refined triangulation by a matrix vector multiplication
    """
    col = np.tile(np.arange(nE), (2**d, 1)).reshape(1, -1, order="F").copy()
    col = col[0]
    [t, _] = triangles.shape
    row = np.arange(t)
    data = np.full(t, 1)
    p0 = ssp.csr_matrix(
        (data, (row, col)), shape=(max(row) + 1, max(col + 1)),
    ).astype(dtype="float64")
    return p0.tocsc()


def get_sparse_matrix_p1(nC:int, new_nodes:np.array, edges:np.array,
                         nS:int)->ssp.csc:
    """Prolongation matrix for continuous, piecewise affine functions functions

    Used for computing the coefficients of a given
    finite element function on the coarse triangulation with respect to the nodal
    basis on the refined triangulation by a matrix vector multiplication
    """
    t = np.arange(nC)
    mat1 = np.concatenate((t, new_nodes, new_nodes))
    mat2 = np.concatenate((t, edges[:, 0], edges[:, 1]))
    mat3 = np.concatenate((np.full(nC, 1), 0.5 * np.full(2 * nS, 1)))
    p1 = ssp.csr_matrix((mat3, (mat1, mat2)), shape=(nC + nS, nC)).astype(
        dtype="float64",
    )
    return p1.tocsc()
