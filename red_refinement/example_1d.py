"""Example for red refinement on a 1d domain"""
import matplotlib.pyplot as plt
import numpy as np
from red_refine import red_refine

# triangulation of a line
coord = np.array([[0], [1]])
section = np.array([[0, 1]])
dirichlet = np.array([0])
neumann = np.array([1])

# plot the line
plt.hlines(1, coord[0], coord[1])
plt.xlim(0, 2)
plt.ylim(0.5, 1.5)
plt.eventplot(
    section,
    orientation="horizontal",
    lineoffsets=1,
    linelengths=0.1,
    linestyles="solid",
)
plt.axis("off")
plt.show()

# refinement through red refine
n = 2
for _ in range(n):
    coord, section, dirichlet, neumann, _, _ = red_refine(
        coord, section, dirichlet, neumann,
    )
section_list = [section[i] for i in range(section.shape[0])]

sec = set()
for arrs in section_list:
    sec.add(arrs[0])
    sec.add(arrs[1])

# plot the refined line
sec = list(sec)
plt.hlines(1, coord[0], coord[1])
plt.xlim(0, 2)
plt.ylim(0.5, 1.5)
y = np.ones(np.shape(sec))
plt.plot(coord[sec], y, "|", ms=40)
plt.axis("off")
plt.show()
