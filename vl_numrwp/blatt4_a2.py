"""Mögliche Lösung für Blatt 4 Aufgabe 2"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize


def expl_runge(f:callable, a:int, teta: float, u0:int, n:int) -> tuple:
    """Explizites Runge Kutta Verfahren auch klassisches Runge Kutta Verfahren"""
    # Einspeisen der Anfangswerte
    u = np.zeros(n)
    t = np.zeros(n)
    t[0]=a
    u[0]=u0

    # Berechnen der einzelnen Werte nach den Vorgaben
    for k in range(1,n):
        k1 = f(t[k-1],u[k-1])
        k2 = f(t[k-1]+teta/2,u[k-1]+teta/2*k1)
        k3 = f(t[k-1]+teta/2,u[k-1]+teta/2*k2)
        k4 = f(t[k-1]+teta,u[k-1]+teta*k3)
        u[k] = u[k-1]+teta/6*k1+teta/3*k2+teta/3*k3+teta/6*k4
        t[k] = t[0] + k * teta
    return t,u

#--------------------------------------------------------------------------------------------------

def h_func(k,f,t,teta,u):
    """Hilfsfunktion"""
    return [f(t+teta*(1/2-1/6*np.sqrt(3)),
              u+teta/4*k[0] +(1/4-1/6*np.sqrt(3))*teta*k[1])-k[0],
            f(t+teta*(1/2+1/6*np.sqrt(3)),
              u+teta/4*k[1] +(1/4+1/6*np.sqrt(3))*teta*k[0])-k[1]]


def gauss(f:callable, a:int, n:int, u0:int, teta:float) -> tuple:
    """Implizites Runge Kutta Verfahren auch Gauß Verfahren"""
    u = np.zeros(n)
    t = np.zeros(n)
    t[0]=a
    u[0]=u0
    k1 = 0
    k2 = 0
    for k in range(1,n):
        [k1,k2] = optimize.fsolve(h_func,[k1,k2],args=(f,t[k-1],teta,u[k-1]))
        u[k]=u[k-1] + teta/2*k1+teta/2*k2
        t[k] = t[0] + k * teta
    return t,u


def eoc(err:np.array, tau:np.array) -> np.array:
    """Funktion zu Berechnung der experimentellen Konvergenzrate"""
    eoc = np.zeros(5)
    for j in range(5):
        eoc[j] = (np.log(err[j+1])-np.log(err[j]))/(np.log(tau[j+1])-np.log(tau[j]))
    return eoc

def present(f:callable, a:int, teta:np.array, u0:int, n_anz:np.array,
            func_exact:callable) -> None:
    """Visualisierung der Ergebnisse"""
    fig, axs = plt.subplots(3,2, figsize=(18, 12))
    fig.canvas.manager.set_window_title("Runge Kutta Verfahren")
    axs = axs.ravel()
    max_err_runge = np.zeros(6)
    max_err_gauss = np.zeros(6)
    i=0
    for n in n_anz:
        teta = tau[i]
        x,y = expl_runge(f, a, teta, u0, n)
        axs[i].plot(x,y,"-x",color="blue")
        x,z = gauss(f, a, n, u0, teta)
        axs[i].plot(x, z, "-x", color="green")
        axs[i].plot(x, func_exact(x),"-x",color="red")
        axs[i].legend(["Runge Kutta", "Gauss", "Loesung"])
        axs[i].set_title(f"N = {n}")
        # Runge Kutta Verfahren haben immer 4te Ordnung
        # Berechnung des Diskretisierungsfehlers
        max_err_runge[i] = np.max(np.abs(func_exact(x)-y))
        max_err_gauss[i] = np.max(np.abs(func_exact(x)-z))
        i=i+1
    eoc_runge = eoc(max_err_runge, tau)
    eoc_gauss = eoc(max_err_gauss, tau)
    print("Diskretisierungsfehler für das Runge-Kutta-Verfahren = ", max_err_runge)
    print("Diskretisierungsfehler für das Gauss-Verfahren = ", max_err_gauss)

    print("Experimentelle Konvergenzrate für das Runge-Kutta-Verfahren = ", eoc_runge)
    print("Experimentelle Konvergenzrate für das Gauss-Verfahren = ", eoc_gauss)
    plt.show()

def f(t, u):
    """Rechte Seite"""
    return -2 * t + 3 * t * u - t * u ** 2
def func_exact(t):
    """Exakte Lösung"""
    return 1 + 1 / (1 - 1 / 2 * np.exp(-t ** 2 / 2))


if __name__ == "__main__":
    # vorgegebene Funktionen
    u0 = 3
    a = 0
    b = 1
    tau = [1/2, 1/(2**2), 1/(2**3), 1/(2**4), 1/(2**5), 1/(2**6)]
    n_anz = []
    for t in tau:
        n_anz.append(int((b-a)/t)+1)

    # Programme Ausfuehren
    present(f, a, tau, u0, n_anz, func_exact)
