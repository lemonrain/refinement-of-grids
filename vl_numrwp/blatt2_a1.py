"""Mögliche Lösung für Blatt 2 Aufagbe 1"""
import timeit

import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize


def exp_euler(f:callable, u_0:int, a:int, b:int, n:int) -> tuple:
    """Explizites Euler Verfahren"""
    # Einspeisung der Anfangswerte
    teta = (b-a)/n
    h = int((b-a)/teta)
    u = np.zeros(h)
    t = np.zeros(h)
    t[0]=a
    u[a]=u_0
    for k in range(1,h):
        # direkte Berechnung mit explizitem Euler
        u[k] = u[k-1] + teta*f(t[k-1],u[k-1])
        t[k] = t[0] + k * teta
    return t,u

def fun_imp(z, teta, u, t):
    """Umschreiben der rechten Seite zur Anwendung des Newton Verfahren"""
    return u + teta * f(t, z) - z

def imp_euler(u_0:int, a:int, b:int, n:int) -> tuple:
    """Impliziter Euler"""
    # Einspeisung der Anfangswerte
    teta = (b-a)/n
    h = int((b-a)/teta)
    u = np.zeros(h)
    t = np.zeros(h)
    t[0]=a
    u[a]=u_0
    # umschreiben, damit es mit dem Newtonverfahren gelöst werden kann
    for k in range(1,h):
        # Ratewert für die Nullstelle über Explizites Eulerverfahren berechnet
        u[k] = optimize.newton(fun_imp, u[k-1], args=[teta, u[k-1], t[k-1]])
        # kann man eigentlich weglassen
        t[k] = t[0] + k * teta
    return t,u

def fun_ck(z, teta, u, t):
    """Umschreiben der rechten Seite zur Anwendung des Newton Verfahren"""
    return u + teta / 2 * (f(t, u) + f(t, z)) - z

def crank_nicolson(u_0:int, a:int, b:int, n:int) -> tuple:
    """Crank-Nicolson Verfahren"""
    # Einspeisung der Anfangswerte
    teta = (b-a)/n
    h = int((b-a)/teta)
    u = np.zeros(h)
    t = np.zeros(h)
    t[0]=a
    u[a]=u_0
    for k in range(1,h):
        # Ratewert als vorheriges Ergebnis wählen
        u[k] = optimize.newton(fun_ck, u[k-1], args=[teta, u[k-1], t[k-1]])
        t[k] = t[0] + k * teta
    return t,u

def present(f:callable, u_0:int, a:int, b:int, f_exact:callable) -> None:
    """Visualisierung der Ergebnisse"""
    fig, axs = plt.subplots(2,2, figsize=(18, 12))
    fig.canvas.manager.set_window_title("Euler Verfahren, Crank Nicolson Verfahren")
    axs = axs.ravel()
    i=0
    for n in [10, 15, 20, 50]:
        x,y = crank_nicolson(u_0,a,b,n)
        axs[i].plot(x,y,color="blue")
        x,z = exp_euler(f,u_0,a,b,n)
        axs[i].plot(x,z,color="green")
        x,z1 = imp_euler(u_0,a,b,n)
        axs[i].plot(x,z1,color="yellow")
        axs[i].plot(x,f_exact(x),color="red")
        axs[i].legend(["CNV","Euler expl","Euler impl","Loesung"])
        axs[i].set_title(f"N = {n}")
        i=i+1
    plt.show()

# Funktion y'
def f(t, y):
    """Rechte Seite"""
    return -20 * y

def f_exact(t):
    """Exakte Lösung mit Anfangswert y(0)=1"""
    return np.exp(-20 * t)


if __name__ == "__main__":
    a = 0
    b = 1
    u_0 = 1
    # ausführen und plotten der verschiedenen Methoden
    present(f, u_0, a, b, f_exact)


    # messen der Zeit, die die verschiedenen Methoden zum Berechnen brauchen
    n = 100

    start = timeit.default_timer()
    u_ee = exp_euler(f, u_0, a, b, n)
    stop = timeit.default_timer()
    print("Zeit expliziter Euler: ", stop - start)

    start = timeit.default_timer()
    u_ee = imp_euler(u_0, a, b, n)
    stop = timeit.default_timer()
    print("Zeit impliziter Euler: ", stop - start)

    start = timeit.default_timer()
    u_ee = crank_nicolson(u_0, a, b, n)
    stop = timeit.default_timer()
    print("Zeit Crank-Nicolson: ", stop - start)
