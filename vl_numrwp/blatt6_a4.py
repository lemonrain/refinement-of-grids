"""Mögliche Lösung Blatt 6 Aufgabe 4"""
import matplotlib.pyplot as plt
import numpy as np


def expl_runge(f:callable, a:int, teta:float, u0:int, n:int) -> tuple:
    """Explizites Runge Kutta Verfahren"""
    # Einspeisen der Anfangswerte
    u = np.zeros(n)
    t = np.zeros(n)
    t[0]=a
    u[0]=u0

    # Berechnen der einzelnen Werte nach den Vorgaben
    for k in range(1,n):
        k1 = f(t[k-1],u[k-1])
        k2 = f(t[k-1]+teta/2,u[k-1]+teta/2*k1)
        k3 = f(t[k-1]+teta/2,u[k-1]+teta/2*k2)
        k4 = f(t[k-1]+teta,u[k-1]+teta*k3)
        u[k] = u[k-1]+teta/6*k1+teta/3*k2+teta/3*k3+teta/6*k4
        t[k] = t[0] + k * teta
    return t,u
#--------------------------------------------------------------------------------------------------

def adams(f:callable, a:int, n:int, u0:int, teta:float) -> tuple:
    """Adams-Bashfort Verfahren"""
    u = np.zeros(n)
    t = np.zeros(n)
    # Berechnen der ersten vier Werte durch das expl. Runge Kutta Verfahren
    t[:4], u[:4]= expl_runge(f, a, teta, u0, 4)
    for k in range(4,n):
        # Berechnen nach Vorgabe
        u[k]=u[k-1] + teta/24*(55*f(t[k-1],u[k-1])
                               -59*f(t[k-2],u[k-2])
                               +37*f(t[k-3],u[k-3])
                               -9*f(t[k-4],u[k-4]))
        t[k] = t[0] + k * teta
    return t,u

def eoc(err:np.array, tau:np.array) -> np.array:
    """Funktion zu Berechnung der experimentellen Konvergenzrate"""
    eoc = np.zeros(5)
    for j in range(5):
        eoc[j] = (np.log(err[j+1])-np.log(err[j]))/(np.log(tau[j+1])-np.log(tau[j]))
    return eoc

def present(n_anz:np.array, f:callable, a:int, u0:int, tau:np.array,
            func_exact:callable) -> None:
    """Visualisierung der Ergebnisse"""
    fig, axs = plt.subplots(3,2, figsize=(18, 12))
    fig.canvas.manager.set_window_title("Adams-Bashfort Verfahren")
    axs = axs.ravel()
    max_err_adams = np.zeros(6)
    max_err_runge = np.zeros(6)
    i=0
    for n in n_anz:
        teta = tau[i]
        x,z = adams(f, a, n, u0, teta)
        axs[i].plot(x,z,"-x",color="green")
        axs[i].plot(x,func_exact(x),"-x",color="red")
        x,y = expl_runge(f, a, teta, u0, n)
        axs[i].legend(["Adams-Bashfort","Loesung"])
        axs[i].set_title(f"N = {n}")
        # Berechnung des Diskretisierungsfehlers
        max_err_runge[i] = np.max(np.abs(func_exact(x)-y))
        max_err_adams[i] = np.max(np.abs(func_exact(x)-z))
        i=i+1
    eoc_adams = eoc(max_err_adams,tau)
    eoc_runge = eoc(max_err_runge,tau)
    print("Diskretisierungsfehler für das Adams-Bashfort-Verfahren = ",max_err_adams)
    print("Diskretisierungsfehler für das Runge-Kutta-Verfahren = ",max_err_runge)

    print("Experimentelle Konvergenzrate für das Adams-Bashfort-Verfahren = ",eoc_adams)
    print("Experimentelle Konvergenzrate für das Runge-Kutta-Verfahren = ",eoc_runge)
    plt.show()


def f(t, u):
    """Rechte Seite"""
    return -2 * t + 3 * t * u - t * u ** 2
def func_exact(t):
    """Exakte Lösung"""
    return 1 + 1 / (1 - 1 / 2 * np.exp(-t ** 2 / 2))

if __name__ == "__main__":
    u0 = 3
    a = 0
    b = 1
    tau = [1/(2**2),1/(2**3),1/(2**4),1/(2**5),1/(2**6),1/(2**7)]
    n_anz = []
    for t in tau:
        n_anz.append(int((b-a)/t)+1)
    # Programme Ausfuehren
    present(n_anz, f, a, u0, tau, func_exact)
