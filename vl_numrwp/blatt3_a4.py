"""Mögliche Lösung für Blatt 3 Aufgabe 4"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import optimize


def leapfrog(a:int,b:int,n:int,v:float,w:int) -> tuple:
    """Leapfrog-Verfahren"""
    # Einspeisung der Anfangswerte
    teta = (b-a)/n
    u = np.zeros(n)
    t = np.zeros(n)
    t[0]=a
    u[0]=v
    u[1]=w*teta+u[0]

    for k in range(2,n):
        # umstellen, sodass sich ein explizites Verfahren ergibt
        u[k] = 2*u[k-1]-u[k-2]-teta**2*(g/l)*u[k-1]
        t[k] = t[0] + k * teta
    return t,u

#------------------------------------------------------------------------------
#Crank-Nicolson-Verfahren

#Hilfsfunktion
def func_onedim(xk,y1,y2,g,l):
    return np.array([y2,
        (-g/l)*(y1)])

def h_func1(z,s,xk,h,g,l):
    return z - (h/2)*func_onedim(xk,z[0],z[1],g,l) + s

def cnv(a:int, b:int, n:int, v:float, w:int) -> tuple:
    """Crank-Nicolson-Verfahren"""
    # Einspeisen der Anfangswerte
    h=(b-a)/n
    y1=np.zeros(n)
    y2=np.zeros(n)
    x=np.linspace(a,b,n)
    y1[0]=v
    y2[0]=w
    s=0
    for i in range(1,n):
        # Umstellen, damit es mit fsolve gelöst werden kann
        s=np.array([-y1[i-1],-y2[i-1]]) - (h/2)*func_onedim(x[i-1],y1[i-1], y2[i-1],
                                                            g, l)
        # als Ratewert die vorher berechneten Ergebnisse verwenden
        [y1[i],y2[i]]=optimize.fsolve(h_func1,np.array([y1[i-1],y2[i-1]]),args=(s,x[i-1],h,g,l))
    return x,y1

#------------------------------------------------------------------------------
def present(a:int, b:int, v:float, w:int, g:float, l:int,
            func_exact:callable) -> None:
    """Visualisierung der Ergebnisse"""
    fig, axs = plt.subplots(3,2, figsize=(18, 12))
    fig.canvas.manager.set_window_title(
        "Leapfrog Verfahren, Crank Nicolson Verfahren")
    axs = axs.ravel()
    i=0
    for n in [10,15,20,50,100,200]:
        x,y = leapfrog(a, b, n, v, w)
        axs[i].plot(x,y,color="blue")
        x,z = cnv(a,b,n,v,w)
        axs[i].plot(x,z,color="green")
        axs[i].plot(x,func_exact(x,y,g,l,v,w),color="red")
        axs[i].legend(["Leapfrog","CNV","Loesung"])
        axs[i].set_title(f"N = {n}")
        i=i+1
    plt.show()

#------------------------------------------------------------------------------

def func(t, y, g, l):
    """Rechte Seite"""
    return -g / l * y
def func_exact(t, y, g, l, v, w):
    """Lösung der Pendelgleichung"""
    return (np.cos(np.sqrt(g / l) * t) * v + np.sqrt(g / l) *
            np.sin(np.sqrt(g / l) * t) * w)

if __name__ == "__main__":
    # Startwerte
    v = np.pi /12
    w = 0
    # Intervall I = [a,b]
    a = 0
    b = 10
    # Pendelgleichungsvariablen
    g = 9.81
    l = 1

    #Programme Ausfuehren
    present(a, b, v, w, g, l, func_exact)
